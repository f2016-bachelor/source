// Mongoose Schema for the assignment model - registered via the mongoose db
// instance and then exported

import Mongoose = require('mongoose');
import uuid = require('node-uuid');
import DeviceModel = require('./../models/DeviceModel');
import UserSchema = require('./UserSchema');
import assignmentSchema = require('./assignmentSchema');

class DeviceSchema {
    static get schema() {
        // define how the schemas looks
        var schema = Mongoose.Schema({
            _id: {
                type: String,
                default: uuid.v1
            },
            password: {
                type: String,
                required: true
            },
            users: [{ type: String, ref: 'User' }],
            assignments: [{ type: String, ref: 'Assignment' }]
        });
        return schema;
    }
}
// Create the mongoose model to be used for db storing/retrieving
var mongooseDevicesModel: Mongoose.Model<DeviceModel> = Mongoose.model<DeviceModel>('Device', DeviceSchema.schema);

// Export the model so it can be used by the data access logic
export = mongooseDevicesModel;
