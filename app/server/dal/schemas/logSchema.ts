// Mongoose Schema for the logModel - registered via the mongoose db
// instance and then exported

// import DBConnection = require('./../DBconnection');
import Mongoose = require('mongoose');
import uuid = require('node-uuid');
import {LogModel} from '../models/logModel';

/**
 * Class used to define the Mongoose schema for a LogEvent object
 */
class LogSchema {
    'use strict';

    static get schema() {
        // define how the schemas looks
        var schema = Mongoose.Schema({
            event: {
                type: String,
                required: true
            },
            timeStamp: {
              type: Date,
                required: true
            },
            userInputText: {
                type: String,
                required: false
            },
            assignmentName: {
                type: String,
                required: false
            },
            isCareTakerAssignment: {
                type: Boolean,
                required: true
            },
            deviceId: {
                type: String,
                required: true
            },
            userId: {
                type: String,
                required: false
            }
        });
        schema.index({deviceId: 1, userId: 1}, {unique: false}); // making deviceId + userId a compound index key
        return schema;
    }
}
// Create the mongoose model to be used for db storing/retrieving
var mongooseLogModel: Mongoose.Model<LogModel> = Mongoose.model<LogModel>('Logs', LogSchema.schema);

// Export the model so it can be used by the data access logic
export = mongooseLogModel;

