// Mongoose Schema for the assignment model - registered via the mongoose db
// instance and then exported

import Mongoose = require('mongoose');
import uuid = require('node-uuid');
import AssignmentModel = require('./../models/assignmentModel');

class AssignmentSchema {
    static get schema() {
        // define how the schemas looks
        var schema = Mongoose.Schema({
            _id: {
                type: String,
                default: uuid.v1
            },
            name: {
                type: String,
                required: true
            },
            priority: {
                type: String,
                required: true
            },
            date: {
                type: Date,
                required: true
            },
            description: {
                type: String,
                required: false

            },
            finished: {
                type: Boolean,
                required: true,
                default: false
            },
            users: [{type: String, ref: 'User'}],

            userInputNeeded: {
                type: Boolean,
                required: true,
                default: false
            },

            userInput: {
                type: String,
                required: false,
                default: ''
            },

            recurring: {
                type: Boolean,
                required: true,
                default: false
            },

            recurringInfo: {
                type: String,
                required: false,
                default: ''
            },
            alarm: {
                type: Boolean,
                required: false,
                default: true
            },
            roles: [{type: String}]
        });

        return schema;
    }
}

// Create the mongoose model to be used for db storing/retrieving
var mongooseAssignmentModel: Mongoose.Model<AssignmentModel> = Mongoose.model<AssignmentModel>('Assignment', AssignmentSchema.schema);

// Export the model so it can be used by the data access logic
export = mongooseAssignmentModel;
