// Mongoose Schema for the assignment model - registered via the mongoose db
// instance and then exported

// import DBConnection = require('./../DBconnection');
import Mongoose = require('mongoose');
import uuid = require('node-uuid');
import UserModel = require('./../models/UserModel');

class UserSchema {
    static get schema() {
        // define how the schemas looks
        var schema = Mongoose.Schema({
            _id: {
                type: String,
                default: uuid.v1
            },
            firstName: {
                type: String,
                required: true
            },
            lastName: {
                type: String,
                required: true
            },
            middleName: {
                type: String,
                required: false
            }
        });
        return schema;
    }
}
// Create the mongoose model to be used for db storing/retrieving
var mongooseUsersModel: Mongoose.Model<UserModel> = Mongoose.model<UserModel>('User', UserSchema.schema);

// Export the model so it can be used by the data access logic
export = mongooseUsersModel;
