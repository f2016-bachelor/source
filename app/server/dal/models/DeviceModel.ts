import mongoose = require('mongoose');

export interface DeviceModel extends mongoose.Document {
    id: string;
    password: string;
    users: Array;
    assignments: Array;
}
