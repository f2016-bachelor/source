import mongoose = require('mongoose');

/**
 * Same as dto/Assignment, should have identical elements
 */
export interface AssignmentModel extends mongoose.Document {
    name: string;
    priority: string;
    date: Date;
    description: string;
    finished: boolean;
    users: Array;
    recurring: boolean;
    recurringInfo: string;
    alarm: boolean;
    roles: Array;
    userInput: string;
    userInputNeeded: boolean;
}
