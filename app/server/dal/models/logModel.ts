import mongoose = require('mongoose');
// Data object for log event information - used by the logging class
export interface LogModel extends mongoose.Document {
    event: string; // Fx. "assignmentFinish" - strings can be seen in the LogEventTypes static class
    userInputText?: string; // The user input if an assignment required the user to submit information during accomplishment of assignment
    assignmentName?: string;
    isCareTakerAssignment: boolean; // This makes it possible to show only logs for care taker assignments
    timeStamp: Date;
    deviceId: string; // What device (e.g. house) does this log event come from?
    userId?: string; // what user is the logging event associated with
}
