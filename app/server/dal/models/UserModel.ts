import mongoose = require('mongoose');

/**
 * Same as dto/User, should have identical elements
 */
export interface UserModel extends mongoose.Document {
    firstName: string;
    lastName: string;
    middleName: string;
}
