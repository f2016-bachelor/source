import Mongoose = require('mongoose');

'use strict';
class DBConnection {
    public static mongooseInstance: Mongoose;

    /**
     * Sets up the db connection so it's ready for use
     */
    static setupConnection(connectionString: string): void {
        if (this.mongooseInstance) {
            return;
        } else {
            console.log('Connecting to Mongo Db with Mongoose...');
            this.mongooseInstance = Mongoose.connect(connectionString);
        }
    }
}

export = DBConnection;
