import {BasicCRUDPromise} from '../../../shared/interfaces/BasicCrudPromise';
import {DeviceModel} from '../models/DeviceModel';
import {UserModel} from '../models/UserModel';
import {AssignmentModel} from '../models/assignmentModel';

export interface IDeviceDAO extends BasicCRUDPromise<DeviceModel> {
    getUsers(id: String): Promise<Array<UserModel>>;
    getAssignments(id: String): Promise<Array<AssignmentModel>>;
    deleteAssignmentFromDevice(devid: String, assid: String);
}
