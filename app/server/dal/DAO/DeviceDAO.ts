import * as Mongoose from 'mongoose';
import {MongooseCRUD} from './MongooseCRUD';
import {DeviceModel} from '../models/DeviceModel';
import {IDeviceDAO} from '../interfaces/IDeviceDAO';
import {UserModel} from '../models/UserModel';
import {AssignmentModel} from '../models/assignmentModel';

// Data access object for handling the storing and retrieval of items objects in the db
export class DeviceDAO extends MongooseCRUD<DeviceModel>, IDeviceDAO {
    public constructor(model: Mongoose.Model<DeviceModel>) {
        super(model);
    }

    getUsers(id: String): Promise<Array<UserModel>> {
        var devicePromise = this.get(id, ['users']);
        return devicePromise.then((device) => device.users);
    }

    getAssignments(id: String): Promise<Array<AssignmentModel>> {
        var devicePromise = this.get(id, ['assignments']);
        return devicePromise.then((device) => device.assignments);
    }

    deleteAssignmentFromDevice(devid, assid) {
        this.model.update( {_id: devid}, { $pullAll: {assignments: [assid] } } ).exec();
    }
}
