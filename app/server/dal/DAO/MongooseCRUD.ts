import * as Mongoose from 'mongoose';
import {BasicCRUDPromise} from '../../../shared/interfaces/BasicCrudPromise';
import {isArray} from 'util';

// Data access object for handling the storing and retrieval of items objects in the db

export abstract class MongooseCRUD<T> implements BasicCRUDPromise<T> {
    public constructor(protected model: Mongoose.Model<T>) {
    }

    protected checkPopulate(query: Mongoose.Query, populate: Array<String> = []): Mongoose.Query {
        if (isArray(populate) && populate.length > 0) {
            query = query.populate(populate.join(' '));
        }
        return query;
    }

    getAll(populate: Array<String> = []): Promise<Array<T>> {
        return this.checkPopulate(this.model.find({}), populate)
            .exec() as Promise<Array<T>>;
    }

    get(id: String, populate: Array<String> = []): Promise<T> {
        return this.checkPopulate(this.model.findById(id), populate)
            .exec() as Promise<T>;
    }

    find(query: any, populate: Array<String> = []): Promise<Array<T>> {
        return this.checkPopulate(this.model.find(query), populate)
            .exec() as Promise<Array<T>>;
    }

    create(item: T): Promise<any> {
        return this.model.create(item) as Promise<any>;
    }

    update(item: T): Promise<any> {
        return this.model.update({_id: item.id}, item, null).exec() as Promise<any>;
    }

    delete(item: T): Promise<any> {
        return this.model.remove({_id: item}) as Promise<any>;
    }
}
