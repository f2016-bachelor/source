import * as Mongoose from 'mongoose';
import {MongooseCRUD} from './MongooseCRUD';
import {UserModel} from '../models/UserModel';

// Data access object for handling the storing and retrieval of assignments objects in the db
export class UserDAO extends MongooseCRUD<UserModel> {
    public constructor(model: Mongoose.Model<UserModel>) {
        super(model);
    }

}
