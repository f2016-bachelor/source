import * as Mongoose from 'mongoose';
import {MongooseCRUD} from './MongooseCRUD';
import {AssignmentModel} from '../models/assignmentModel';

/**
 * Data access object class for handling the storing and retrieval of assignments objects in the db
 */
export class AssignmentDAO extends MongooseCRUD<AssignmentModel> {

    /**
     * Constructor
     * @param assignmentMongooseModel The model created by using Mongoose.Model and Mongoose.Schema.
       Through this, we can manage the "Assignments"-document in the DB
     */
    public constructor(assignmentMongooseModel: Mongoose.Model<AssignmentModel>) {
        super(assignmentMongooseModel);
    }
}
