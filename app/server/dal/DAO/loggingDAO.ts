import Mongoose = require('mongoose');
import {LogModel} from '../models/logModel';

/**
 * This class read/writes logs to the database
 */
export class LoggingDAO {
    'use strict';

    /**
     * The Mongoose model throug witch logs are saved to the database. Acts as a handle to the db
     */
    private static logModel: Mongoose.Model<LogModel>;

    /**
     * Method for loggin events
     * @param log The log object containing the info to be logged
     * @returns {Promise<LogModel>}
     */
    public static logEvent(log: LogModel): Promise<LogModel> {
        return this.logModel.create(log) as Promise<LogModel>;
    }

    /**
     * Sets the Mongoose model through witch we can log
     * @param logModel
     */
    public static setLogModel(logModel: Mongoose.Model<LogModel>) {
        this.logModel = logModel;
    }

    /**
     * Get all the logs associated with a particular deviceId
     * @param deviceId
     * @returns {Promise<Array<LogModel>>}
     */
    public static getLogsForDevice(deviceId: string): Promise<Array<LogModel>> {
        return this.logModel.find({deviceId: deviceId}).exec() as Promise<Array<LogModel>>;
    }

    /**
     * Get all the logs associated with a particular userId
     * @param userId
     * @returns {Promise<Array<LogModel>>}
     */
    public static getLogsForUser(userId: string): Promise<Array<LogModel>> {
        return this.logModel.find({userId: userId}).exec() as Promise<Array<LogModel>>;
    }
}
