/// <reference path="../../../typings/browser.d.ts" />
import {UserDTO} from '../../shared/dto/user';

export interface IUserStorePromise {
    getUsers(deviceID: string): Promise<Array<UserDTO>>;
    getUser(id): Promise<UserDTO>;
    createUser(item: UserDTO, deviceID: string): Promise<any>;
    updateUser(item: UserDTO, deviceID: string): Promise<any>;
}
