// <reference path="../../../typings/browser.d.ts" />

import {AssignmentDTO} from '../../shared/dto/Assignment';
interface IAssignmentStorePromise {
    getAssignmentsPromise(deviceID: string): Promise<Array<AssignmentDTO>>;

    getAssignmentPromise(id: String, deviceID: string): Promise<AssignmentDTO>;

    createAssignmentPromise(item: AssignmentDTO, deviceID: string): Promise<any>;

    updateAssignmentPromise(item: AssignmentDTO, deviceID: string): Promise<any>;

    getAssignmentsToNotifyPromise(deviceID: string): Promise<Array<AssignmentDTO>>;

    deleteAssignmentPromise(item: AssignmentDTO, deviceID: string): Promise<any>;
}

export {IAssignmentStorePromise};
