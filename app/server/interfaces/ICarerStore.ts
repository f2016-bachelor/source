// <reference path="../../../typings/browser.d.ts" />

export interface ICarerStorePromise {
    /**
     * Login carer from remote, example rfid tag
     * @param id
     */
    remoteLoginCarer(id: string): Promise<boolean>;
}
