// <reference path="../../../typings/browser.d.ts" />

import {Device} from '../../shared/dto/device';
export interface IDeviceStorePromise {
    createDevice(item: Device): Promise<any>;
}
