/// <reference path="../../../typings/browser.d.ts" />
import {BasicCRUDStub} from './stub/BasicCRUDStub';
import {AssignmentDTO} from '../../shared/dto/Assignment';
import {GenericRx, RxChangeEmitter} from './GenericRx';

describe('Server: GenerixRx', () => {
    var storeStub;
    var store: GenericRx<AssignmentDTO>;
    var changeEmitter;
    var items;

    beforeEach(() => {
        items = [];
        items.push(new AssignmentDTO('1', 'Tag medicin', 'High', this.dateStamp, [], [], false, true, '', 'Du skal tage medicin'));
        items.push(new AssignmentDTO('2', 'Mål blodtryk', 'High', this.dateStamp, [], [], false, true, '', 'Du skal måle blodtryk'));
        items.push(new AssignmentDTO('3', 'Ring til bank', 'Normal', this.dateStamp, [], [], false, true, '', 'Du skal ringe til bank'));
        items.push(new AssignmentDTO('4', 'Ring til Ole', 'Low', this.dateStamp, [], [], false, true, '', ''));
        items.push(new AssignmentDTO('5', 'Gå en tur', 'Low', this.dateStamp, [], [], false, true, '', ''));
        changeEmitter = new RxChangeEmitter<AssignmentDTO>();
        storeStub = new BasicCRUDStub<AssignmentDTO>(items);
        store = new GenericRx<AssignmentDTO>(storeStub, changeEmitter);
    });

    it('Add assignment', (done) => {
        // arrange
        var isAdded = false;
        var otherEdited = false;
        // the queue that should receive an item
        changeEmitter.getCreatedHandler().subscribe((item) => isAdded = true);

        // queues that shouldn't receive items.
        changeEmitter.getUpdateHandler().subscribe((item) => otherEdited = true);

        var date = new Date(0).toString();
        var newAssignment = new AssignmentDTO('6', 'name', 'Normal', date, [], [], false, true, '', '');

        // act
        store.create(newAssignment).then(() => {
            // assert
            expect(isAdded).toBe(true);
            expect(otherEdited).toBe(false);
            done();
        });
    });

    it('Update assignment', (done) => {
        // arrange
        var isEdited = false;
        var otherEdited = false;
        changeEmitter.getUpdateHandler().subscribe((item) => isEdited = true);
        changeEmitter.getCreatedHandler().subscribe((item) => otherEdited = true);

        store.get('1').then((toEdit) => {
            toEdit.name = 'EDITED';
            // act
            store.update(toEdit).then(() => {
                // assert
                expect(isEdited).toBe(true);
                done();
            });
        });
    });
});
