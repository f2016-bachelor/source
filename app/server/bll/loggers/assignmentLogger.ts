import {ILogger} from 'iLogger';
import {LogModel} from '../../dal/models/logModel';
import {LoggingDAO} from '../../dal/DAO/loggingDAO';
import {LogEventTypes} from '../../../shared/logEventTypes';
import {AssignmentDTO} from '../../../shared/dto/Assignment';

/**
 * Class for logging assignment specific events
 */
export class AssignmentLogger implements ILogger<AssignmentDTO> {
    'use strict';

    /**
     *
     * Abstract method for logging events
     * @param item: Can be a user og an assignment. The JSON object associated with this particular event log
     * @param deviceId: Id of the device
     * @param userId: Id of the user - for an assignment, this is required (but implementing the interface for ILogger, it needs to be nullable)
     * @param event: Type of the log - these types are defined in the LogEventTypes class in shared folder
     * @returns {Promise<LogModel>}
     */
    logEvent(item: AssignmentDTO, deviceId: string, event: string, userId?: string): Promise<LogModel> {

        var userInput: string;

        // Check if the assignment required user input
        if (item.userInputNeeded && LogEventTypes.assignmentFinish) {
            userInput = item.userInput;
        }

        // Generate the event text based on the type of event (oprettet, udført and so on) and the roles of the assignment
        if (item.roles.indexOf('carer') !== -1 && item.roles.indexOf('resident') !== -1) {
            event = 'Opgave tilhørende plejer og borger ' + event;
        } else if (item.roles.indexOf('carer') !== -1) {
            event = 'Opgave tilhørende plejer ' + event;
        } else {
            event = 'Opgave tilhørende borger ' + event;
        }

        var eventModel: LogModel = <LogModel>{
            event: event,
            userInputText: userInput,
            assignmentName: item.name,
            isCareTakerAssignment: item.roles.indexOf('carer') !== -1,
            timeStamp: new Date(),
            deviceId: deviceId,
            userId: userId
        };

        return LoggingDAO.logEvent(eventModel);
    }
}
