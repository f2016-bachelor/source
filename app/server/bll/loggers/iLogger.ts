import Mongoose = require('mongoose');
import {LogModel} from '../../dal/models/logModel';

/**
 * This abstract class implements the common things for the logging mecanism in the BLL
 */
export interface ILogger<T> {
    'use strict';

    /**
     * Abstract method fpor logging events - overriden by extending classes
     * @param item: The JSON object associated with this particular event log
     * @param deviceId: Id of the device
     * @param userId: Id of the user (not required)
     * @param event: Type of the log
     * @returns Promise<LogModel>
     */
    logEvent(item: T, deviceId: string, event: string, userId?: string): Promise<LogModel>;
}
