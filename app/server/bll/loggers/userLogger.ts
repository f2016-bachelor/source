import {ILogger} from 'iLogger';
import {LogModel} from '../../dal/models/logModel';
import {LoggingDAO} from '../../dal/DAO/loggingDAO';
import {LogEventTypes} from '../../../shared/logEventTypes';
import {UserDTO} from '../../../shared/dto/user';

/**
 *  Class for logging user specific events
 */
export class UserLogger implements ILogger<UserDTO> {
    'use strict';

    /**
     *
     * Method for logging events
     * @param item: A user object . The JSON object associated with this particular event log
     * @param deviceId: Id of the device
     * @param userId: Id of the user (not required)
     * @param event: What the kind of the event to be logged is
     * @returns {Promise<LogModel>}
     */
    logEvent(item: UserDTO, deviceId: string, event: string, userId?: string): Promise<LogModel> {

        // First create the log event object containing the relevant info
        var eventModel: LogModel = <LogModel>{
            event: event,
            userInputText: null,
            assignmentName: null,
            isCareTakerAssignment: false,
            timeStamp: new Date(),
            deviceId: deviceId,
            userId: userId
        };

        return LoggingDAO.logEvent(eventModel);
    }
}
