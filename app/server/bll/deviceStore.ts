/// <reference path="../../../typings/main.d.ts" />

import * as bcrypt from 'bcrypt-nodejs';
import {BasicCRUDPromise} from '../../shared/interfaces/BasicCrudPromise';
import {ConverterCRUD} from './../../shared/bll/crud/ConverterCRUD';
import {IDeviceStorePromise} from '../interfaces/IDeviceStore';
import {Device} from '../../shared/dto/device';
import {DeviceModel} from '../dal/models/DeviceModel';
import {DeviceConverter} from './converters/DeviceConverter';

export class DeviceStore implements IDeviceStorePromise {
    converted: BasicCRUDPromise<Device>;

    constructor(protected dao: BasicCRUDPromise<DeviceModel>) {
        this.converted = new ConverterCRUD(dao,
            (fromDTO) => DeviceConverter.newFromDTO(fromDTO),
            (fromModel) => DeviceConverter.newToDTO(fromModel));
    }

    createDevice(item: Device): Promise<any> {
        // hash password with salt
        item.password = bcrypt.hashSync(item.password, bcrypt.genSaltSync(5));
        return this.converted.create(item).catch((err) => new Error('Unable to create device' + err));
    }
}
