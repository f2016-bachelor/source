/// <reference path="../../../typings/main.d.ts" />
import {RRule} from 'rrule';
import * as uuid from 'node-uuid';
import * as _ from 'lodash';
import {IAssignmentStorePromise} from '../interfaces/IAssignmentStore';
import {AssignmentConverter} from './converters/AssignmentConverter';
import {BasicCRUDPromise} from '../../shared/interfaces/BasicCrudPromise';
import {AssignmentModel} from '../dal/models/assignmentModel';
import {RxChangeEventSource, GenericRx} from './GenericRx';
import {IDeviceDAO} from '../dal/interfaces/IDeviceDAO';
import {AssignmentDTO} from '../../shared/dto/Assignment';
import {ConverterCRUD} from '../../shared/bll/crud/ConverterCRUD';
import {DeviceModel} from '../dal/models/DeviceModel';

export class AssignmentStore implements IAssignmentStorePromise {
    converted: BasicCRUDPromise<AssignmentDTO>;
    wrapped: GenericRx<AssignmentDTO>;

    constructor(protected dao: BasicCRUDPromise<AssignmentModel>, protected deviceDAO: IDeviceDAO, protected changeSource: RxChangeEventSource<AssignmentDTO>) {
        this.converted = new ConverterCRUD(dao,
            (fromDTO) => AssignmentConverter.newFromDTO(fromDTO),
            (fromModel) => AssignmentConverter.newToDTO(fromModel));

        this.wrapped = new GenericRx(this.converted, changeSource);
    }

    // finds assignment to remove from list, based on the id of assignment passed from click function
    getAssignmentsPromise(deviceID: string): Promise<Array<AssignmentDTO>> {
        return this.deviceDAO.getAssignments(deviceID).then((assignments) =>
            assignments.map(item => AssignmentConverter.newToDTO(item)));
    }

    getAssignmentPromise(id: String, deviceID: string): Promise<AssignmentDTO> {
        return this.converted.get(id);
    }

    updateAssignmentPromise(item: AssignmentDTO, deviceID: string): Promise<any> {
        // return new Promise((resolve, reject) => {
        var currentAssignment = this.wrapped.get(item.id);

        // if not recurring, then we can just update the object
        if (!item.recurring) {
            return this.wrapped.update(item, deviceID);
        } else { // if it is recurrent then it is a bit more difficult
            var self = this;
            // action: create a new assignment for next recurrence, shrink previous assignment
            return currentAssignment.then((currentFromDB) => {
                var promisesToReturn = []; // list of promices to wait for, it's actions send to the database
                var itemsToCreate = [];
                var itemDate = new Date(item.date); // parse the date from the item.
                var reccurrenceOptions = RRule.parseString(item.recurringInfo); // parse the RRule from the item
                var rules = this.splitRule(reccurrenceOptions, itemDate, item.finished); // calculate new rules based on the date

                // function to copy data from rule to assignment
                var applyRule = (dto: AssignmentDTO, rule: RRuleOption) => {
                    dto.finished = false;
                    dto.date = rule.dtstart.toString();
                    dto.recurringInfo = RRule.optionsToString(rule);
                };

                // if only the updated element's rule needs to be changed
                if (rules.length === 1) {
                    var updatedObject = item; // expect that information in the assignment was changed

                    // if it's finished then we do not change it's content. // TODO: Should we change the content anyways
                    if (item.finished) { // if finished then the updated should not be changed
                        updatedObject = currentFromDB;
                    }

                    // copy values from the rrule to the item
                    applyRule(updatedObject, rules[0]);

                    // update the assignment in the database
                    promisesToReturn.push(self.wrapped.update(updatedObject));
                } else if (rules.length === 2) { // a earlier and after rule have been created
                    // earlier rule, update current object in database
                    applyRule(currentFromDB, rules[0]);
                    promisesToReturn.push(self.wrapped.update(currentFromDB));

                    // after, create new
                    var toBeNext = this.copyAssignment(item);
                    toBeNext.userInput = '';
                    applyRule(toBeNext, rules[1]);

                    itemsToCreate.push(toBeNext);
                }

                // create a task for a finished assignment, if no rules was returned then the rules have no reccurrences left and should not be recurring
                if (item.finished || rules.length === 0) {
                    // finished element
                    var toBeClosed;

                    // if no rules generated then close the existing assignment
                    if (rules.length === 0) {
                        toBeClosed = _.cloneDeep(item);
                    } else { // otherwise create a new one and close that
                        toBeClosed = this.createSingleAssingmentFrom(item);
                    }
                    toBeClosed.recurring = false;
                    toBeClosed.recurringInfo = '';

                    // if this is the last instance of the reccurence rule, update the existing object
                    if (rules.length === 0) {
                        promisesToReturn.push(self.wrapped.update(toBeClosed));
                    } else { // otherwise create a new one
                        itemsToCreate.push(toBeClosed);
                    }
                }
                promisesToReturn.push(this.deviceDAO.get(deviceID)
                    .catch((err) => new Error(err + ', Unable to get device'))
                    .then((devicePromise) => this.createMultipleInternal(itemsToCreate, devicePromise)));
                return Promise.all(promisesToReturn);
            });
        }
    }

    createAssignmentPromise(item: AssignmentDTO, deviceID: string): Promise<any> {
        return this.deviceDAO.get(deviceID)
            .catch((err) => new Error(err + ', Unable to get device'))
            .then((devicePromise) => this.createMultipleInternal([item], devicePromise));
    }

    deleteAssignmentPromise(id: string, deviceID: string): Promise<any> {
        return Promise.all(
                [
                    this.wrapped.delete(id),
                    this.deviceDAO.deleteAssignmentFromDevice(deviceID, id)
                ]
            );
    }

    createMultipleInternal(items: Array<AssignmentDTO>, device: DeviceModel): Promise<any> {
        var createPromises: Array<Promise> = items.map((item) => this.wrapped.create(item)
            .then(() => device.assignments.push(item.id))
            .catch((err) => new Error(err + ', Unable to create assignment'))
        );

        // if all promises succeed then add them to the device
        return Promise.all(createPromises).then(() =>
            this.deviceDAO.update(device).catch((err) => new Error(err + ', Unable to update device'))
        );
    }

    /**
     * Splits a rule into two rules
     * @param ruleOptions the rule to split
     * @param date the first recurrence in the second rule, the first rule will end before this occurence.
     * @param excludeDate remove the date if an occurence
     * @returns {any}
     */
    splitRule(ruleOptions: RRuleOption, date: Date, excludeDate: boolean): Array<RRuleOption> {
        var recurrence = new RRule(_.cloneDeep(ruleOptions));

        var earlierValid = true;
        var afterValid = true;
        var earlierRecurrenceOptions = _.cloneDeep(ruleOptions); // copy of original rule
        var afterRecurrenceOptions = _.cloneDeep(ruleOptions); // copy of original rule

        // before the first recurrence
        if (date.getTime() < ruleOptions.dtstart.getTime()) {
            // just return the same rule;
            return [ruleOptions];
        } else if (date.getTime() === ruleOptions.dtstart.getTime()) { // if it's the first recurrence
            if (excludeDate) { // if the date have to be removed
                var nextDate = recurrence.after(date);

                // no need to create an additional rule
                afterValid = false;

                // if the only recurrence is excluded
                if (nextDate === null) {
                    earlierValid = false;
                } else {
                    earlierRecurrenceOptions.dtstart = nextDate;
                }
            } else { // first date should be start of new, just return the original rule
                return [ruleOptions];
            }
        } else { // date after first recurrence
            // next element
            var newNextDate = excludeDate ? recurrence.after(date) : recurrence.after(date, true);

            // if after is null, then there is no more assignments.
            // IE the finished assignment is the last of a sequence
            if (newNextDate === null) {
                afterValid = false;
            } else {
                afterRecurrenceOptions.dtstart = newNextDate;
            }
        }

        // if count is defined in the rule then we need to know how much to lower it
        if (ruleOptions.count) {
            // get all dates before a specific date
            var dates = recurrence.all((dateInstance, i) => dateInstance < date);
            var countBetween = dates.length;

            earlierRecurrenceOptions.count = countBetween;
            afterRecurrenceOptions.count -= countBetween;

            // if the date should be excluded then the count should be lowered additionally
            if (excludeDate) {
                afterRecurrenceOptions.count -= 1;

            }
        } else { // use until to limit earlier recurrence.
            earlierRecurrenceOptions.until = recurrence.before(date);
        }

        var arrayToReturn = [];
        if (earlierValid) {
            arrayToReturn.push(earlierRecurrenceOptions);
        }
        if (afterValid) {
            arrayToReturn.push(afterRecurrenceOptions);
        }

        return arrayToReturn;
    }

    setTaskFinished(item: AssignmentDTO) {
        item.recurring = false;
        item.recurringInfo = '';
        item.finished = true;
    }

    copyAssignment(item: AssignmentDTO): AssignmentDTO {
        var newObj = _.cloneDeep(item);
        newObj.id = uuid.v1();
        return newObj;
    }

    createSingleAssingmentFrom(item: AssignmentDTO): AssignmentDTO {
        var newObj = this.copyAssignment(item);

        newObj.recurring = false;
        newObj.recurringInfo = '';

        return newObj;
    }

    isFirstInSeries(assignment: AssignmentDTO): boolean {
        return assignment.seqno === 0;
    }

    /**
     * Finds assignments that needs to have notifications shown now.
     * @param deviceID
     * @returns {undefined}
     */
    getAssignmentsToNotifyPromise(deviceID: string): Promise<Array<AssignmentDTO>> {
        return this.deviceDAO.get(deviceID).then((device: DeviceModel) => this.converted.find({
            '_id': {
                $in: device.assignments
            },
            'finished': false,
            'alarm': true,
            'date': {
                $lte: new Date()
            },
            'roles': {
                $in: ['resident']
            }
        }, ['users']));
    }
}
