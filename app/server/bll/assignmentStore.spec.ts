/// <reference path="../../../typings/browser.d.ts" />

import * as _ from 'lodash';
import {AssignmentStore} from './assignmentStore';
import {AssignmentDTO} from '../../shared/dto/Assignment';
import {RxChangeEmitter} from './GenericRx';
import {AssignmentConverter} from './converters/AssignmentConverter';
import {DeviceModel} from '../dal/models/DeviceModel';

describe('Server: AssignmentStore', () => {
    var uut: AssignmentStore;
    var assignmentDAO;
    var deviceDAO;

    beforeEach(() => {
        var dtoFunctions = ['getAll', 'find', 'get', 'create', 'update', 'delete'];
        deviceDAO = jasmine.createSpyObj('deviceDAO', dtoFunctions.concat(['getUsers', 'getAssignments']));
        assignmentDAO = jasmine.createSpyObj('assignmentDAO', dtoFunctions);

        var rxChangeEmitter = new RxChangeEmitter<AssignmentDTO>();
        uut = new AssignmentStore(assignmentDAO, deviceDAO, rxChangeEmitter);
    });

    var createSimpleDTO = (dateNowString, dateRRuleStringBefore): AssignmentDTO => {
        var itemRecurrenceRule = 'FREQ=HOURLY;INTERVAL=1;DTSTART=' + dateRRuleStringBefore;
        var item = new AssignmentDTO('1', 'Tag medicin', 'Normal', dateNowString, [], [], false, true, itemRecurrenceRule, 'Du skal tage medicin');
        return item;
    };

    describe('getall', () => {
        beforeEach(() => {

        });

        it('no elements', (done) => {
            // arrange
            var deviceID = '1';

            deviceDAO.getAssignments.and.returnValue(Promise.resolve([]));

            // act
            var act = uut.getAssignmentsPromise(deviceID);
            act.then((items) => {
                // assert
                expect(deviceDAO.getAssignments).toHaveBeenCalledWith(deviceID);

                expect(items.length).toEqual(0);

                done();
            });
            act.catch(() => {
                // the act call should not fail.
                expect(false).toBe(true);
                done();
            });
        });

        it('one assignment', (done) => {
            // arrange
            var deviceID = '1';

            var now = new Date();
            var assignment = AssignmentConverter.newFromDTO(createSimpleDTO(now.toISOString(), ''));
            deviceDAO.getAssignments.and.returnValue(Promise.resolve([assignment]));

            // act
            var act = uut.getAssignmentsPromise(deviceID);
            act.then((items) => {
                // assert
                expect(deviceDAO.getAssignments).toHaveBeenCalledWith(deviceID);

                expect(items.length).toEqual(1);

                done();
            });
            act.catch(() => {
                // the act call should not fail.
                expect(false).toBe(true);
                done();
            });
        });

        it('correct type', (done) => {
            // arrange
            var deviceID = '1';

            var now = new Date();
            var assignment = AssignmentConverter.newFromDTO(createSimpleDTO(now.toISOString(), ''));
            deviceDAO.getAssignments.and.returnValue(Promise.resolve([assignment]));

            // act
            var act = uut.getAssignmentsPromise(deviceID);
            act.then((items) => {
                // assert
                expect(deviceDAO.getAssignments).toHaveBeenCalledWith(deviceID);

                // check that it contains a date
                expect(items[0].date).toEqual(now.toISOString());
                expect(items[0].id).toBeDefined(); // to contain an id

                expect(items.length).toEqual(1);

                done();
            });
            act.catch(() => {
                // the act call should not fail.
                expect(false).toBe(true);
                done();
            });
        });
    });

    describe('update', () => {
        var dates: any = {};
        var datesString: any = {};
        var itemRecurrenceRule;
        beforeEach(() => {
            dates.before = new Date(Date.UTC(2000, 0, 1, 10));
            dates.now = new Date(Date.UTC(2000, 0, 1, 12));
            // dates.after = new Date(Date.UTC(2000, 0, 1, 14));

            var deviceModel = <DeviceModel>{};
            deviceModel._id = '1';
            deviceModel.assignments = [];

            deviceDAO.get.and.returnValue(Promise.resolve(deviceModel));
            deviceDAO.update.and.returnValue(Promise.resolve());
            assignmentDAO.update.and.returnValue(Promise.resolve());
            assignmentDAO.create.and.returnValue(Promise.resolve());
        });

        var setupReturnValues = (toDbModel) => {
            var itemModel = AssignmentConverter.newFromDTO(toDbModel);
            assignmentDAO.get.and.returnValue(Promise.resolve(itemModel));
        };

        var createDTO = (): AssignmentDTO => {
            datesString.before = '20000101T100000Z';
            datesString.now = '20000101T120000Z';
            datesString.after = '20000101T140000Z';

            itemRecurrenceRule = 'FREQ=HOURLY;INTERVAL=1;DTSTART=' + datesString.before;
            var item = new AssignmentDTO('1', 'Tag medicin', 'Normal', dates.now.toString(), [], [], false, true, itemRecurrenceRule, 'Du skal tage medicin');
            return item;
        };

        describe('recurring', () => {
            /**
             * If the assignment is the first in the series, then it just needs to be updated.
             */
            it('first in series, update', (done) => {
                // arrange
                dates.now = dates.before;

                var item = createDTO();
                setupReturnValues(item);

                item.name = 'changed';

                // act
                var act = uut.updateAssignmentPromise(item, '1');
                act.then(() => {
                    // assert
                    expect(assignmentDAO.update).toHaveBeenCalled();
                    expect(assignmentDAO.create).not.toHaveBeenCalled();

                    var updatedElm = assignmentDAO.update.calls.argsFor(0)[0];
                    expect(updatedElm.name).toEqual('changed');
                    done();
                });
                act.catch(() => {
                    // the act call should not fail.
                    expect(false).toBe(true);
                    done();
                });
            });

            it('first in series, finish', (done) => {
                // arrange
                dates.now = dates.before;
                var item = createDTO();

                var hourAfterBefore = new Date(dates.before.getTime());
                hourAfterBefore.setHours(hourAfterBefore.getHours() + 1);

                var itemModel = AssignmentConverter.newFromDTO(item);
                assignmentDAO.get.and.returnValue(Promise.resolve(itemModel));

                item.finished = true;

                var original = _.cloneDeep(item);

                // act
                var act = uut.updateAssignmentPromise(item, '1');
                act.then(() => {
                    // assert
                    // the item moved forward should be updated
                    expect(assignmentDAO.update).toHaveBeenCalled();

                    // the new, finished item should be saved
                    expect(assignmentDAO.create).toHaveBeenCalled();

                    // the updated element
                    expect(assignmentDAO.update.calls.count()).toBe(1);
                    var updatedElm = assignmentDAO.update.calls.mostRecent().args[0];
                    expect(updatedElm.date).not.toEqual(dates.before, 'before');
                    expect(updatedElm.date).toEqual(hourAfterBefore, 'hour later');
                    expect(updatedElm.recurring).toBeTruthy('updated should be recurring');
                    expect(updatedElm.finished).toBeFalsy('updated should not be finished');

                    /*
                     the new element
                     */
                    // only one assignment should be created
                    expect(assignmentDAO.create.calls.count()).toBe(1);

                    // get the created assignment
                    var createdElm = assignmentDAO.create.calls.mostRecent().args[0];
                    // the new element should have the same date as before
                    expect(createdElm.date).toEqual(new Date(original.date));
                    // it should not be recurring anymore
                    expect(createdElm.recurring).toBeFalsy('created should not be recurring');
                    // it should be finished
                    expect(createdElm.finished).toBeTruthy('created should be finished');

                    done();
                });
                act.catch(() => {
                    // the act call should not fail.
                    expect(false).toBe(true);
                    done();
                });
            });

            describe('middle of series, update', () => {
                var item: AssignmentDTO;
                var savedItem: AssignmentDTO;

                beforeEach(() => {
                    // arrange
                    item = createDTO();
                    savedItem = createDTO();

                    item.seqno = 1;

                    item.date = dates.now.toString();
                    savedItem.date = dates.before.toString();
                });

                it('correct update, create calls', (done) => {
                    // arrange
                    setupReturnValues(savedItem);


                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert
                        expect(assignmentDAO.update).toHaveBeenCalled();
                        expect(assignmentDAO.create).toHaveBeenCalled();

                        expect(assignmentDAO.update.calls.count()).toBe(1);
                        expect(assignmentDAO.create.calls.count()).toBe(1);


                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });

                it('name/priority changed', (done) => {
                    // arrange
                    setupReturnValues(savedItem);

                    item.name = 'changed';
                    item.priority = 'Low';

                    var original = _.cloneDeep(item);

                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert

                        // the updated element
                        var updatedElm = assignmentDAO.update.calls.mostRecent().args[0];
                        expect(updatedElm.name).not.toEqual(original.name, 'before');
                        expect(updatedElm.priority).not.toEqual(original.priority, 'before');

                        // get the created assignment
                        var createdElm = assignmentDAO.create.calls.mostRecent().args[0];
                        expect(createdElm.name).toEqual(original.name, 'after');
                        expect(createdElm.priority).toEqual(original.priority, 'after');

                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });

                it('recurring/finished/date', (done) => {
                    // arrange
                    setupReturnValues(savedItem);

                    var original = _.cloneDeep(item);

                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert

                        // the updated element
                        var updatedElm = assignmentDAO.update.calls.mostRecent().args[0];
                        expect(updatedElm.date).toEqual(dates.before, 'before');
                        expect(updatedElm.recurring).toBeTruthy('updated should be recurring');
                        expect(updatedElm.finished).toBeFalsy('updated should not be finished');

                        // get the created assignment
                        var createdElm = assignmentDAO.create.calls.mostRecent().args[0];
                        // the new element should have the same date as before
                        expect(createdElm.date).toEqual(dates.now, 'now');
                        // it should not be recurring anymore
                        expect(createdElm.recurring).toBeTruthy('created should be recurring');
                        // it should be finished
                        expect(createdElm.finished).toBeFalsy('created should not be finished');

                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });

                it('specified until', (done) => {
                    // arrange
                    var untilUpdated = 'UNTIL=20000101T110000Z';
                    var untilCreated = 'UNTIL=20000101T130000Z';
                    item.recurringInfo += ';' + untilCreated;

                    setupReturnValues(savedItem);

                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert

                        // the updated element
                        var updatedElm = assignmentDAO.update.calls.mostRecent().args[0];
                        expect(updatedElm.recurringInfo).toContain(untilUpdated);

                        // get the created assignment
                        var createdElm = assignmentDAO.create.calls.mostRecent().args[0];
                        expect(createdElm.recurringInfo).toContain(untilCreated);

                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });

                it('specified count', (done) => {
                    // arrange
                    var untilCreated = 'COUNT=5';
                    item.recurringInfo += ';' + untilCreated;
                    savedItem.recurringInfo += ';' + untilCreated;

                    setupReturnValues(savedItem);

                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert

                        // the updated element
                        var updatedElm = assignmentDAO.update.calls.mostRecent().args[0];
                        expect(updatedElm.recurringInfo).toContain('COUNT=2');

                        // get the created assignment
                        var createdElm = assignmentDAO.create.calls.mostRecent().args[0];
                        expect(createdElm.recurringInfo).toContain('COUNT=3');

                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });
            });

            describe('middle of series, finish', () => {
                var item;
                var savedItem;
                beforeEach(() => {
                    // arrange
                    item = createDTO();
                    savedItem = createDTO();

                    item.seqno = 1;
                    item.finished = true;

                    // Stored value is 1 day before item
                    savedItem.date = dates.before.toString();
                    assignmentDAO.create.and.returnValue(Promise.resolve());
                });


                it('correct update, create calls', (done) => {
                    // arrange
                    setupReturnValues(savedItem);

                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert
                        expect(assignmentDAO.update).toHaveBeenCalled();
                        expect(assignmentDAO.create).toHaveBeenCalled();

                        expect(assignmentDAO.update.calls.count()).toBe(1);
                        expect(assignmentDAO.create.calls.count()).toBe(2);

                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });

                it('recurring/finished/date', (done) => {
                    // arrange
                    dates.after = new Date(Date.UTC(2000, 0, 1, 13));

                    setupReturnValues(savedItem);

                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert

                        // the updated element
                        var updatedElm = assignmentDAO.update.calls.mostRecent().args[0];
                        expect(updatedElm.date).toEqual(dates.before, 'before');
                        expect(updatedElm.recurring).toBeTruthy('updated should be recurring');
                        expect(updatedElm.finished).toBeFalsy('updated should not be finished');

                        // created element, should be finished
                        // get the created assignment
                        var createdElm = assignmentDAO.create.calls.argsFor(1)[0];
                        // the new element should have the same date as before
                        expect(createdElm.date).toEqual(dates.now, 'now');
                        // it should not be recurring anymore
                        expect(createdElm.recurring).toBeFalsy('created[0] should not be recurring');
                        // it should be finished
                        expect(createdElm.finished).toBeTruthy('created[0] should be finished');

                        // created element, should continue after the finished one
                        // get the created assignment
                        var continueingElm = assignmentDAO.create.calls.argsFor(0)[0];
                        // the new element should have the same date as before
                        expect(continueingElm.date).toEqual(dates.after, 'after');
                        // it should not be recurring anymore
                        expect(continueingElm.recurring).toBeTruthy('created[1] should be recurring');
                        // it should be finished
                        expect(continueingElm.finished).toBeFalsy('created[1] should not be finished');

                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });

                it('specified until', (done) => {
                    // arrange
                    var untilUpdated = 'UNTIL=20000101T110000Z';
                    var untilCreated = 'UNTIL=20000101T130000Z';
                    item.recurringInfo += ';' + untilCreated;

                    setupReturnValues(savedItem);


                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert

                        // the updated element
                        var updatedElm = assignmentDAO.update.calls.mostRecent().args[0];
                        expect(updatedElm.recurringInfo).toContain(untilUpdated);

                        // get the created assignment
                        var createdElm = assignmentDAO.create.calls.argsFor(0)[0];
                        expect(createdElm.recurringInfo).toContain(untilCreated);

                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });

                it('specified count', (done) => {
                    // arrange
                    var untilCreated = 'COUNT=4';
                    item.recurringInfo += ';' + untilCreated;
                    savedItem.recurringInfo += ';' + untilCreated;

                    setupReturnValues(savedItem);

                    // act
                    var act = uut.updateAssignmentPromise(item, '1');
                    act.then(() => {
                        // assert

                        // the updated element
                        var updatedElm = assignmentDAO.update.calls.mostRecent().args[0];
                        expect(updatedElm.recurringInfo).toContain('COUNT=2', 'updated');

                        // get the created assignment
                        var createdElm = assignmentDAO.create.calls.argsFor(0)[0];
                        expect(createdElm.recurringInfo).toContain('COUNT=1', 'created');

                        done();
                    });
                    act.catch((err) => {
                        // the act call should not fail.
                        expect(false).toBe(true);
                        done();
                    });
                });
            });

            describe('last of series', () => {
                var item;
                var savedItem;
                var untilString: any = {};

                beforeEach(() => {
                    // arrange
                    item = createDTO();
                    savedItem = createDTO();
                    assignmentDAO.create.and.returnValue(Promise.resolve());

                    untilString.beforeUpdate = '20000101T' + dates.now.getUTCHours() + '0000Z';
                    untilString.afterUpdate = '20000101T' + (dates.now.getUTCHours() - 1) + '0000Z';
                });

                describe('update', () => {
                    it('correct update, create calls', (done) => {
                        // arrange
                        var untilUpdated = 'UNTIL=' + untilString.beforeUpdate;
                        item.recurringInfo += ';' + untilUpdated;
                        savedItem.recurringInfo += ';' + untilUpdated;
                        item.seqno = 2;

                        setupReturnValues(savedItem);

                        // act
                        var act = uut.updateAssignmentPromise(item, '1');
                        act.then(() => {
                            // assert
                            expect(assignmentDAO.update).toHaveBeenCalled();
                            expect(assignmentDAO.create).toHaveBeenCalled();

                            expect(assignmentDAO.update.calls.count()).toBe(1);
                            expect(assignmentDAO.create.calls.count()).toBe(1);

                            done();
                        });
                        act.catch((err) => {
                            // the act call should not fail.
                            expect(false).toBe(true);
                            done();
                        });
                    });

                    it('specified until', (done) => {
                        // arrange
                        var untilUpdated = ';UNTIL=' + untilString.beforeUpdate;
                        item.recurringInfo += untilUpdated;
                        savedItem.recurringInfo += untilUpdated;
                        item.seqno = 2;
                        item.name = 'changed';

                        var original = _.cloneDeep(item);

                        setupReturnValues(savedItem);

                        // act
                        var act = uut.updateAssignmentPromise(item, '1');
                        act.then(() => {
                            // assert
                            expect(assignmentDAO.create.calls.count()).toBe(1);

                            // the updated element
                            var updatedElm = assignmentDAO.update.calls.argsFor(0)[0];
                            expect(updatedElm.recurringInfo).toContain(untilString.afterUpdate, 'updated until');
                            expect(updatedElm.name).not.toEqual(original.name, 'updated name not should be changed');


                            // get the created assignment
                            var createdElm = assignmentDAO.create.calls.argsFor(0)[0];
                            expect(createdElm.recurringInfo).toContain(untilString.beforeUpdate, 'created');
                            expect(createdElm.finished).toBeFalsy('created[0] should not be finished');
                            expect(createdElm.name).toEqual(original.name, 'created[0] name should be changed');

                            done();
                        });
                        act.catch((err) => {
                            // the act call should not fail.
                            expect(false).toBe(true);
                            done();
                        });
                    });

                    it('specified count', (done) => {
                        // arrange
                        var untilUpdated = 'COUNT=3';
                        item.recurringInfo += ';' + untilUpdated;
                        savedItem.recurringInfo += ';' + untilUpdated;
                        item.seqno = 2;
                        item.name = 'changed';

                        var original = _.cloneDeep(item);

                        setupReturnValues(savedItem);

                        // act
                        var act = uut.updateAssignmentPromise(item, '1');
                        act.then(() => {
                            // assert
                            expect(assignmentDAO.create.calls.count()).toBe(1, 'create finished');

                            // the updated element
                            var updatedElm = assignmentDAO.update.calls.argsFor(0)[0];
                            expect(updatedElm.recurringInfo).toContain('COUNT=2');
                            expect(updatedElm.name).not.toEqual(original.name, 'updated name not should be changed');

                            // get the created assignment
                            var createdElm = assignmentDAO.create.calls.argsFor(0)[0];
                            expect(createdElm.finished).toBeFalsy('created[0] should not be finished');
                            expect(createdElm.name).toEqual(original.name, 'created[0] name should be changed');
                            done();
                        });
                        act.catch((err) => {
                            // the act call should not fail.
                            expect(false).toBe(true);
                            done();
                        });
                    });
                });

                describe('finish', () => {
                    it('correct update, create calls', (done) => {
                        // arrange
                        var untilUpdated = ';UNTIL=' + untilString.beforeUpdate;
                        item.recurringInfo += untilUpdated;
                        savedItem.recurringInfo += untilUpdated;
                        item.seqno = 2;
                        item.finished = true;

                        setupReturnValues(savedItem);

                        // act
                        var act = uut.updateAssignmentPromise(item, '1');
                        act.then(() => {
                            // assert
                            expect(assignmentDAO.update).toHaveBeenCalled();
                            expect(assignmentDAO.create).toHaveBeenCalled();

                            expect(assignmentDAO.update.calls.count()).toBe(1);
                            expect(assignmentDAO.create.calls.count()).toBe(1);

                            done();
                        });
                        act.catch((err) => {
                            // the act call should not fail.
                            expect(false).toBe(true);
                            done();
                        });
                    });

                    it('specified until', (done) => {
                        // arrange
                        var untilUpdated = ';UNTIL=' + untilString.beforeUpdate;
                        item.recurringInfo += untilUpdated;
                        savedItem.recurringInfo += untilUpdated;
                        item.seqno = 2;
                        item.finished = true;

                        setupReturnValues(savedItem);

                        // act
                        var act = uut.updateAssignmentPromise(item, '1');
                        act.then(() => {
                            // assert
                            expect(assignmentDAO.create.calls.count()).toBe(1);

                            // the updated element
                            var updatedElm = assignmentDAO.update.calls.argsFor(0)[0];
                            expect(updatedElm.recurringInfo).toContain(untilString.afterUpdate);

                            // get the created assignment
                            var createdElm = assignmentDAO.create.calls.argsFor(0)[0];
                            expect(createdElm.finished).toBeTruthy('created[0] should be finished');

                            done();
                        });
                        act.catch((err) => {
                            // the act call should not fail.
                            expect(false).toBe(true);
                            done();
                        });
                    });

                    it('specified count', (done) => {
                        // arrange
                        var untilUpdated = 'COUNT=3';
                        item.recurringInfo += ';' + untilUpdated;
                        savedItem.recurringInfo += ';' + untilUpdated;
                        item.seqno = 2;
                        item.finished = true;

                        setupReturnValues(savedItem);

                        // act
                        var act = uut.updateAssignmentPromise(item, '1');
                        act.then(() => {
                            // assert
                            expect(assignmentDAO.create.calls.count()).toBe(1, 'create finished');

                            // the updated element
                            var updatedElm = assignmentDAO.update.calls.argsFor(0)[0];
                            expect(updatedElm.recurringInfo).toContain('COUNT=2');

                            // get the created assignment
                            var createdElm = assignmentDAO.create.calls.argsFor(0)[0];
                            expect(createdElm.finished).toBeTruthy('created[0] should be finished');
                            done();
                        });
                        act.catch((err) => {
                            // the act call should not fail.
                            expect(false).toBe(true);
                            done();
                        });
                    });
                });
            });


        });
    });

    // describe('notification', () => {
    //     var uut: AssignmentStore;
    //     var assignmentDAO;
    //     var deviceDAO;
    //     var itemDate;
    //     var itemDTStart;
    //     var itemRecurrenceRule;
    //     var deviceModel;
    //     beforeEach(() => {
    //         itemDate = new Date(Date.UTC(2000, 0, 1, 12));
    //         itemDTStart = '20000101T100000Z';
    //         itemRecurrenceRule = 'FREQ=HOURLY;INTERVAL=1;DTSTART=' + itemDTStart;
    //
    //         var dtoFunctions = ['getAll', 'find', 'get', 'create', 'update', 'delete'];
    //         deviceDAO = jasmine.createSpyObj('deviceDAO', dtoFunctions.concat(['getUsers', 'getAssignments']));
    //         assignmentDAO = jasmine.createSpyObj('assignmentDAO', dtoFunctions);
    //
    //         var rxChangeEmitter = new RxChangeEmitter<AssignmentDTO>();
    //         uut = new AssignmentStore(assignmentDAO, deviceDAO, rxChangeEmitter);
    //
    //         deviceModel = <DeviceModel>{};
    //         deviceModel._id = '1';
    //         deviceModel.assignments = [];
    //
    //         deviceDAO.get.and.returnValue(Promise.resolve(deviceModel));
    //         deviceDAO.update.and.returnValue(Promise.resolve());
    //         assignmentDAO.update.and.returnValue(Promise.resolve());
    //         assignmentDAO.create.and.returnValue(Promise.resolve());
    //     });
    //
    //     var createModel = (): AssignmentModel => {
    //         var item = new AssignmentDTO('1', 'Tag medicin', 'Normal', itemDate.toString(), [], true, itemRecurrenceRule, 'Du skal tage medicin');
    //         item.recurrenceId = 'r-1';
    //         return AssignmentConverter.newFromDTO(item);
    //     };
    //
    //     var timeTomorrow = () => {
    //         var now = new Date();
    //         var tomorrow = new Date(now.getTime() + 24 * 60 * 60);
    //         return tomorrow;
    //     };
    //
    //     var timeYesterday = () => {
    //         var now = new Date();
    //         var tomorrow = new Date(now.getTime() - 24 * 60 * 60);
    //         return tomorrow;
    //     };
    //
    //     it('no alarm, duedate tomorrow', (done) => {
    //         // arrange
    //         var item = createModel();
    //         assignmentDAO.find.and.returnValue(Promise.resolve([item]));
    //
    //         item.dueDate = timeTomorrow(); // should not warn when it is not due yet.
    //
    //
    //         // act
    //         uut.getAssignmentsToNotifyPromise('1').then((notifications: Array<string>) => {
    //             // assert
    //             expect(notifications.length).toEqual(0);
    //             done();
    //         }).catch((err) => {
    //             // the act call should not fail.
    //             throw err;
    //             done();
    //         });
    //     });
    //
    //     it('alarm set dueDate tomorrow', (done) => {
    //         // arrange
    //         var item = createModel();
    //         assignmentDAO.find.and.returnValue(Promise.resolve([item]));
    //         item.alarm = true;
    //         item.dueDate = timeTomorrow(); // should not warn when it is not due yet.
    //
    //         var seperator = ' skulle være udført ';
    //
    //         // act
    //         uut.getAssignmentsToNotifyPromise('1').then((notifications: Array<string>) => {
    //             // assert
    //             expect(notifications.length).toEqual(0);
    //             done();
    //         }).catch((err) => {
    //             // the act call should not fail.
    //             throw err;
    //             done();
    //         });
    //     });
    //
    //     it('alarm set dueDate yesterday', (done) => {
    //         // arrange
    //         var item = createModel();
    //         assignmentDAO.find.and.returnValue(Promise.resolve([item]));
    //         item.alarm = true;
    //         item.dueDate = timeYesterday(); // should not warn when it is not due yet.
    //
    //         var seperator = ' skulle være udført ';
    //
    //         // act
    //         uut.getAssignmentsToNotifyPromise('1').then((notifications: Array<string>) => {
    //             // assert
    //             expect(notifications.length).toEqual(1);
    //             expect(notifications[0]).toEqual(item.name + seperator + item.dueDate);
    //             done();
    //         }).catch((err) => {
    //             // the act call should not fail.
    //             throw err;
    //             done();
    //         });
    //     });
    // });
});
