/// <reference path="../../../../typings/browser.d.ts" />

import {AssignmentModel} from '../../dal/models/assignmentModel';
import {AssignmentDTO} from '../../../shared/dto/Assignment'; // used by the data access object to manage assignment objects in database
import AssignmentSchema = require('../../dal/schemas/assignmentSchema'); // used by the data access object to manage assignment objects in database

/**
 * Converter class converting Assignment correctly between Server and Database
 */
export class AssignmentConverter {
    static toDTO(from: AssignmentModel, to: AssignmentDTO) {
        to.id = from._id;
        to.name = from.name;
        to.description = from.description;
        to.priority = from.priority; // the Enum is a string
        to.finished = from.finished || false;
        to.date = from.date.toISOString();
        to.users = from.users;
        to.recurring = from.recurring || false;
        to.recurringInfo = from.recurringInfo || '';
        to.alarm = from.alarm;
        to.roles = from.roles;
        to.userInput = from.userInput;
        to.userInputNeeded = from.userInputNeeded;
    }

    static newToDTO(from: AssignmentModel): AssignmentDTO {
        var dto = new AssignmentDTO(from._id,
            from.name,
            from.priority,
            from.date.toISOString(),
            from.users,
            from.roles,
            from.userInputNeeded,
            from.recurring,
            from.recurringInfo,
            from.description,
            from.userInput,
            from.finished || false);
        dto.alarm = from.alarm;
        return dto;
    }

    static fromDTO(from: AssignmentDTO, to: AssignmentModel) {
        to._id = from.id;
        to.name = from.name;
        to.description = from.description;
        to.priority = from.priority; // the Enum is a string
        to.finished = from.finished || false;
        to.users = from.users;
        to.roles = from.roles;
        to.userInputNeeded = from.userInputNeeded;
        to.userInput = from.userInput;
        to.recurring = from.recurring || false;
        to.recurringInfo = from.recurringInfo || '';
        to.alarm = from.alarm;
        // conversion of dates in try-catch to avoid exceptions, default is unix-0
        try {
            to.date = new Date(from.date);
        } catch (err) {

        }
    }

    static newFromDTO(from: AssignmentDTO): AssignmentModel {
        var to: any = {};
        this.fromDTO(from, to);
        var model = new AssignmentSchema(to) as AssignmentModel;
        return model;
    }
}
