/// <reference path="../../../../typings/browser.d.ts" />

import DeviceSchema = require('../../dal/schemas/DeviceSchema');
import {DeviceModel} from '../../dal/models/DeviceModel';
import {Device} from '../../../shared/dto/device';

/**
 * Converter class converting Device correctly between Server and Database
 */
export class DeviceConverter {
    static toDTO(from: DeviceModel, to: Device) {
        to.users = from.users;
        to.id = from._id;
    }

    static newToDTO(from: DeviceModel): Device {
        return new Device(from._id, from.password, from.users, from.assignments);
    }

    static fromDTO(from: Device, to: DeviceModel) {
        to.users = from.users;
        to._id = from.id;
        to.password = from.password;
    }

    static newFromDTO(from: Device): DeviceModel {
        var to: any = {};
        this.fromDTO(from, to);
        var model = new DeviceSchema(to) as DeviceModel;
        return model;
    }
}
