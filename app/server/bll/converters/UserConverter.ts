/// <reference path="../../../../typings/browser.d.ts" />

import {UserDTO} from '../../../shared/dto/user';
import {UserModel} from './../../dal/models/UserModel';
import UserSchema = require('../../dal/schemas/UserSchema');

/**
 * Converter class converting User correctly between Server and Database
 */
export class UserConverter {
    static toDTO(from: UserModel, to: UserDTO) {
        to.firstName = from.firstName;
        to.lastName = from.lastName;
        to.middleName = from.middleName;
        to.id = from._id;
    }

    static newToDTO(from: UserModel): UserDTO {
        return new UserDTO(from._id, from.firstName, from.lastName, from.middleName);
    }

    static fromDTO(from: UserDTO, to: UserModel) {
        to.firstName = from.firstName;
        to.lastName = from.lastName;
        to.middleName = from.middleName;
        to._id = from.id;
    }

    static newFromDTO(from: UserDTO): UserModel {
        var to: any = {};
        this.fromDTO(from, to);
        var model = new UserSchema(to) as UserModel;
        return model;
    }
}
