/// <reference path="../../../typings/main.d.ts" />
import {BasicCRUDPromise} from '../../shared/interfaces/BasicCrudPromise';
import {ConverterCRUD} from './../../shared/bll/crud/ConverterCRUD';
import {RxChangeEventSource, GenericRx} from './GenericRx';
import {UserDTO} from '../../shared/dto/user';
import {UserModel} from '../dal/models/UserModel';
import {UserConverter} from './converters/UserConverter';
import {IUserStorePromise} from '../interfaces/userStore';
import {IDeviceDAO} from '../dal/interfaces/IDeviceDAO';

export class UserStore implements IUserStorePromise {
    converted: BasicCRUDPromise<UserDTO>;
    wrapped: GenericRx<UserDTO>;

    constructor(protected dao: BasicCRUDPromise<UserModel>, protected deviceDAO: IDeviceDAO,
                protected changeSource: RxChangeEventSource<UserDTO>) {
        this.converted = new ConverterCRUD(dao,
            (fromDTO) => UserConverter.newFromDTO(fromDTO),
            (fromModel) => UserConverter.newToDTO(fromModel));

        this.wrapped = new GenericRx(this.converted, changeSource);
    }

    getUsers(deviceID: string): Promise<Array<UserDTO>> {
        return this.deviceDAO.getUsers(deviceID).then((users) =>
            users.map(user => UserConverter.newToDTO(user)));
    }

    getUser(id): Promise<UserDTO> {
        return this.converted.get(id);
    }

    createUser(item: UserDTO, deviceID: string): Promise<any> {
        return new Promise((resolve, reject) => {
            var devicePromise = this.deviceDAO.get(deviceID);

            // First we create the user
            this.wrapped.create(item, deviceID).then(() => {
                    // If that succeedes, then we add the user to the device.users list
                    // Note: This can only be done if insert succeeded, otherwise it can be used as a way to gain access to users.
                    devicePromise.then((device) => {
                        // Add user id to device list.
                        device.users.push(item.id);
                        // update the device
                        this.deviceDAO.update(device).then(() => {
                            // everything happened as expected
                            resolve();
                        }).catch((err) => reject(err + ', Unable to update device'));
                    }).catch((err) => reject(err + ', Unable to get device'));
                })
                .catch((err) => reject(err + ', Unable to create user'));
        });
    }

    updateUser(item: UserDTO, deviceID: string): Promise<any> {
        return this.wrapped.update(item, deviceID);
    }
}
