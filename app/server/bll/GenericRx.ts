/// <reference path="../../../typings/main.d.ts" />
import * as Rx from 'rx';
import {BasicCRUDPromise} from '../../shared/interfaces/BasicCrudPromise';

interface RxChangeEventSource<T> {
    create(item: T, filter = ''): void;
    update(item: T, filter = ''): void;
    delete(item: T, filter = ''): void;
}

interface RxChangeEvents<T> {
    getUpdateHandler(): Rx.Observable<T>;
    getCreatedHandler(): Rx.Observable<T>;
    getDeleteHandler(): Rx.Observable<T>;
}

class RxChangeEmitter<T> implements RxChangeEvents<T>, RxChangeEventSource<T> {
    protected updatedHandler: Rx.Subject<T> = new Rx.Subject<T>();
    protected createdHandler: Rx.Subject<T> = new Rx.Subject<T>();
    protected deletedHandler: Rx.Subject<T> = new Rx.Subject<T>();

    create(item, filter = '') {
        var v = {value: item, filter: filter};
        this.createdHandler.onNext(v);
    }

    update(item, filter = '') {
        var v = {value: item, filter: filter};
        this.updatedHandler.onNext(v);
    }

    delete(item, filter = '') {
        var v = {value: item, filter: filter};
        this.deletedHandler.onNext(v);
    }

    getUpdateHandler(): Rx.Observable<T> {
        return this.updatedHandler as Rx.Observable<T>;
    }

    getCreatedHandler(): Rx.Observable<T> {
        return this.createdHandler as Rx.Observable<T>;
    }

    getDeleteHandler(): Rx.Observable<T> {
        return this.deletedHandler as Rx.Observable<T>;
    }
}

class GenericRx<T> implements BasicCRUDPromise<T> {
    constructor(protected wrapped: BasicCRUDPromise<T>, protected socket: RxChangeEmitter<T>) {
    }

    getAll(): Promise<Array<T>> {
        return this.wrapped.getAll();
    }

    get(id: String): Promise<T> {
        return this.wrapped.get(id);
    }

    find(query: any, populate?: Array<String>): Promise<Array<T>> {
        return this.wrapped.find(query, populate);
    }

    create(item, filter = ''): Promise<any> {
        var promise = this.wrapped.create(item);

        return promise.then(() => {
            this.socket.create(item, filter);
        });
    }

    update(item, filter = ''): Promise<any> {
        var promise = this.wrapped.update(item);

        return promise.then(() => {
            this.socket.update(item, filter);
        });
    }

    delete(item, filter = ''): Promise<any> {
        var promise = this.wrapped.delete(item);

        return promise.then(() => {
            this.socket.delete(item, filter);
        });
    }
}

export {RxChangeEvents, RxChangeEventSource, RxChangeEmitter, GenericRx};
