/// <reference path="../../../../typings/browser.d.ts" />
import {BasicCRUDPromise} from '../../../shared/interfaces/BasicCrudPromise';

export class BasicCRUDStub<T> implements BasicCRUDPromise<T> {
    constructor(public items: Array<T>) {
    }

    getAll(): Promise<Array<T>> {
        return Promise.resolve(this.items);
    }

    find(query: any, populate?: Array<String>): Promise<Array<T>> {
        return Promise.resolve(this.items);
    }

    get(id: String): Promise<T> {
        return Promise.resolve(this.items.find((elm) => elm.id === id));
    }

    create(item: T): Promise<any> {
        this.items.push(item);
        return Promise.resolve();
    }

    update(item: T): Promise<any> {
        var index = this.items.findIndex((elm) => elm.id === item.id);
        if (index !== -1) {
            this.items[index] = item;
        }

        return Promise.resolve();
    }

    delete(item: T): Promise<any> {
        return Promise.resolve();
    }
}
