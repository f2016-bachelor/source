/// <reference path="../../../../typings/browser.d.ts" />

import {IDeviceDAO} from '../../dal/interfaces/IDeviceDAO';
import {UserModel} from '../../dal/models/UserModel';
import {DeviceModel} from '../../dal/models/DeviceModel';
import {AssignmentModel} from '../../dal/models/assignmentModel';
export class DeviceDAOStub implements IDeviceDAO {

    protected dateStamp = new Date();

    constructor(public items: Array<DeviceModel>, public users: Array<DeviceModel>) {
    }

    getAll(): Promise<Array<DeviceModel>> {
        return new Promise((resolve, reject) => {
            resolve(this.items);
        });
    }

    find(query: any, populate?: Array<String>): Promise<Array<DeviceModel>> {
        return undefined;
    }

    get(id: String): Promise<DeviceModel> {
        return new Promise((resolve, reject) => {
            resolve(this.items.find((elm) => elm.id === id));
        });
    }

    create(item: DeviceModel): Promise<any> {
        return new Promise((resolve, reject) => {
            resolve(this.items.push(item));
        });
    }

    update(item: DeviceModel): Promise<any> {
        return new Promise((resolve, reject) => {
            var index = this.items.find((elm) => elm.id === item.id);
            if (index !== -1) {
                this.items[index] = item;
            }
            resolve(item);
        });
    }

    delete(item: DeviceModel): Promise<any> {
        return undefined;
    }

    getUsers(id: String): Promise<Array<UserModel>> {
        return undefined;
    }

    getAssignments(id: String): Promise<Array<AssignmentModel>> {
        return undefined;
    }
}
