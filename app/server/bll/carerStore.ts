/// <reference path="../../../typings/main.d.ts" />

import {ICarerStorePromise} from '../interfaces/ICarerStore';

export class CarerStore implements ICarerStorePromise {

    constructor(protected path: string, protected io: SocketIO.Server) {
    }

    remoteLoginCarer(id: string): Promise<boolean> {
        this.io.to(id).emit(this.path, true);

        return Promise.resolve(true);
    }
}
