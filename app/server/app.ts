/// <reference path="../../typings/main.d.ts" />
// token secret - shh!
var verySecretSecret = process.env.JWT_KEY || 'xxx'; // get a key from environment to use. Default is xxx
var port = process.env.PORT || 8080; 				// set the port
var dbConnectionString = process.env.MONGODB_URI || 'mongodb://localhost/bachelorDb';
/**
 ************* Node Js Server Setup using Express Js ***********************
  ***************************************************************************
 ***************************************************************************
 */

import express = require('express');
import http = require('http');
import socketIO = require('socket.io');
import bodyParser = require('body-parser');
import DBConnection = require('./dal/DBconnection');
import path = require('path'); // used to resolve relative paths
import Mongoose = require('mongoose');
import Config from '../shared/presentation/config';

var app: express.Express = express();
var server = http.Server(app);
var io = socketIO(server);

// Setup connection to DB
DBConnection.setupConnection(dbConnectionString);

app.use(bodyParser.json()); // So that JSON data can be parsed from/to server
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies
app.use(express.static(path.join(__dirname, '../public'))); // In production, the static will be served from here

console.log(__dirname);

// device data access object
import {DeviceDAO} from './dal/DAO/DeviceDAO';
import DeviceSchema = require('./dal/schemas/DeviceSchema'); // used by the data access object to manage assignment objects in database
var deviceDAOInstance = new DeviceDAO(DeviceSchema);

import jwt = require('jwt-simple');
// setup passport
import passport = require('passport');
require('./presentation/rest/passportConfig')(app, passport, jwt, deviceDAOInstance, verySecretSecret);
require('./presentation/socket.io/setup')(io, verySecretSecret);

// Error handling middleware
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Error in request');
});

// Setup data access objects
import {AssignmentDAO} from './dal/DAO/assignmentDAO';
import mongooseAssignmentModel = require('./dal/schemas/assignmentSchema'); // used by the data access object to manage assignment objects in database

var assignmentDaoInstance = new AssignmentDAO(mongooseAssignmentModel);


import {UserDAO} from './dal/DAO/UserDAO';
import UserSchema = require('./dal/schemas/UserSchema'); // used by the data access object to manage assignment objects in database
var userDAOInstance = new UserDAO(UserSchema);

import {RxChangeEmitter} from './bll/GenericRx';
import {UserDTO} from '../shared/dto/user';
import {AssignmentModel} from './dal/models/assignmentModel';

// rx event listeners
var assignmentRx = new RxChangeEmitter<AssignmentModel>();
var userRx = new RxChangeEmitter<UserDTO>();

import {AssignmentStore} from './bll/assignmentStore';
var assignmentStore = new AssignmentStore(assignmentDaoInstance, deviceDAOInstance, assignmentRx);

import {UserStore} from './bll/userStore';
var userStore = new UserStore(userDAOInstance, deviceDAOInstance, userRx);

import {DeviceStore} from './bll/deviceStore';
var deviceStore = new DeviceStore(deviceDAOInstance);

import {CarerStore} from './bll/carerStore';
var carerStore = new CarerStore(Config.api.login.rfid, io);

// ***Setup event logger DAO *************
import {LoggingDAO} from './dal/DAO/loggingDAO';
import logModel = require('./dal/schemas/logSchema'); // The Mongoose LogModel through witch the LoggingDAO can save to the database
LoggingDAO.setLogModel(logModel); // Now the logger is ready!
// ******************************************

// *********Setup REST routes on /api ***********************************************************
// /api/items   routes for assignments
import rest_assignments = require('./presentation/rest/assignmentRoutes');
import {AssignmentLogger} from './bll/loggers/assignmentLogger'; // The business object used for Assignment-specific logs
rest_assignments(app, assignmentStore, new AssignmentLogger, passport); // passing the express app as routes are being registered on that instance.

// /api/users
import rest_users = require('./presentation/rest/userRoutes');
import {UserLogger} from './bll/loggers/userLogger'; // The business object used for User-specific logs
rest_users(app, userStore, new UserLogger, passport); // passing the express app as routes are being registered on that instance.

// /api/devices
import rest_devices = require('./presentation/rest/deviceRoutes');
import {DeviceLogger} from './bll/loggers/deviceLogger'; // The business object used for Device-specific logs
rest_devices(app, deviceStore, new DeviceLogger); // passing the express app as routes are being registered on that instance.

// /api/login
import rest_login = require('./presentation/rest/loginRoutes');
import {LoginLogger} from './bll/loggers/loginLogger'; // The business object used for Login-specific logs
rest_login(app, carerStore, new LoginLogger, passport, jwt, verySecretSecret);

// /api/notifications
import rest_notif = require('./presentation/rest/notificationRoutes');
rest_notif(app, assignmentStore, passport);

// /api/logs
import rest_logs = require('./presentation/rest/logRoutes');
rest_logs(app, passport);

// setup socket.io endpoints

import socket_assignments = require('./presentation/socket.io/assignmentSocket');
socket_assignments(io, assignmentStore, assignmentRx);

import socket_users = require('./presentation/socket.io/userSocket');
socket_users(io, userStore, userRx);

import {socketDebugger} from '../shared/bll/stub/socket-debug';
socketDebugger(io);

// ************************************************************************


// Start the Node Js server on configured port number
server.listen(port, () => {
    console.log('REST API listening on port ' + port);
});
