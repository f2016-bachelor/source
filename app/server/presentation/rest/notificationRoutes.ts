/// <reference path='../../../../typings/main.d.ts' />
import {Express} from 'express';
var dateFormat = require('dateformat');
import Config from '../../../shared/presentation/config';
import {AssignmentStore} from '../../bll/assignmentStore';
import {AssignmentDTO} from '../../../shared/dto/Assignment';

/**
 * Node Js module used to configure REST endpoints for Notification specific tasks
 * @param app The Express Js App
 * @param assignmentStore Business object for assignments - follows the CRUD pattern
 * @param passport The Node Js Passport module. The authentication mechanism used in this app.
 */
module.exports = (app: Express, assignmentStore: AssignmentStore, passport) => {
    app.route(Config.api.notifications.this)

        // validate request using passport
        .get(passport.authenticate('jwt', {session: false}), (req, res) => {
            // if valid, get assignments that are required to send a notification about
            assignmentStore.getAssignmentsToNotifyPromise(req.deviceID)
                .then(
                    (items: Array<AssignmentDTO>) => {
                        if (items != null) {
                            // send notification
                            res.send(items);
                        } else {
                            res.send([]);
                        }
                    })
                .catch((err) => {
                    res.status(500).send('Could not retrieve notifications due to: ' + err);
                });
        });

    app.route(Config.api.notifications.text)

        // validate request using passport
        .get(passport.authenticate('jwt', {session: false}), (req, res) => {
            // if valid, get assignments that are required to send a notification about
            assignmentStore.getAssignmentsToNotifyPromise(req.deviceID)
                .then(
                    (items: Array<AssignmentDTO>) => {
                        if (items != null) {
                            // send notification
                            res.send(items.map((item: AssignmentDTO) => {
                                return item.users[0].firstName + ' ' + item.users[0].lastName + ': ' + item.name + ' skulle være udført klokken ' + dateFormat(item.date, 'HH:MM');
                            }));
                        }
                    })
                .catch((err) => {
                    res.status(500).send('Could not retrieve notifications due to: ' + err);
                });
        });
};
