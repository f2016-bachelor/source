/* SETUP PASSPORT STRATEGIES FOR USER AUTHENTICATION */

var localStrategy = require('passport-local').Strategy;
var jwtStrategy = require('passport-jwt').Strategy;
var extractJwt = require('passport-jwt').ExtractJwt;
import * as bcrypt from 'bcrypt-nodejs';
import {BasicCRUDPromise} from '../../../shared/interfaces/BasicCrudPromise';
import {DeviceModel} from '../../dal/models/DeviceModel';

/**
 *
 * @param app The Express Js App used for REST API
 * @param passport The Node Js Passport module. The authentication mechanism used in this app.
 * @param jwt The Node Js jwt-simple module
 * @param deviceDAO Data access object used to retrieve/update/create devices
 * @param verySecretSecret Json Web Token secret
 */
module.exports = function (app, passport, jwt, deviceDAO: BasicCRUDPromise<DeviceModel>, verySecretSecret) {
    /**
     * Register Local strategy to passport instance
     */
    passport.use(new localStrategy(
        {
            // specifies that login is with id, instead of username
            usernameField: 'id'
        },
        // local strategy, for id+password login
        function (id, password, done) {
            deviceDAO.get(id).then( // tries to match id on device database
                (device) => { // found device -> validate password
                    // bcrypt used to compare plaintext password to hashed password saved on device
                    bcrypt.compare(password, device.password, function (err, res) {
                        if (res === true) { // if password matches hashed password on device
                            return done(null, device); // match = done called back with matching device
                        } else {
                            return done(null, false); // invalid password, login not authorized
                        }
                    });
                })
                .catch((err) => { // error finding device
                    return done(null, false); // login not authorized
                });
        })
    );

    /**
     * JsonWebToken strategy configuration
     * @type {{secretOrKey: any, tokenQueryParameterName: string, authScheme: string, jwtFromRequest: any}}
     */
    const JWT_STRATEGY_CONFIG = {
        secretOrKey: verySecretSecret, // json secret
        tokenQueryParameterName: 'access_token',
        authScheme: 'Bearer',
        algorithms: ['HS256'],
        // tells passport-jwt to extract token from header with scheme 'Bearer'
        jwtFromRequest: extractJwt.fromAuthHeaderWithScheme('Bearer'),
        passReqToCallback: true
    };

    /**
     * strategy: tries to find id collected from auth-header in device database. if found, passport allows access
     * @param req The HTTP request object
     * @param jwtPayload payload collected using JWT_STRATEGY_CONFiG
     * @param done
     */
    const onJwtStrategyAuth = (req, jwtPayload, done) => {
        var payload = {'id': jwtPayload.id};
        req.deviceID = jwtPayload.id;
        done(null, payload);
    };

    /**
     * Register JWT strategy to passport instance
     */
    passport.use(new jwtStrategy(JWT_STRATEGY_CONFIG, onJwtStrategyAuth));

    // initializes passport
    app.use(passport.initialize());
    // app.use(passport.session());
};
