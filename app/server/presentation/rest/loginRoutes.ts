/// <reference path='../../../../typings/main.d.ts' />
import {Express} from 'express';
import {ILogger} from '../../bll/loggers/iLogger';
import {LogEventTypes} from '../../../shared/logEventTypes';
import Config from '../../../shared/presentation/config';
import {ICarerStorePromise} from '../../interfaces/ICarerStore';

/**
 * Defines Login routes
 * @param app The Express Js App
 * @param logger The logger object that handles the logging of events
 * @param passport The Node Js Passport module. The authentication mechanism used in this app.
 * @param jwt The jwt-simple module instans
 */
module.exports = (app: Express, carerStore: ICarerStorePromise, logger: ILogger, passport, jwt, verySecretSecret) => {

    // passport.authenticate uses local strategy to verify entered user credentials from client
    // only if valid a JWT token is created and saved
    app.route(Config.api.login.this)
        .post(passport.authenticate('local', {session: false}), function (req, res) {
            var token = jwt.encode({id: req.body.id}, verySecretSecret);
            // Log that a device has logged on
            logger.logEvent({}, req.body.id, LogEventTypes.loggedOn).then(() => res.json({token: token}))
                .catch((err) => {
                    console.log('Error logging in! Message: ', err);
                    res.status(500).send('Log in failure');
                })
            ;
        });

    app.route(Config.api.login.rfid)
        .post(passport.authenticate('jwt', {session: false}), function (req, res) {
            //
            carerStore.remoteLoginCarer(req.deviceID);
            res.status(200).send('ok');
        });
};
