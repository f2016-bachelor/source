/// <reference path='../../../../typings/main.d.ts' />
import {Express} from 'express';
import Config from '../../../shared/presentation/config';
import {LoggingDAO} from '../../dal/DAO/loggingDAO';

/**
 * Node Js module used to configure REST endpoint for logs - for retrievieving log objects
 * @param app The Express Js Node Module for REST
 * @param passport The Node Js Passport module. The authentication mechanism used in this app
 */
module.exports = (app: Express, passport) => {
    app.route(Config.api.logs.this)
        // Get all logs for the current device ID (specified in the req object)
        .get(passport.authenticate('jwt', {session: false}), (req, res) => {
            LoggingDAO.getLogsForDevice(req.deviceID)
                .then(
                    (logItems) => {
                        if (logItems != null) {
                            res.send(logItems);
                        }
                    })
                .catch((err) => {
                    res.status(500).send('Could not retrieve logs! Error was: ' + err);
                });
        });
};
