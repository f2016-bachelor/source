/// <reference path='../../../../typings/main.d.ts' />
import {Express} from 'express';
import Config from '../../../shared/presentation/config';
import {ILogger} from '../../bll/loggers/iLogger';
import {LogEventTypes} from '../../../shared/logEventTypes';
import {IDeviceStorePromise} from '../../interfaces/IDeviceStore';

/**
 * Node Js module used to configure REST endpoints for device specific tasks
 * @param app The Express Js App used for REST API
 * @param deviceStore Data access object used to retrieve/update/create devices
 * @param logger The logger object that handles the logging of events
 */
module.exports = (app: Express, deviceStore: IDeviceStorePromise, logger: ILogger) => {
    app.route(Config.api.devices.this)
        // post device - device object should be in the request body
        .post((req, res) => {
            deviceStore.createDevice(req.body)
                .then(
                    (resp) => {
                        // Log that a device has been created!
                        logger.logEvent(req.body, req.body.id, LogEventTypes.deviceCreate).then(() => res.send()).catch((err) => {
                            console.log('Error logging a device create. Message: ', err);
                            res.status(500).send('Error logging a device create');
                        });
                    })
                .catch((err) => {
                    res.status(500).send(err);
                });
        });
};
