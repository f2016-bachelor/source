/// <reference path='../../../../typings/main.d.ts' />
import {Express} from 'express';
import Config from '../../../shared/presentation/config';
import {AssignmentStore} from '../../bll/assignmentStore';
import {ILogger} from '../../bll/loggers/iLogger';
import {LogEventTypes} from '../../../shared/logEventTypes';

/**
 * Node Js module used to configure REST endpoints for assignment specific tasks
 * @param app The Express Js App
 * @param assignmentStore Business object for assignments - follows the CRUD pattern
 * @param logger The logger object that handles the logging of events
 * @param passport The Node Js Passport module. The authentication mechanism used in this app.
 */
module.exports = (app: Express, assignmentStore: AssignmentStore, logger: ILogger, passport) => {
    app.route(Config.api.assignments.this)
        // Get all items
        .get(passport.authenticate('jwt', {session: false}), (req, res) => {
            assignmentStore.getAssignmentsPromise(req.deviceID)
                .then(
                    (assignments) => {
                        if (assignments != null) {
                            res.status(200).send(assignments);
                        }
                    })
                .catch((err) => {
                    res.status(500).send('Could not retrieve assignments');
                });
        })

        // post assignment - assignment object should be in the request body
        .post(passport.authenticate('jwt', {session: false}), (req, res) => {
            assignmentStore.createAssignmentPromise(req.body, req.deviceID)
                .then(
                    () => {
                        // log the event of adding an assignment object. Passing the saved assignment and the device ID
                        logger.logEvent(req.body, req.deviceID, LogEventTypes.assignmentCreate, req.body.users[0]).then(() => {
                            res.status(200).send();
                        }).catch((err) => console.log('Error logging assignment post event! ' + err));
                    })
                .catch((err) => {
                    console.log('Error for post:' + err);
                    res.status(500).send('Could not retrieve assignments');
                });

        });

    app.route(Config.api.assignments.this + '/:uuid')
        // find assignment by id (passed are request parameter)
        .get(passport.authenticate('jwt', {session: false}), function (req, res) {
            assignmentStore.getAssignmentPromise(req.params.uuid, req.deviceID)
                .then(
                    (item) => {
                        if (item != null) {
                            res.status(200).send(item);
                        }
                    })
                .catch((err) => {
                    console.log('Error for get:' + err);
                    res.status(500).send('Could not retrieve assignment');
                });
        })
        // Update an assignment - new assignment object should be in request body
        .put(passport.authenticate('jwt', {session: false}), (req, res) => {
            assignmentStore.updateAssignmentPromise(req.body, req.deviceID)
                .then(
                    () => {
                        // log the event of updating an assignment object. Passing the updated assignment and the device ID
                        var event: string = (req.body.finished ? LogEventTypes.assignmentFinish : LogEventTypes.assignmentUpdate);
                        logger.logEvent(req.body, req.deviceID, event, req.body.users[0]).then(() => {
                            res.status(200).send();
                        }).catch((err) => console.log('Error logging assignment update event! ', err));
                    })
                .catch((err) => {
                    console.log('Error for put: ', err);
                    res.status(500).send('Could not update assignment');
                });
        })
        .delete(passport.authenticate('jwt', {session: false}), (req, res) => {
            assignmentStore.deleteAssignmentPromise(req.params.uuid as String, req.deviceID)
                .then(
                    () => {
                        // log the event of updating an assignment object. Passing the updated assignment and the device ID
                        // TODO: Fix logging of assignment-delete events.
                        // logger.logEvent(req.params.uuid, req.deviceID, LogEventTypes.assignmentDelete, null).then(() => {
                        //     res.status(200).send();
                        // }).catch((err) => console.log('Error logging assignment post event! ' + err));
                    }
                )
                .catch((err) => {
                    console.log('Error for delete: ', err);
                    res.status(500).send('Could not update assignment');
                });
        });

};
