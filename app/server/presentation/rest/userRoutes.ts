/// <reference path='../../../../typings/main.d.ts' />
import {Express} from 'express';
import Config from '../../../shared/presentation/config';
import {ILogger} from '../../bll/loggers/iLogger';
import {LogEventTypes} from '../../../shared/logEventTypes';
import {IUserStorePromise} from '../../interfaces/userStore';

/**
 *
 * @param app The Express Js App
 * @param userStore Business object User object. Has CRUD operations
 * @param logger The logger object that handles the logging of events
 * @param passport The Node Js Passport module. The authentication mechanism used in this app.
 */
module.exports = (app: Express, userStore: IUserStorePromise, logger: ILogger, passport) => {
    app.route(Config.api.users.this)
        // Get all items
        .get(passport.authenticate('jwt', {session: false}), (req, res) => {
            userStore.getUsers(req.deviceID)
                .then(
                    (items) => {
                        if (items != null) {
                            res.send(items);
                        }
                    })
                .catch((err) => {
                    res.status(500).send('Could not retrieve users');
                });
        })

        // post user - user object should be in the request body
        .post(passport.authenticate('jwt', {session: false}), (req, res) => {
            userStore.createUser(req.body, req.deviceID)
                .then(
                    () => {
                        // Log the event of creating a new user
                        logger.logEvent(req.body, req.deviceID, LogEventTypes.userCreate, req.body.id)
                            .then(() => res.send()).catch((err) => console.log('Error logging user post event! Message: ', err));
                    })
                .catch((err) => {
                    console.log('Error for post:' + err);
                    res.status(500).send('Could not save user');
                });
        });

    app.route(Config.api.users.this + '/:uuid')
        // find assignment by id (passed are request parameter)
        .get(passport.authenticate('jwt', {session: false}), (req, res) => {
            userStore.getUser(req.params.uuid)
                .then(
                    (item) => {
                        if (item != null) {
                            res.send(item);
                        }
                    })
                .catch((err) => {
                    console.log('Error for get:' + err);
                    res.status(500).send('Could not retrieve user');
                });
        })
        // Update an assignment - new assignment object should be in request body
        .put(passport.authenticate('jwt', {session: false}), (req, res) => {
            userStore.updateUser(req.body, req.params.uuid)
                .then(
                    (item) => {
                        // Log the event of updating aa user
                        logger.logEvent(req.body, req.deviceID, LogEventTypes.userUpdate, req.body.id)
                            .then(() => res.send()).catch((err) => console.log('Error logging user put event! Message: ', err));
                        res.send();
                    })
                .catch((err) => {
                    console.log('Error for put:' + err);
                    res.status(500).send('Could not update user');
                });
        });
};
