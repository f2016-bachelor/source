/// <reference path="../../../../typings/main.d.ts" />

import Config from '../../../shared/presentation/config';
import {GenericSocketIOEmitter} from './genericSocketIOEmitter';
import {RxChangeEvents} from '../../bll/GenericRx';
import {IUserStorePromise} from '../../interfaces/userStore';
import {UserDTO} from '../../../shared/dto/user';

module.exports = (io: SocketIO.Server, userStore: IUserStorePromise, assignmentRx: RxChangeEvents<UserDTO>) => {
    var eventEmitter = new GenericSocketIOEmitter(io, Config.api.users.this, assignmentRx);
    eventEmitter.register();

    io.on('authenticated', (socket) => {
        var deviceID = socket.decoded_token.id;
        // this socket is authenticated, we are good to handle more events from it.
        userStore.getUsers(deviceID)
            .then((items) => io.to(deviceID).emit(Config.api.users.this + '/all', items));
    });
};
