/// <reference path="../../../../typings/main.d.ts" />
import * as socketioJwt from 'socketio-jwt';

module.exports = (io: SocketIO.Server, verySectet: string) => {
    io.use(socketioJwt.authorize({
        secret: verySectet,
        handshake: true
    }));
    io.on('connection', socketioJwt.authorize({
        secret: verySectet,
        timeout: 15000
    }));

    io.on('connection', (socket) => {
        // be able to do io.to('device.id').emit(..)
        socket.join(socket.decoded_token.id);
    });
};
