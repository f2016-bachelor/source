/// <reference path="../../../../typings/main.d.ts" />
import {RxChangeEvents} from '../../bll/GenericRx';

export class GenericSocketIOEmitter<T> {
    constructor(protected io: SocketIO.Server, protected path: string, protected changeEvents: RxChangeEvents<T>) {
    }

    register() {
        var io = this.io;
        this.changeEvents.getCreatedHandler().subscribe((elm) => {
            if (elm.filter !== '') {
                io.to(elm.filter).emit(this.path + '/create', elm.value);
            } else {
                io.emit(this.path + '/create', elm.value);
            }
        });

        // updated
        this.changeEvents.getUpdateHandler().subscribe((elm) => {
            if (elm.filter !== '') {
                io.to(elm.filter).emit(this.path + '/update', elm.value);
            } else {
                io.emit(this.path + '/update', elm.value);
            }
        });

        // deleted
        this.changeEvents.getDeleteHandler().subscribe((elm) => {
            if (elm.filter !== '') {
                io.to(elm.filter).emit(this.path + '/delete', elm.value);
            } else {
                io.emit(this.path + '/delete', elm.value);
            }
        });
    }
}
