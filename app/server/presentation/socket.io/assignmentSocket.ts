/// <reference path="../../../../typings/main.d.ts" />

import Config from '../../../shared/presentation/config';
import {GenericSocketIOEmitter} from './genericSocketIOEmitter';
import {RxChangeEvents} from '../../bll/GenericRx';
import {IAssignmentStorePromise} from '../../interfaces/IAssignmentStore';
import {AssignmentDTO} from '../../../shared/dto/Assignment';

module.exports = (io: SocketIO.Server, assignmentStore: IAssignmentStorePromise, assignmentRx: RxChangeEvents<AssignmentDTO>) => {
    var eventEmitter = new GenericSocketIOEmitter(io, Config.api.assignments.this, assignmentRx);
    eventEmitter.register();

    io.on('authenticated', (socket) => {
        var deviceID = socket.decoded_token.id;
        // this socket is authenticated, we are good to handle more events from it.
        assignmentStore.getAssignmentsPromise(deviceID)
            .then((items) => io.to(deviceID).emit(Config.api.assignments.this + '/all', items));
    });
};
