import {routes}  from './routes';
import {LogController} from './logController';

var logModule: ng.IModule = angular.module('log', ['ui.router']);

logModule
    .config(routes)
    .controller('LogController', LogController);
export {logModule};
