import {LogStore} from '../bll/stores/logStore';
import {LogModel} from '../bll/models/logModel';
import {IUserStore} from '../bll/interfaces/userStore';

/**
 * Controller for logs page
 */
export class LogController {

    static $inject = ['logStore', 'userStore'];

    /**
     * Constructor LogController. Starts by geting all the logs
     * @param logStore
     */
    constructor(protected logStore: LogStore,  protected userStore: IUserStore) {
        this.users = userStore.getUsers();
        this.getAllLogs();
    }

    // ***** View-bindable attributes ***** \\
    public logList: Array<LogModel> = [];

    // Date order configuration
    public dateShowText: string = 'Vis nyeste først';
    public dateReverseOrder: boolean = false;


    // ***** View-bindable methods ***** \\
    getAllLogs() {
        this.logStore.getAllLogs().then((logs) => {
            this.logList = logs;
        });
    }

    getUsername(id) {
        var name = this.users.filter((m) => m.id === id).map((m) => {
            var n = m.firstName + ' ' + m.lastName;
            return n;
        });
        return name[0];
    }


    // Method for changing the order of the date in the list of logs
    changeDateOrder() {
        this.dateReverseOrder = !this.dateReverseOrder;
        this.dateShowText = this.dateReverseOrder ? 'Vis ældste først' : 'Vis nyeste først';
    }

    public users;
}

