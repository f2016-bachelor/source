/// <reference path="../../../typings/browser.d.ts" />
import {IStateProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider'];

/**
 * Set up routes for log page
 * @param $stateProvider for creating states
 */
function routes($stateProvider: IStateProvider) {
    'use strict';
    $stateProvider
        .state('main.logs', {
            url: '/logs', // url
            templateUrl: 'client/log/log.html', // view
            controller: 'LogController', // controller
            controllerAs: 'logCtrl',
        });
}

export {routes}
