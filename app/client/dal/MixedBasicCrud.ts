/// <reference path="../../../typings/browser.d.ts" />

import * as Rx from 'rx';
import {BasicCRUD} from '../../shared/interfaces/BasicCrud';
import {CRUD} from './../../shared/bll/crud/CRUDActions';

export abstract class MixedBasicCRUD<T> implements BasicCRUD<T> {
    protected itemMap = new Map([]); // Ecmascript 6 Map object with key/value paairs
    protected itemObservableArray: Rx.BehaviorSubject<Array<T>> = new Rx.BehaviorSubject<Array<T>>([]);

    /**
     *
     * @param rootAPI   Rest api endpoint to read items from.
     * REST-Put will be based on this path too: rootAPI + '/<uuid>'
     * Socket.io will use rootAPI + '/create' and '/update' to get events
     * @param $http     Angular $http service, uses get, post and put methods
     * @param socketObservable    Rx.Observable socket, used to subscribe to socket.io events
     * @param fromDTO   function (item: OBJ) => DTO function that can convert the local data type to a DTO for transport
     * @param toDTO     function (item: DTO) => Obj function that can convert the dto from transport to a local type
     */
    constructor(protected rootAPI: string, protected $http: angular.IHttpService, protected socketObservable: Rx.Observable<SocketIOClient.Socket>, public fromDTO: Function, public toDTO: Function) {
        socketObservable.forEach((socket) => {

            // initialize list by reading using rest
            this.getItemsPromise().then((items) => {
                this.itemMap.clear();
                items.forEach((remoteObj) => {
                    this.processDTOUpdate(remoteObj, CRUD.Read);
                });
                this.updateMap();
            });

            /*
             * The following fromEventPatterns set up subscriptions on socket.io events and pipe them to Rx.Observables.
             */

            // socket.io all batch stream
            Rx.Observable.fromEventPattern((eventCallback) => {
                socket.on(this.rootAPI + '/all', eventCallback);
            }, (eventCallback) => {
                // socket.off(this.rootAPI, eventCallback);
            })
                .map(items => {
                    this.itemMap.clear();
                    items.forEach((remoteObj) => {
                        this.processDTOUpdate(remoteObj, CRUD.Read);
                    });
                    this.updateMap();
                });

            // socket.io read stream
            Rx.Observable.fromEventPattern((eventCallback) => {
                socket.on(this.rootAPI, eventCallback);
            }, (eventCallback) => {
                // socket.off(this.rootAPI, eventCallback);
            })
                .map(items => items.forEach(elm => this.processDTOUpdate(elm, CRUD.Read))).forEach(() => this.updateMap());

            // socket.io create stream
            Rx.Observable.fromEventPattern((eventCallback) => {
                socket.on(this.rootAPI + '/create', eventCallback);
            }, (eventCallback) => {
                // socket.off(this.rootAPI + '/create', eventCallback);
            })
                .forEach((remoteObj) => {
                    this.processDTOUpdate(remoteObj, CRUD.Create);
                    this.updateMap();
                });


            // socket.io update stream
            Rx.Observable.fromEventPattern((eventCallback) => {
                socket.on(this.rootAPI + '/update', eventCallback);
            }, (eventCallback) => {
                // socket.off(this.rootAPI + '/update', eventCallback);
            })
                .forEach((remoteObj) => {
                    this.processDTOUpdate(remoteObj, CRUD.Update);
                    this.updateMap();
                });

            // socket.io delete stream
            Rx.Observable.fromEventPattern((eventCallback) => {
                socket.on(this.rootAPI + '/delete', eventCallback);
            }, (eventCallback) => {
                // socket.off(this.rootAPI + '/delete', eventCallback);
            })
                .forEach((remoteId) => {
                    this.processDTOUpdate(remoteId, CRUD.Delete);
                    this.updateMap();
                });
        });
    }

    /**
     * Process each dto and convert it to the local variant or  delete it if a delete event.
     * @param remoteObj The DTO
     * @param action    The CRUD action the event is from: CRUD.{Create,Read,Update,Delete}
     */
    processDTOUpdate(remoteObj: T, action: CRUD) {
        switch (action) {
            case CRUD.Delete:
                this.itemMap.delete(remoteObj);
                break;
            default:
                var localObj = this.fromDTO(remoteObj);
                this.itemMap.set(localObj.id, localObj);
                break;
        }
    }

    /**
     * Push items to the observable queue
     */
    protected updateMap() {
        var itemsToReturn: Array<T> = [];
        var map = this.itemMap;

        /*
         Here Map.entries(), Map.values() and Map.keys() doesn't work.
         for (let [key, value] of/in map) doesn't work either
         The reason is the corejs uses a for loop which take's the Map's length which is always 0.
         */
        map.forEach((value, key) => itemsToReturn.push(value));

        this.itemObservableArray.onNext(itemsToReturn);
    }

    /**
     * Get the current list of items
     * @returns {Array<T>|T}
     */
    getAll(): Array<T> {
        return this.itemObservableArray.getValue();
    }

    /**
     * Get an item in the list
     * @param id
     * @returns {T} Obj
     */
    get(id: string): T {
        return this.itemMap.get(id);
    }

    /**
     * Create a new element
     * @param item                  Item to create on server
     * @returns {IHttpPromise<T>}   Response from server
     */
    create(item): Promise<any> {
        return this.$http.post(this.rootAPI, this.toDTO(item));
    }

    /**
     * Update an object on the server
     * @param item Item to update
     * @returns {IPromise<any|HTMLElement>}
     */
    update(item): Promise<any> {
        return this.$http.put(this.rootAPI + '/' + item.id, this.toDTO(item));
    }

    /**
     * Delete an object on the server
     * @param item Item to delete
     * @returns {IPromise<any|HTMLElement>}
     */
    delete(item) {
        return this.$http.delete(this.rootAPI + '/' + item.id, this.toDTO(item));
    }

    /**
     * Get a list of items on REST: rootAPI
     * @returns {IPromise<TResult>}
     */
    protected getItemsPromise(): Promise<Array<T>> {
        return this.$http.get(this.rootAPI)
            .then((response) => response.data);
    }
}
