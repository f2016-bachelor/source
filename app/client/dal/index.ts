import {socketObservable} from './socketObservable';
import {LocalStorageService} from './localStorage';
import {createSocket} from './socket';
import {carerlogin} from './carerlogin';

var dal: ng.IModule = angular.module('dal', []);

dal
    .service('localStorageService', LocalStorageService)
    .factory('socketObservable', socketObservable)
    .factory('socket', createSocket)
    .run(carerlogin)
;
export {dal};
