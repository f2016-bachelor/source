/// <reference path="../../../typings/browser.d.ts" />

import * as Rx from 'rx';

function socketObservable() {
    return new Rx.ReplaySubject(1);
}

export {socketObservable};
