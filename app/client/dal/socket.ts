/// <reference path="../../../typings/browser.d.ts" />
// export the socket object.
import io = require('socket.io-client');
import {LocalStorageService} from './localStorage';

createSocket.$inject = ['localStorageService', 'socketObservable'];
function createSocket(localStorageService: LocalStorageService, socketObservable: Rx.Observable<SocketIOClient.Socket>) {
    var socket = io({
        'query': 'token=' + localStorage.getItem('jwt')
    });
    // setup to recieve changes when we a login occurs
    localStorageService.getStream()
        .filter((ev: any) => ev.key === 'jwt')
        .subscribe((ev) => {
            socket.disconnect();
            socket.io.opts.query = 'token=' + ev.value;
            socket.connect();
            socketObservable.onNext(socket);
        });

    socket.connect();
    socket.on('error', (error) => {
        if (error.type === 'UnauthorizedError' || error.code === 'invalid_token') {
            // redirect user to login page perhaps?
            console.log("User's token has expired");
        }
    });
    socket.emit('authenticate', {token: localStorage.getItem('jwt')});
    socketObservable.onNext(socket);
    return socket;
}

export {createSocket};
