import {LogModel} from '../bll/models/logModel';
import IHttpPromise = angular.IHttpPromise;
/**
 * Abstract class for making the Http calls to the REST Endpoint for logs. Retrieves and deletes logs
 */
abstract class Logging {

    /**
     * Constructor for Logging class
     * @param rootApi String representing the WEB Api Endpoint to be called with the HTTP service
     * @param $http Angular Http service for making WEB Api calls
     */
    constructor(protected rootApi: string, protected $http: angular.IHttpService) {
    }

    public getAll(): Promise<Array<LogModel>> {
        return this.$http.get(this.rootApi).then((response) => {
            return response.data;
        }) as Promise<Array<LogModel>>;
    }
}

export {Logging}

