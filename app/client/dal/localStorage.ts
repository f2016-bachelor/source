/// <reference path="../../../typings/browser.d.ts" />
import * as Rx from 'rx';

class LocalStorageService {
    changeStream: Rx.Subject;
    constructor() {
        this.changeStream = new Rx.Subject<any>();
    }

    getItem(key: string): string {
        return localStorage.getItem(key);
    }

    setItem(key: string, value: string) {
        localStorage.setItem(key, value);
        this.changeStream.onNext({key: key, value: value});
    }

    getStream(): Rx.Observable<string> {
        return this.changeStream;
    }

    removeItem(key) {
        localStorage.removeItem(key);
        this.changeStream.onNext({key: key, value: undefined});
    }
}

export {LocalStorageService};
