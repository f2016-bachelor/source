import * as Rx from 'rx';
import io = require('socket.io-client');
import {LocalStorageService} from './localStorage';
import Config from '../../shared/presentation/config';

carerlogin.$inject = ['localStorageService', 'socket'];
function carerlogin(localStorageService: LocalStorageService, socket: SocketIOClient.Socket) {

    var careLoginEvents = Rx.Observable.fromEventPattern((eventCallback) => {
        socket.on(Config.api.login.rfid, eventCallback);
    }, (eventCallback) => {
        socket.off(Config.api.login.rfid, eventCallback);
    });

    // var careLoginEvents = new Rx.Subject<string>();
    //
    // socket.on(Config.api.login.rfid, (data) => {
    //     careLoginEvents.onNext(data);
    // });

    careLoginEvents
        .forEach(message => localStorageService.setItem('caregiver', 'true'));
    careLoginEvents.forEach(message => console.log(message));

    return careLoginEvents;
}

export {carerlogin};
