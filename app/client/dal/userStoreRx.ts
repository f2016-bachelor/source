/// <reference path="../../../typings/browser.d.ts" />
import {User} from '../bll/models/user';
import * as Rx from 'rx';

export interface IUserStoreRx {
    getUsersObservable(): Rx.Observable<Array<User>>;
}
