/// <reference path="../../../typings/browser.d.ts" />
import {UserStore} from './stores/userStore';
import {AssignmentStore} from './stores/assignmentStore';
import {DeviceStore} from './stores/deviceStore';
import {CurrentUser} from './currentUser';
import {LogStore} from './stores/logStore';

// Registering the BLL angular module on client side to be used by Angular Controllers
var services: ng.IModule = angular.module('services', ['ui.router', 'dal']);

services
    .service('userStore', UserStore)
    .service('currentUser', CurrentUser)
    .service('assignmentStore', AssignmentStore)
    .service('deviceStore', DeviceStore)
    .service('logStore', LogStore);
export {services};
