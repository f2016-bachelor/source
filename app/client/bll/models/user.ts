/// <reference path="../../../../typings/browser.d.ts" />

export class User {
    constructor(public id: string, public firstName: string, public lastName: string, public middleName = '') {
    }

    public getFullName(): string {
        var toReturn = this.firstName + ' ';
        if (this.middleName !== '') {
            toReturn += this.middleName + ' ';
        }
        toReturn += this.lastName;
        return toReturn;
    }
}
