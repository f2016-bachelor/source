/**
 * Class representing a log.
 */
export class LogModel {

    /**
     * @param event The type of the event to be logged. These types are defined in the LogEventTypes class
     * @param userInputText The information the user submitted when accomplishing an assignment (if the assignment required it)
     * @param assignmentName Name of the assignment (only if the logs is for an assignment event)
     * @param isCareTakerAssignment Boolean flag to inform weather an assignment belongs to a care taker or not.
     * @param timeStamp The date of the log
     * @param deviceId Id of the device on which this log is made
     * @param userId The id of the user who made a change
     */
    constructor(public event: string, public isCareTakerAssignment: boolean, public timeStamp: Date, public deviceId: string, public userId?: string, public userInputText?: string, public assignmentName?: string) {
    }
}
