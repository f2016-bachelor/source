/// <reference path="../../../../typings/browser.d.ts" />

import Config from '../../../shared/presentation/config';

export class DeviceStore {
    static $inject = ['$http', 'socket'];

    constructor(protected $http: angular.IHttpService, protected socket: SocketIOClient.Socket) {
    }

    create(item): Promise<any> {
        return this.$http.post(Config.api.devices.this, item);
    }
}
