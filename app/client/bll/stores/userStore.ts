/// <reference path="../../../../typings/browser.d.ts" />

import Config from '../../../shared/presentation/config';
import {MixedBasicCRUD} from '../../dal/MixedBasicCrud';
import {User} from '../models/user';
import {IUserStoreRx} from '../../dal/userStoreRx';
import {IUserStore} from '../interfaces/userStore';
import {UserConverter} from '../converters/UserConverter';

/**
 * Assignment Transport retrieves, sends and recieves Assignments from the server.
 * When a change is recieved then the Map is updated and a new array of Assignment is pushed to 'items' Observable
 * The other parts of the project can use this observable to observe changes to the array.
 */
export class UserStore extends MixedBasicCRUD<User> implements IUserStore, IUserStoreRx {
    static $inject = ['$http', 'socketObservable'];

    constructor(protected $http: angular.IHttpService, protected socketObservable: Rx.ReplaySubject) {
        super(Config.api.users.this, $http, socketObservable, (fromDTO) => UserConverter.newFromDTO(fromDTO), (toDTO) => UserConverter.newToDTO(toDTO));
    }

    getUsers(): Array<User> {
        return this.getAll();
    }

    getUser(id): User {
        return this.get(id);
    }

    getUsersObservable(): Rx.Observable<Array<User>> {
        return this.itemObservableArray;
    }

    createUser(item: User) {
        return this.create(item);
    }

    updateUser(item: User) {
        return this.update(item);
    }
}
