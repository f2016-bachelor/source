/// <reference path="../../../../typings/browser.d.ts" />
import * as Rx from 'rx';

import {IAssignmentTransport} from '../interfaces/IAssignmentTransport';
import Config from '../../../shared/presentation/config';
import {AssignmentConverter} from './../converters/AssignmentConverter';
import {Assignment} from './../assignment';
import {MixedBasicCRUD} from '../../dal/MixedBasicCrud';

/**
 * Assignment Transport retrieves, sends and recieves Assignments from the server.
 * When a change is recieved, then the Map is updated and a new array of Assignments is pushed to 'assignments' Observable
 * The other parts of the project can use this observable to observe changes to the array.
 */
export class AssignmentStore extends MixedBasicCRUD<Assignment> implements IAssignmentTransport {
    static $inject = ['$http', 'socketObservable'];

    constructor(protected $http: angular.IHttpService, socketObservable: Rx.ReplaySubject) {
        super(Config.api.assignments.this, $http, socketObservable, (fromDTO) => AssignmentConverter.newFromDTO(fromDTO),
            (fromModel) => AssignmentConverter.newToDTO(fromModel));

        this.getAll();
    }

    getAssignments(): Array<Assignment> {
        return this.getAll();
    }

    getAssignmentsObservable(): Rx.Observable< Array<Assignment> > {
        return this.itemObservableArray;
    }

    getAssignment(id: String): Assignment {
        return this.get(id);
    }

    createAssignment(item: Assignment) {
        this.create(item);
    }

    updateAssignment(item: Assignment) {
        this.update(item);
    }

    finishAssignment(item: Assignment) {
        item.finished = true;
        this.update(item);
    }

    deleteAssignment(item: Assignment) {
        this.delete(item);
    }
}
