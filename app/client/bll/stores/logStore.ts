/// <reference path="../../../../typings/browser.d.ts" />

import Config from '../../../shared/presentation/config';
import {LogModel} from '../models/logModel';
import {ILogStore} from '../interfaces/iLogStore';
import {Logging} from '../../dal/Logging';
import IHttpPromise = angular.IHttpPromise;

/**
 * Class responsible for retrieving and deleting log objects
 */
export class LogStore extends Logging implements ILogStore  {
    static $inject = ['$http'];

    /**
     * @param $http Angular Http service being passed on to the super class Logging
     */
    constructor(protected $http: angular.IHttpService) {
        super(Config.api.logs.this, $http);
    }

    /**
     * Get all the logs existing for a device
     * @returns {any} Array of LogModel objects
     */
    getAllLogs(): Promise<Array<LogModel>> {
        return this.getAll();
    }

}

