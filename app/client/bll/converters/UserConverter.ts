/// <reference path="../../../../typings/browser.d.ts" />

import {UserDTO} from '../../../shared/dto/user';
import {User} from '../models/user';

/**
 * Converter class converting User correctly between Server and Client
 */
export class UserConverter {
    static commonCopy(from: any, to: any) {
        to.firstName = from.firstName;
        to.lastName = from.lastName;
        to.middleName = from.middleName;
        to.id = from.id;
    }

    static toDTO(from: User, to: UserDTO) {
        this.commonCopy(from, to);
    }

    static newToDTO(from: User): UserDTO {
        return new UserDTO(from.id, from.firstName, from.lastName, from.middleName);

    }

    static fromDTO(from: UserDTO, to: User) {
        this.commonCopy(from, to);
    }

    static newFromDTO(from: UserDTO): User {
        return new User(from.id, from.firstName, from.lastName, from.middleName);

    }
}
