/// <reference path="../../../../typings/browser.d.ts" />

import {RRule} from 'rrule';
import {AssignmentDTO} from '../../../shared/dto/Assignment';
import {Assignment, PriorityLevel} from '../assignment';

/**
 * Converter class converting Assignment correctly between Server and Client
 */
export class AssignmentConverter {
    static commonCopy(from: any, to: any) {
        to.id = from.id;
        to.name = from.name;
        to.description = from.description;
        to.finished = from.finished || false;
        to.users = from.users;
        to.recurring = from.recurring || false;
        to.alarm = from.alarm;
        to.roles = from.roles;
        to.userInputNeeded = from.userInputNeeded;
        to.userInput = from.userInput;
    }

    static toDTO(from: Assignment, to: AssignmentDTO) {
        this.commonCopy(from, to);

        to.priority = <string>from.priority; // the Enum is a string
        to.date = from.date.toString();
        if (to.recurring) {
            to.recurringInfo = from.recurringInfo.toString();
        } else {
            to.recurringInfo = '';
        }
    }

    static newToDTO(from: Assignment): AssignmentDTO {
        var recurringString = '';
        try {
            recurringString = from.recurringInfo.toString();
        } catch (err) {

        }
        var newAssignment = new AssignmentDTO(from.id,
            from.name,
            from.priority as string,
            from.date.toString(),
            from.users,
            from.roles,
            from.userInputNeeded,
            from.recurring,
            recurringString,
            from.description,
            from.userInput,
            from.finished || false);
        newAssignment.seqno = from.seqno;
        newAssignment.alarm = from.alarm;
        return newAssignment;
    }

    static fromDTO(from: AssignmentDTO, to: Assignment) {
        this.commonCopy(from, to);

        // conversion of dates in try-catch to avoid exceptions, default is unix-0
        try {
            to.date = new Date(from.date);
            to.priority = <PriorityLevel>PriorityLevel[from.priority]; // the Enum is a string
            to.recurringInfo = RRule.fromString(from.recurringInfo);
        } catch (err) {

        }
    }

    static newFromDTO(from: AssignmentDTO): Assignment {
        var to = new Assignment(from.id, from.name, PriorityLevel.High, new Date(0), from.users, from.roles, from.userInputNeeded,
            from.recurring, from.recurring && RRule.fromString(from.recurringInfo) || '', from.description, from.userInput, from.finished);
        to.finalizeProperties = {}; // TODO: How should the db store this?
        // conversion of dates in try-catch to avoid exceptions, default set above is unix epoch
        try {
            to.priority = <PriorityLevel>PriorityLevel[from.priority];
            to.date = new Date(from.date);
        } catch (err) {

        }

        return to;
    }
}
