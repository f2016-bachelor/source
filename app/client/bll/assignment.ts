/**
 * Enum: Prioritylevel for Assignment
 */
enum PriorityLevel {
    High = <any>'High',
    Normal = <any>'Normal',
    Low = <any>'Low'
}

/**
 * Method used like a "ToString" method for the priority level
 * @param prio The Priority level
 * @returns {string} The priority in Danish
 */
function prioToString(prio: PriorityLevel): string {
    switch (prio) {
        case 'High':
            return 'Høj';
        case 'Normal':
            return 'Middel';
        case 'Low':
            return 'Lav';
    }
}

/**
 * Assignment Class
 * Internal representation of an assignment, needs to be converted to be send over the wire.
 */
class Assignment {

    /**
     * Constructor for Assignment
     * @param id                    UUID Assingment unique identifier
     * @param name                  String Primary name of the assignment
     * @param priority              PriorityLevel Enum/String The priority of task
     * @param date                  Date/time for assignment
     * @param users                 Array of user ID's referring to the users associated with this assignment
     * @param roles                 Array of roles, specifieng who should do the assignment (carer or resident)
     * @param userInputNeeded       Is user input needed before assignement can be finished
     * @param recurring             Is the assignment recurring?
     * @param recurringInfo         RRule specifying how assignment should be recurring
     * @param description           Optional description
     * @param userInput             Field for entered userinput if needed when finished
     * @param finished              Bool, indicating if assignment is finished
     */
    constructor(public id: string, public name: string,
                public priority: PriorityLevel, public date: Date, public users: Array, public roles: Array, public userInputNeeded = false,
                public recurring = false, public recurringInfo: RRule, public description = '', public userInput = '', public finished = false) {
        this.seqno = 0; // sequence number for assignment in list
        this.alarm = true; // notification is on
    }

    finalizeProperties: any = {};

    // variables
    public alarm;
    public seqno: number;
}

export {Assignment, PriorityLevel, prioToString};
