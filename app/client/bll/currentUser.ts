/// <reference path="../../../typings/browser.d.ts" />

import * as Rx from 'rx';
import {User} from './models/user';
import {IUserStore} from 'interfaces/userStore';

export class CurrentUser {
    static $inject = ['$rootScope', '$stateParams', 'userStore'];

    /**
     * Contains the current userId.
     * The same value may be pushed multiple times in a row.
     */
    currentUser: Rx.BehaviorSubject<string>;

    /**
     * Returns current userId, but does not return the same id two or more times in a row.
     */
    currentUserObservable: Rx.Observable<string>;

    /**
     * Returns the current user as an user object from the userStore.
     */
    currentUserObject: Rx.Observable<User>;

    /**
     * Constructor for CurrentUser
     * @param $rootScope Shared scope between all controllers
     * @param $stateParams Service giving access to parameters send in url
     * @param userStore Store for accessing CRUD actions on user
     */
    constructor($rootScope, $stateParams, userStore: IUserStore) {
        // setup behavior subject, this observable always returns a value

        // check if the id is already set, if it is use it as the start value.
        if ($stateParams.userId) {
            this.currentUser = new Rx.BehaviorSubject<string>($stateParams.userId);
        } else { // if it isn't set then just use an empty string
            this.currentUser = new Rx.BehaviorSubject<string>('');
        }
        // distinctUntilChanged only emits when the current user id changes, skips empty strings
        this.currentUserObservable = this.currentUser.distinctUntilChanged()
            .filter((userId) => userId !== '');

        // watch for all $stateChangeStart events
        $rootScope.$on('$stateChangeStart',
            (event, toState, toParams, fromState, fromParams, options) => {
                if (toParams.userId) {
                    this.currentUser.onNext(toParams.userId);
                }
            });

        // Combine userId events with the list of users from userStore
        this.currentUserObject = this.currentUserObservable
            .combineLatest(userStore.getUsersObservable(), (userId, users) => {
                return userStore.getUser(userId);
            })
            // if the user isn't found then skip emitting anything.
            .filter((user) => user !== undefined);
    }

    /**
     * Observable sequence of userId's.
     * @returns {Rx.Observable<string>}
     */
    getUserStringObservable(): Rx.Observable<string> {
        return this.currentUserObservable;
    }

    /**
     * Observable sequence of User objects
     * @returns {Rx.Observable<User>}
     */
    getUserObjectObservable(): Rx.Observable<User> {
        return this.currentUserObject;
    }
}
