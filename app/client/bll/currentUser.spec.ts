/// <reference path="../../../typings/browser.d.ts" />

/* tslint:disable:variable-name */
import 'angular-mocks';
import {CurrentUser} from './currentUser';
import {UserStore} from '../../shared/bll/stub/userStore';

describe('CurrentUser', () => {

    var uut: CurrentUser;
    var $rootScope;
    var userStore;

    // beforeEach(angular.mock.module('services'));

    beforeEach(() => {
        angular.mock.inject((_$rootScope_) => {
            $rootScope = _$rootScope_;
            userStore = new UserStore();
        });
    });

    var initialize = ($stateParams) => {
        return new CurrentUser($rootScope, $stateParams, userStore);
    };

    it('with $stateParams.userId = undefined', () => {
        // arrange
        var params = {};
        var testUserId = 'not-changed';

        // act
        uut = initialize(params);
        userStore.emitList();

        // assert
        uut.getUserStringObservable().subscribe((userId) => {
            testUserId = userId;
        });

        expect(testUserId).toBe('not-changed');
    });

    it('with $stateParams.userId = id', () => {
        // arrange
        var testUserId = 'not-changed';
        var params = {userId: userStore.users[0].id};

        // act
        uut = initialize(params);
        userStore.emitList();

        // assert
        uut.getUserStringObservable().subscribe((userId) => {
            testUserId = userId;
        });

        expect(testUserId).toBe(userStore.users[0].id);
    });
});
