import {LogModel} from '../models/logModel';
/**
 * Interface for getting and deleting logs from/on the server
 */
interface ILogStore {
    /**
     * Get all the logs for the deivce. No device Id is required as it will be in the token in the request header
     * @returns HTTP promise containing all the logs in an array
     */
    getAllLogs(): Promise<Array<LogModel>>;
}

export {ILogStore}
