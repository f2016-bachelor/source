// <reference path="../../../typings/browser.d.ts" />

import {Assignment} from '../assignment';
interface IAssignmentStore {
    getAssignments(): Array<Assignment>;

    getAssignment(id: String): Assignment;

    createAssignment(item: Assignment);

    updateAssignment(item: Assignment);

    finishAssignment(item: Assignment);

    deleteAssignment(item: Assignment);
}

export {IAssignmentStore};
