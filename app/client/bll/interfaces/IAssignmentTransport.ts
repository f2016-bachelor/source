// <reference path="../../../typings/browser.d.ts" />
import * as Rx from 'rx';
import {Assignment} from '../assignment';
import {IAssignmentStore} from './assignmentStore';

interface IAssignmentTransport extends IAssignmentStore {
    getAssignmentsObservable(): Rx.Observable<Array<Assignment>>;
}

export {IAssignmentTransport};
