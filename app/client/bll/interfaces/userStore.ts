/// <reference path="../../../../typings/browser.d.ts" />
import {IUserStoreRx} from '../../dal/userStoreRx';
import {User} from '../models/user';

interface IUserStore extends IUserStoreRx {
    getUsers(): Array<User>;
    getUser(id): User;
    createUser(item: User);
    updateUser(item: User);
}

export {IUserStore}
