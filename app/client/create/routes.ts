/// <reference path="../../../typings/browser.d.ts" />

import {IStateProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider'];

// defines all routes, used when creating assignment
// each item in assignment has own html page

function routes($stateProvider: IStateProvider) {
    'use strict';
    $stateProvider
        .state('main.create', { // 'main' page for creating assignment. all others are put into this with ui-view
            url: '/:userId/create',
            templateUrl: 'client/create/create.html', // specifies html page to use
            controller: 'createController', // defines controller
            controllerAs: 'create'          // defines controller 'nickname', easier to use
        })

        // sub-pages - links to htmls defined in subviews folder.
        // url is attached to the url in 'main' state: /:userId/create/ + fx 'name'
        .state('main.create.name', {
            url: '/name',
            templateUrl: 'client/create/subviews/name.html',

        })
        .state('main.create.date', {
            url: '/date',
            templateUrl: 'client/create/subviews/date.html',

        })
        .state('main.create.priority', {
            url: '/priority',
            templateUrl: 'client/create/subviews/priority.html',

        })
        .state('main.create.description', {
            url: '/description',
            templateUrl: 'client/create/subviews/description.html',

        })

        .state('main.create.confirm', {
            url: '/confirm',
            templateUrl: 'client/create/subviews/confirm.html',
        })
    ;
}

export {routes}
