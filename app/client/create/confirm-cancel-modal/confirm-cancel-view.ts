/// <reference path="../../../../typings/browser.d.ts" />

// http://stackoverflow.com/a/28373164/3411879
import {IStateService} from 'angular-ui-router';

/**
 * Controller for ConfirmCancel Modal window
 */
export class ConfirmCancelViewController {
    static $inject = ['$uibModalInstance', '$state'];

    /**
     * Constructor for Controller
     * @param $uibModalInstance Injectiable for $uibModal for accessing instance of modal window
     * @param $state Service to change state
     */
    constructor(private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, protected $state: IStateService) {
    }

    /**
     * Sets up Modal window by defining the view (html) and controller for window.
     * @returns {{templateUrl: string, controller: ConfirmCancelViewController, controllerAs: string, backdrop: string, keyboard: boolean}}
     */
    static options(): ng.ui.bootstrap.IModalSettings {
        return {
            templateUrl: 'client/create/confirm-cancel-modal/confirm-cancel-view.html',
            controller: ConfirmCancelViewController,
            controllerAs: '$ctrl',
            backdrop: 'static', // disable that clicks outside closes modal window
            keyboard: false,    // disable that keyboard ESC click closes window
        };
    }

    /**
     * Confirm canceling of creation
     */
    confirm() {
        this.$state.go('main.today'); // redirect to today page
        this.$uibModalInstance.dismiss('cancel'); // closes modal window
    }

    /**
     * Cancel canceling of creation
     */
    cancel() {
        this.$uibModalInstance.dismiss('cancel'); // closes modal window , stays on creation page
    }
}
