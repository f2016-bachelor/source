/// <reference path="../../../typings/browser.d.ts" />
import * as uuid from 'uuid';
import {RRule} from 'rrule';
import {IStateService} from 'angular-ui-router';
import {UserDTO} from '../../shared/dto/user';
import {PriorityLevel, Assignment, prioToString} from '../bll/assignment';
import {IAssignmentStore} from '../bll/interfaces/assignmentStore';
import {ConfirmCancelViewController} from './confirm-cancel-modal/confirm-cancel-view';
import {CurrentUser} from '../bll/currentUser';
import {LocalStorageService} from '../dal/localStorage';

/**
 * Controller for creating new assignment. Uses input collected from views in 'subviews' folder.
 */
export class CreateController {
    'use strict';

    // injections used in Controller
    static $inject = ['$state', 'assignmentStore', '$uibModal', 'currentUser', 'localStorageService'];

    /**
     * Constructor for CreateController. Sets up default data for input-fields and subscribes to currentUser observable
     * @param $state Service to change state
     * @param assignmentStore Business object for assignments - follows the CRUD pattern
     * @param $uibModal Service for creating Modal windows
     * @param currentUser Observable for getting the current user. Used to create assignment on specific user
     * @param localStorageService Wrapper for getting data from LocalStorage
     */
    public constructor(protected $state: IStateService, protected assignmentStore: IAssignmentStore,
                       protected $uibModal: ng.ui.bootstrap.IModalService, protected currentUser: CurrentUser,
                       protected localStorageService: LocalStorageService) {
        // set ui-view default to name.html
        // http://www.podpea.co.uk/blog/default-child-states-in-angular-ui-router/
        var parent = 'main.create';
        if ($state.current.name.substr(-parent.length) === parent) {
            $state.go('main.create.name');
        }

        // subscribes to currentUser to get ID to put in new Assignment
        currentUser.getUserStringObservable().subscribe((user: string) => this.userID = user);

        // variable used to create radiobuttons to select Priority
        this.prio = PriorityLevel;
        // default priority
        this.priority = PriorityLevel.High;

        // Options for reccuring frequency
        this.freqOPTIONS = ['Årligt', 'Månedligt', 'Ugenligt', 'Dagligt', 'Timeligt', 'Aldrig'];
        this.freq = this.freqOPTIONS[5]; // sets default
        this.freqUnits = ['år', 'måneder', 'uger', 'dage', 'timer']; // units to show  depending on the requency chosen by user

        // default variables for user inputs
        this.userinputneeded = false;
        this.interval = 1;
        this.date = new Date();
        this.date.setMinutes(0);
        var tempH = this.date.getHours();
        this.date.setHours(tempH + 1); // set hours to next hour
        if (!this.isCarerLoggedIn()) {
            // just a regular user, assignment can only be created for user itself
            this.roles = ['resident'];
        } else {
            this.roles = ['carer']; // default
        }
        this.roleOptions = {Borger: false, Plejer: true};
        // this.roleOptions = ['Borger', 'Plejer'];
        // Define date picker options (to be used on the date picker directive
        this.dateOptions = {
            minDate: new Date(), // You shouldn't be allowed to pick a date before today!
            startingDay: 1 // Startday is Monday
        };

        // Configuration object for Angular Virtual Keyboard
        this.keyBoardConfig = {
            kt: 'Dansk', // layout
            numberPad: false,
            relative: false, // Keyboard will be positioned in the bottom
            sizeAdj: false, // user cannot adjust size of keyboard
            deadkeysOn: false // Special keys that we don't need
        };

        // Another configuration for keyboard but this time using the numberpad and not positioned in bottom
        // To be used when only numbers are needed
        this.numpadConfig = {
            kt: 'Numerico'
        };
    }

    // Bindable methods

    // save new assignment, based on data input by user
    /**
     * Adding new assignment - called on click on Finish button from confirm.html.
     */
    public addAssignment() {
        var newUUID = uuid.v4(); // assignment ID created
        this.users.push(this.userID); // puts currentUsers ID in users-Array

        // if Assignment is not set to recurring, recurringInfo on assignment is to be empty
        if (this.recurring) {    // otherwise, new RRule created with user specified frequency , interval and date
            var rule = new RRule({
                freq: this.freqOPTIONS.indexOf(this.freq),
                interval: this.interval,
                dtstart: this.date,
            });
            this.recurringInfo = rule;
        }
        // creates assignment by using Assignmentstore's create method
        this.assignmentStore.createAssignment(new Assignment(newUUID, this.name, this.priority, this.date, this.users, this.roles,
            this.userinputneeded, this.recurring, this.recurringInfo, this.description));

        this.$state.go('main.today'); // redirect to today page
    }

    /**
     * Allows user to cancel creation of assignment
     */
    public cancel() {
        var options = ConfirmCancelViewController.options(); // gets setup for ConfirmCancel modal window
        if (this.dirty) { // if user have entered data, Modal window is show asking for confirmation of canceling
            this.$uibModal
                .open(options);
        } else if (!this.dirty) { // otherwise, just redirect back to Today page
            this.$state.go('main.today');
        }
    }

    /**
     * Sets this.dirty to true, if user sets a name for assignment (called on ng-change in name.html)
     */
    public setDirty() {
        this.dirty = true;
    }

    public setRecurring() {
        if (this.freq === 'Aldrig') {
            this.recurring = false;
        } else {
            this.recurring = true;
        }
    }

    public toggle(status, item) {
        item = this.rolesTranslation(item);
        var idx = this.roles.indexOf(item);
        if (!status) {
            this.roles.splice(idx, 1);
        } else {
            this.roles.push(item);
        }
    }

    public getRoles() {
        var ro = this.roles;
        var that = this;
        ro = ro.map((item) => {
            return that.rolesTranslation(item);
        });
        return ro.join(' og ');
    }

    public rolesTranslation(role): string {
        switch (role) {
            case 'Plejer':
                return 'carer';
            case 'Borger':
                return 'resident';
            case 'carer':
                return 'Plejer';
            case 'resident':
                return 'Borger';
        }
    }

    /**
     * Gets 'caregiver' data from LocalStorage to determine if caregiver is logged in
     * @returns {boolean} true/false indicating if caregiver is logged in or not
     */
    public isCarerLoggedIn() {
        return (this.localStorageService.getItem('caregiver') === 'true');
    }

    //
    /**
     * Method used to write the priority in Danish
     * @param prio gets PriorityLevel enum
     * @returns {string} translated enum
     */
    public prio2String(prio: PriorityLevel): string {
        return prioToString(prio);
    }

    // variables used in Controller
    public user: UserDTO;
    public name: string;
    public date: Date;
    public dateOptions: Object;
    public priority: PriorityLevel;
    public description: string;
    public prio;
    public recurring = false;
    public recurringInfo: RRule;
    public freqOPTIONS;
    public freqUnits;
    public interval;
    public freq;
    public dirty;
    public users = [];
    public userID;
    public roleOptions;
    public roles: Array;
    public userinputneeded: boolean;
    public keyBoardConfig: any;
    public numpadConfig: any;
}
