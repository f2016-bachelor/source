import {routes}  from './routes';
import {CreateController} from './createController';

var createModule: ng.IModule = angular.module('create', ['ui.router', 'services']);

createModule
    .config(routes)
    .controller('createController', CreateController);

export {createModule};
