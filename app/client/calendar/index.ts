import {routes}  from './routes';
import {CalendarController} from './calendarController';

// Skipping optional dependencies, override in jspm-config with @empty
// https://github.com/jspm/jspm-cli/issues/924
import 'angular-bootstrap-calendar';
import 'angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css!';

var homeModule: ng.IModule = angular.module('calendar', ['ui.router', 'mwl.calendar', 'ui.bootstrap', 'ngAnimate', 'services']);

homeModule
    .config(routes)
    .controller('calendarController', CalendarController);

export {homeModule};
