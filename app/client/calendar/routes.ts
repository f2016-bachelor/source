/// <reference path="../../../typings/browser.d.ts" />

import {IStateProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider'];

/**
 * Defines routes for home.
 * @param $stateProvider used to define state main.home
 */
function routes($stateProvider: IStateProvider) {
    'use strict';
    $stateProvider
        .state('main.calendar', {
            url: '/calendar', // url
            templateUrl: 'client/calendar/calendar.html', // html page for home
            controller: 'calendarController', // controller for home
            controllerAs: '$ctrl' // nickname for controller
        });
}

export {routes}
