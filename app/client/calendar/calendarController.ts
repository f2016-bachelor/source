/// <reference path="../../../typings/browser.d.ts" />
import * as Rx from 'rx';

import {IAssignmentTransport} from '../bll/interfaces/IAssignmentTransport';
import {Assignment, PriorityLevel} from '../bll/assignment';
import {AssignmentViewController} from '../today/assignment-view/assignment-view';
import {LocalStorageService} from '../dal/localStorage';
import {IUserStore} from '../bll/interfaces/userStore';

class Event {
    id: string;
    title: string;
    startsAt: Date;
    type: string;
}

/**
 * Controller for 'home' page.
 */
export class CalendarController {

    // injections used in Controller
    static $inject = ['assignmentStore', '$uibModal', 'localStorageService', 'userStore'];
    public users;
    /**
     * Constructor for Controller. Sets up userstore observable for getting users
     * @param userStore Store for getting users
     * @param localStorageService Wrapper for getting data from LocalStorage
     * @param $state Service to change state
     */
    constructor(protected assignmentStore: IAssignmentTransport, protected $uibModal: ng.ui.bootstrap.IModalService,
                protected localStorageService: LocalStorageService, protected userStore: IUserStore) {
        this.users = userStore.getUsers();

        this.viewDateObs = new Rx.BehaviorSubject<Date>(this.viewDate);
        this.calendarViewObs = new Rx.BehaviorSubject<string>(this.calendarView);
        var firstLastDay = this.calendarViewObs.combineLatest(this.viewDateObs, (view, date: Date) => {
            switch (view) {

                case 'year':
                    return [new Date(date.getFullYear(), 0, 1),
                        new Date(date.getFullYear(), 12, 1)];

                case 'month':
                case 'week':
                    return [new Date(date.getFullYear(), date.getMonth(), 1),
                        new Date(date.getFullYear(), date.getMonth() + 1, 1)];

                case 'day':
                default:
                    var start = new Date(date.getTime());
                    start.setHours(0, 0, 0, 0);
                    var end = new Date(date.getTime());
                    end.setHours(23, 59, 59, 999);
                    return [start, end];
            }
        });

        var assignmentObservable = this.assignmentStore.getAssignmentsObservable().map((items: Array<Assignment>) => items.filter((item) => !item.finished));

        var carerInfo = new Rx.BehaviorSubject<string>(localStorageService.getItem('caregiver'));
        localStorageService.getStream().filter((vv) => vv.key === 'caregiver').map((vv) => vv.value).subscribe(carerInfo);

        var rolesFilteredList = assignmentObservable.combineLatest(carerInfo.map((isCarer) => this.isCarerLoggedIn(isCarer)),
            (assignments, info) => {
                if (!info) { // if carer is NOT logged in, only return assignments where the role array contains 'resident' role
                    return assignments.filter((assignment) => assignment.roles.indexOf('resident') !== -1);
                } else { // else return all assignments, so caregiver can see all assignments
                    return assignments;
                }
            });

        var notRecurringObs = rolesFilteredList;

        var recurringObs = firstLastDay.combineLatest(rolesFilteredList, (firstLast, items: Array<Assignment>) => {
            // assignments that are not recurring is filtered in an array

            var oneoffs = items.filter((item) => !item.recurring);

            // assignments that are recurring have an instance generated for each recurrence.
            var recurring = items.filter((item) => item.recurring)
                .map((item: Assignment) =>
                    item.recurringInfo // item has one recurrringInfo that is of type RRule
                        .between(firstLast[0], firstLast[1])
                        .map((date, i) => { // copy (shallow) the assignment and change date and seqno.
                            var assignment = Object.create(item); // copy
                            assignment.date = date; // set date to specific recurrence found in .all
                            assignment.seqno = i; // set seqno to sequence number in recurrence list
                            return assignment;
                        }));

            // merge oneoffs (Array<Assignment>) with recurring (Array<Array<Assignment>>)
            // http://stackoverflow.com/questions/10865025/merge-flatten-a-multidimensional-array-in-javascript/10865042 (2016-04-27)
            return [].concat.apply(oneoffs, recurring);
        });

        this.recurringChanged.combineLatest([notRecurringObs, recurringObs], (showRecurring, notRecur, recur) => showRecurring ? recur : notRecur)
            .map((assignments: Array<Assignment>) => assignments.map((assignment: Assignment) => {
                return {
                    id: assignment.id,
                    title: assignment.name + ' (' + this.getUsername(assignment.users[0]) + ')',
                    startsAt: assignment.date,
                    type: this.prioToString(assignment.priority),
                    incrementsBadgeTotal: assignment.priority === PriorityLevel.High,
                };
            }))
            .forEach((items) => {
                this.events = items;
            });
    }

    getUsername(id) {
        var name = this.users.filter((m) => m.id === id).map((m) => {
            var n = m.firstName + ' ' + m.lastName;
            return n;
        });
        return name[0];
    }

    prioToString(prio: PriorityLevel) {
        switch (prio) {
            case PriorityLevel.High:
                return 'important';
            case PriorityLevel.Normal:
                return 'warning';
            default:
            case PriorityLevel.Low:
                return 'success';
        }
    }

    // These variables MUST be set as a minimum for the calendar to work
    calendarView = 'month';
    calendarViewObs;

    calendarViewClick() {
        this.calendarViewObs.onNext(this.calendarView);
    }

    viewDate = new Date();
    viewDateObs;

    viewDateClick() {
        this.viewDateObs.onNext(this.viewDate);
    }

    events: Array<Event>;
    eventsRecurring: Array<Event>;
    eventsNotRecurring: Array<Event>;

    recurringChanged = new Rx.BehaviorSubject<boolean>(true);

    isCellOpen = true;

    getAssignment(event: Event) {
        var ass = this.assignmentStore.getAssignment(event.id);
        ass.date = event.startsAt;
        return ass;
    }

    setRecurring(reccur: boolean) {
        this.recurringChanged.onNext(reccur);
    }

    eventClicked(event) {
        var options = AssignmentViewController.options(); // gets setup for AssignmentView modal window
        options.resolve = {assignment: this.getAssignment(event)}; // send assignment to modal window

        this.$uibModal
            .open(options); // open modal window
    }

    eventEdited(event) {
        this.eventClicked(event);
    }

    eventDeleted(event) {
        this.assignmentStore.finishAssignment(this.getAssignment(event));
    }

    eventTimesChanged(event) {
        // alert.show('Dropped or resized', event);
    }

    toggle($event, field, event) {
        $event.preventDefault();
        $event.stopPropagation();
        event[field] = !event[field];
    }

    public isCarerLoggedIn(carerInfo) {
        return (carerInfo === 'true');
    }
}
