/// <reference path="../../../typings/browser.d.ts" />

import {IStateProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider'];

/**
 * Defines routes for home.
 * @param $stateProvider used to define state main.home
 */
function routes($stateProvider: IStateProvider) {
    'use strict';
    $stateProvider
        .state('main.home', {
            url: '/home', // url
            templateUrl: 'client/home/home.html', // html page for home
            controller: 'homeController', // controller for home
            controllerAs: 'home' // nickname for controller
        });
}

export {routes}
