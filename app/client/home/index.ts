import {routes}  from './routes';
import {HomeController} from './homeController';

var homeModule: ng.IModule = angular.module('home', ['ui.router', 'services']);

homeModule
    .config(routes)
    .controller('homeController', HomeController);

export {homeModule};
