/// <reference path="../../../typings/browser.d.ts" />
import * as Rx from 'rx';

import {IUserStoreRx} from '../dal/userStoreRx';
import {LocalStorageService} from '../dal/localStorage';
import {IStateService} from 'angular-ui-router';

/**
 * Controller for 'home' page.
 */
export class HomeController {

    // injections used in Controller
    static $inject = ['userStore', 'localStorageService', '$state', '$timeout'];

    /**
     * Constructor for Controller. Sets up userstore observable for getting users
     * @param userStore Store for getting users
     * @param localStorageService Wrapper for getting data from LocalStorage
    * @param $state Service to change state
     */
    constructor(protected userStore: IUserStoreRx, protected localStorageService: LocalStorageService, protected $state: IStateService, $timeout) {
        // subscribes to observable in userstore
        // get users every time the list changes
        userStore.getUsersObservable().subscribe((items) => this.users = items);
        // checks if caregiver is logged in, by checking item in localStorageService
        // this.carerChecked used to set value of checkbox depending on carer logged in status
        var carerChanges = new Rx.BehaviorSubject((this.localStorageService.getItem('caregiver') === 'true'));
        this.localStorageService.getStream()
            .filter((item) => item.key === 'caregiver')
            .map((item) => item.value)
            .map((item) => item === 'true')
            .subscribe((item) => carerChanges.onNext(item));

        var self = this;
        carerChanges.forEach((item) => {
            $timeout(function(){
                self.carerChecked = item;
            });
        });
    }

    /**
     * Called from home.html for creating list of users on page
     * @returns {Array} users found
     */
    getUsers() {
        return this.users;
    }

    /**
     * Logs in carer depending on value of checkbox, called on ng-change in home.html on checkbox
     * @param carerChecked true/false depending on checked/unchecked
     */
    caregiverLogin(carerChecked) {
        if (carerChecked) { // if checked, carer logs in, and localStorageService sets item 'caregiver' to 'true'
            this.localStorageService.setItem('caregiver', 'true');
        } else { // otherwise set 'caregiver' to 'null' -- logs out carer
            this.localStorageService.setItem('caregiver', null);
        }
    }

    /**
     * Logout device only implemented for debugging purposes...
     */
    logout() {
        this.localStorageService.removeItem('jwt');
        this.localStorageService.removeItem('caregiver');
        this.$state.go('main.login');
    }


    // variables
    public carerChecked;
    public users = [];
}
