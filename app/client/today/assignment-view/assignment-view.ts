/// <reference path="../../../../typings/browser.d.ts" />
import {RRule} from 'rrule';
import {Assignment, PriorityLevel, prioToString} from '../../bll/assignment';
import {IAssignmentStore} from '../../bll/interfaces/assignmentStore';
import {ConfirmViewController} from '../confirm-view/confirm-view';
import {LocalStorageService} from '../../dal/localStorage';

// http://stackoverflow.com/a/28373164/3411879

/**
 * Controller for AssignmentView Modal window
 * Lets user see assignment info and make changes/finish assignment
 */
export class AssignmentViewController {
    static $inject = ['$uibModalInstance', 'assignment', '$uibModal', 'assignmentStore', 'localStorageService'];


    /**
     * Constructor for Controller
     * @param $uibModalInstance Injectiable for $uibModal for accessing instance of modal window
     * @param assignment Assignment class
     * @param $uibModal Service for creating Modal windows
     * @param assignmentStore Store for accessing Assignments
     */
    constructor(private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, public assignment: Assignment,
                protected $uibModal: ng.ui.bootstrap.IModalService, protected assignmentStore: IAssignmentStore,
                protected localStorageService: LocalStorageService) {

        // setup variables used to show assignment
        this.prio = PriorityLevel; // priority enum radio buttons
        this.freqOPTIONS = ['Årligt', 'Månedligt', 'Ugeligt', 'Dagligt', 'Timeligt', 'Aldrig']; // frequency dropdown menu
        this.freqUnits = ['år', 'måneder', 'uger', 'dage', 'timer']; // units to show  depending on the requency chosen by user

        // finds id of the clicked assignment. using this id to replace assignment with changed, if user saves changes
        this.id = assignment.id;
        // clones assignment, to avoid double binding and allowing changes to object
        this.clone = Object.create(assignment);

        // setup default on dropdown for recurring frequency
        if (this.clone.recurring) { // if recurring, get frequency and interval from assignment
            this.freq = this.freqOPTIONS[this.clone.recurringInfo.options.freq];
            this.interval = this.clone.recurringInfo.options.interval;
        } else { // else, default values
            this.freq = this.freqOPTIONS[5]; // aldrig
            this.interval = 1;
        }

        this.dateOptions = {
            startingDay: 1 // Startday is Monday
        };

        this.keyboardConfig2 = {
            kt: 'Dansk',
            numberPad: false,
            relative: true,
            // relative: false, // Keyboard will be positioned in the bottom
            sizeAdj: false, // user cannot adjust size of keyboard
            deadkeysOn: false // Special keys that we don't need
        };

        this.numpadConfig2 = {
            kt: 'Numerico'
        };


        // // Configuration object for Angular Virtual Keyboard
        // this.keyBoardConfig = {
        //     kt: 'Dansk', // layout
        //     numberPad: false,
        //     relative: false, // Keyboard will be positioned in the bottom
        //     sizeAdj: false, // user cannot adjust size of keyboard
        //     deadkeysOn: false // Special keys that we don't need
        // };

    }

    /**
     * Sets up Modal window by defining the view (html) and controller for window.
     * @returns {{templateUrl: string, controller: AssignmentViewController, controllerAs: string, backdrop: string, keyboard: boolean}}
     */
    static options(): ng.ui.bootstrap.IModalSettings {
        return {
            templateUrl: 'client/today/assignment-view/assignment-view.html',
            controller: AssignmentViewController,
            controllerAs: '$ctrl',
            backdrop: 'static', // disable that clicks outside closes modal window
            keyboard: false,    // disable that keyboard ESC click closes window
        };
    }

    /**
     * Method called when user clicks finish assignment.
     * Opens modal window asking for confirmation
     */
    finishAssignment() {
        var options = ConfirmViewController.options(); // gets setup for ConfirmView modal window
        options.resolve = {assignment: this.clone}; // sends the assignment to finish

        this.$uibModal
            .open(options) // open ConfirmView modal window
            .result
            .then(updatedItem => this.assignmentStore.finishAssignment(updatedItem)); // if user confirms, call finisAssignment on store
        this.$uibModalInstance.dismiss('cancel'); // close AssignmentView modal window
    }

    /**
     * Closes modal window on click on cancel button
     */
    cancel() {
        this.$uibModalInstance.dismiss('cancel');
    }

    /**
     * Method to save changes to assignment, if user changes something
     */
    saveChanges() {

        // if recurringInfo is changed. A new RRule must be created with new info.
        // dtstart remains the same.

        // check if recurring is set to true on cloned assignment.
        if (this.clone.recurring) {
            if (!this.assignment.recurring) { // if original assignment was NOT set to recurring
                // create new RRule, since this.assignment.recurringInfo is ''
                var rule = new RRule({
                    freq: this.freqOPTIONS.indexOf(this.freq),
                    interval: this.interval,
                    dtstart: this.clone.date,
                });
                this.clone.recurringInfo = rule;
            } else { // if original assignment WAS set to reccuring
                // update recurringInfo object to new data
                var ruleObj = RRule.parseString(this.clone.recurringInfo.toString());
                ruleObj.freq = this.freqOPTIONS.indexOf(this.freq);
                ruleObj.interval = this.interval;
                this.clone.recurringInfo = new RRule(ruleObj);
            }
        }
        this.assignmentStore.updateAssignment(this.clone); // update assignmeng using store, sending clone
        this.$uibModalInstance.dismiss('cancel'); // close AssignmentView modal window
    }

    /**
     * Method used to write the priority in Danish
     * @param prio gets PriorityLevel enum
     * @returns {string} translated enum
     */
    public prio2String(prio: PriorityLevel): string {
        return prioToString(prio);
    }

    public setRecurring() {
        if (this.freq === 'Aldrig') {
            this.clone.recurring = false;
        } else {
            this.clone.recurring = true;
        }
    }

    /**
     * Gets 'caregiver' data from LocalStorage to determine if caregiver is logged in
     * @returns {boolean} true/false indicating if caregiver is logged in or not
     */
    public isCarerLoggedIn() {
        return (this.localStorageService.getItem('caregiver') === 'true');
    }


    // variables
    public prio: PriorityLevel;
    public freqOPTIONS: Array<string>;
    public freqUnits: Array<string>;
    public clone: Assignment;
    public id: string;
    public freq: string;
    public interval: number;
    public keyboardConfig2: Object;
    public numpadConfig2: Object;
    public dateOptions: Object;
}
