/// <reference path="../../../../typings/browser.d.ts" />
import {Assignment, PriorityLevel} from '../../bll/assignment';
import {IAssignmentStore} from '../../bll/interfaces/assignmentStore';
import {AssignmentViewController} from '../assignment-view/assignment-view';
import {LocalStorageService} from '../../dal/localStorage';

/**
 * Defines assignmentListItem component by defining which Controller and what View component uses
 * @returns {{controller: AssignmentListComponent, templateUrl: string, bindings: {assignment: string}}}
 */
function assignmentListItem() {
    'use strict';
    return {
        controller: AssignmentListComponent, // controller 
        templateUrl: 'client/today/assignment-list-item/assignment-list-item.html', // view
        bindings: { // binds to assignment from assignment-list
            assignment: '<'
        }
    };
}

/**
 * Controller for component AssignmentListItem
 */
class AssignmentListComponent {
    static $inject = ['assignmentStore', '$uibModal', 'localStorageService'];

    // inputs
    assignment: Assignment;

    /**
     * Constructor for AssignmentListComponent
     * @param assignmentStore Store for accessing Assignments
     * @param $uibModal Service for creating Modal windows
     * @param localStorageService Wrapper for getting data from LocalStorage
     */
    constructor(protected assignmentStore: IAssignmentStore, protected $uibModal: ng.ui.bootstrap.IModalService,
                protected localStorageService: LocalStorageService) {
    }

    /**
     * Method called when assignment is clicked
     */
    public clickAssignment() {
        var options = AssignmentViewController.options(); // gets setup for AssignmentView modal window
        options.resolve = {assignment: this.assignment}; // send assignment to modal window

        this.$uibModal
            .open(options); // open modal window
    }

    /**
     * Sets color of assignment button based on priority OR who is logged in
     * @returns {string}
     */
    public getColor() {
        var style = '';

        var pri = this.assignment.priority;
        switch (pri) {
            case PriorityLevel.High :
                style = 'btn-danger'; // red button
                break;
            case PriorityLevel.Normal :
                style = 'btn-warning'; // yellow button
                break;
            case PriorityLevel.Low :
                style = 'btn-success'; // green button
                break;
            default:
                style = 'btn-primary'; // blue, default
        }


        if (localStorage.getItem('caregiver') === 'true') { // if carer is logged in
            if (this.assignment.roles.indexOf('carer') !== -1) { // all assignment with role-array including 'carer'
                style = 'btn-primary'; // set style to blue
            }
        }
        return style;
    }
}

export {assignmentListItem};
