/// <reference path="../../../../typings/browser.d.ts" />
import {Assignment} from '../../bll/assignment';

// http://stackoverflow.com/a/28373164/3411879
/**
 * Controller for ConfirmView Modal window
 */
export class ConfirmViewController {
    static $inject = ['$uibModalInstance', 'assignment'];

    /**
     * Constructor for ConfirmViewController
     * @param $uibModalInstance Injectiable for $uibModal for accessing instance of modal window
     * @param assignment Assignment class
     */
    constructor(private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, public assignment: Assignment) {

        // Configuration object for Angular Virtual Keyboard
        this.keyboardConfig = {
            kt: 'Dansk', // layout
            numberPad: false,
            relative: false, // Keyboard will be positioned in the bottom
            sizeAdj: false, // user cannot adjust size of keyboard
            deadkeysOn: false // Special keys that we don't need
        };
    }

    /**
     * Sets up Modal window by defining the view (html) and controller for window.
     * @returns {{templateUrl: string, controller: ConfirmViewController, controllerAs: string, backdrop: string, keyboard: boolean}}
     */
    static options(): ng.ui.bootstrap.IModalSettings {
        return {
            templateUrl: 'client/today/confirm-view/confirm-view.html',
            controller: ConfirmViewController,
            controllerAs: '$ctrl',
            backdrop: 'static', // disable that clicks outside closes window
            keyboard: false,    // disable that keyboard ESC click closes window
        };
    }

    /**
     * Method called, if user confirms finishing assignment
     */
    finishAssignment() {
        // closing the instance of is caught in assignment-view.ts finishAssignment(). triggers open.result.then in finishAssignment
        this.$uibModalInstance.close(this.assignment);
    }

    /**
     * Method called if user cancels finishing assignment
     */
    cancel() {
        this.$uibModalInstance.dismiss('cancel'); // closes confirmview modal window
    }

    public keyboardConfig: Object;

}
