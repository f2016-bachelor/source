import {routes}  from './routes';
import {assignmentListItem} from './assignment-list-item/assignment-list-item';
import {assignmentList} from './assignment-list/assignment-list';
import {TodayController} from './todayController';

var todayModule: ng.IModule = angular.module('today', ['ui.bootstrap', 'ui.router', 'services']);

todayModule
    .config(routes)
    .component('assignmentList', assignmentList()) // defines component for assignment list on today html
    .component('assignmentListItem', assignmentListItem()) // defines component for single assinmnet on assignmentlist html
    .controller('todayController', TodayController); // defines controller

export {todayModule};
