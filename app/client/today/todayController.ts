/// <reference path='../../../typings/browser.d.ts' />
import {User} from '../bll/models/user';
import {CurrentUser} from '../bll/currentUser';

/**
 * Controller for Today page
 */
export class TodayController {
    static $inject = ['currentUser', '$timeout'];

    /**
     * Constructor for TodayController
     * @param currentUser Store for getting users
     * @param $timeout Service: AngularJS' version of the window.setTimeout
     */
    constructor(protected currentUser: CurrentUser, protected $timeout: ng.ITimeoutService) {

        // subscribes to current user to get the user logged in and firstname
        currentUser.getUserObjectObservable().subscribe((user: User) => this.user = user); // for getting id
        currentUser.getUserObjectObservable().subscribe((user: User) => this.firstName = user.firstName);

        this.clockConfig =  {
            format: 'EEEE d MMMM yyyy',
            theme: 'light'
        };


        this.startTime();
    }

    // http://www.w3schools.com/jsref/met_win_settimeout.asp
    /**
     * Starts clock on today-page
     */
    startTime() {
        // var tt = new Date();
        this.date = new Date();
        // var h = tt.getHours();
        // var m = tt.getMinutes();
        //
        // m = this.checkTime(m);
        // this.time = h + ':' + m;
        //
        // // for 3/3/2016
        // this.date = tt.toLocaleDateString();
        this.$timeout(() => {
            this.startTime();
        }, 500);
    };

    // http://www.w3schools.com/jsref/met_win_settimeout.asp
    /**
     * Adds 0 infront of minutes less that 10 ( 01-02-03 etc)
     * @param i minutes as single number (1,2,10)
     * @returns {any} minutes as double number (01,02,10)
     */
    checkTime(i) {
        if (i < 10) {
            i = '0' + i;
        }
        return i;
    };

    public user: User;
    public firstName = '';
    public time;
    public date;
    public clockConfig: Object;
}
