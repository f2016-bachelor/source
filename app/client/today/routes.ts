/// <reference path="../../../typings/browser.d.ts" />

import {IStateProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider'];

/**
 * Set up routes for today page
 * @param $stateProvider for creating states
 */
function routes($stateProvider: IStateProvider) {
    'use strict';
    $stateProvider
        .state('main.today', {
            url: '/:userId/today', // url, with userId added - sent from home.html when redirecting on button click
            templateUrl: 'client/today/today.html', // view
            controller: 'todayController', // controller
            controllerAs: '$ctrl'
        });
}

export {routes}
