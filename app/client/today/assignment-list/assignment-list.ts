/// <reference path="../../../../typings/browser.d.ts" />

import * as Rx from 'rx';
import {Assignment} from '../../bll/assignment';
import {IAssignmentTransport} from '../../bll/interfaces/IAssignmentTransport';
import {CurrentUser} from '../../bll/currentUser';
import {LocalStorageService} from '../../dal/localStorage';

/**
 *  Defines assignmentList component by defining which Controller and what View component uses
 * @returns {{controller: AssignmentListController, templateUrl: string}}
 */
function assignmentList() {
    'use strict';
    return {
        controller: AssignmentListController, // per default, the "controllerAs" attribute is set to $ctrl
        templateUrl: 'client/today/assignment-list/assignment-list.html'
    };
}

/**
 * Controller for component AssignmentList
 */
class AssignmentListController {
    static $inject = ['assignmentStore', 'currentUser', 'localStorageService'];

    /**
     * Constructor for AssignmentListController
     * @param assignmentStore Store for accessing Assignments
     * @param currentUser Observable for getting the current user. Used to create assignment on specific user
     * @param localStorageService Wrapper for getting data from LocalStorage
     */
    constructor(protected assignmentStore: IAssignmentTransport, protected currentUser: CurrentUser,
                protected localStorageService: LocalStorageService) {

        var getDayLater = () => {
            var adaylater = new Date(); // getting today
            adaylater.setHours(adaylater.getHours() + 24); // setting date to tomorrow
            return adaylater;
        };

        var aDayLater = new Rx.BehaviorSubject<Date>(getDayLater());

        Rx.Observable.interval(1000 * 60)
            .map((i) => getDayLater())
            .subscribe(aDayLater);

        // assignment observable, filter assignments, so only unfinished assignments are returned
        var assignmentobs = assignmentStore.getAssignmentsObservable()
            .map((items: Array<Assignment>) => items.filter((item) => !item.finished));

        // define currentuser observable
        var userobs = currentUser.getUserStringObservable();

        // localStorageService observable, used to see if caregiver is logged in
        // assignment also returned based on which role is logged in
        var carerInfo = new Rx.BehaviorSubject<string>(localStorageService.getItem('caregiver'));
        localStorageService.getStream().filter((vv) => vv.key === 'caregiver').map((vv) => vv.value).subscribe(carerInfo);

        // filter assignments from assignmentobs based on which role is logged in
        var rolesFilteredList = assignmentobs.combineLatest(carerInfo.map((isCarer) => this.isCarerLoggedIn(isCarer)),
            (assignments, info) => {
                if (!info) { // if carer is NOT logged in, only return assignments where the role array contains 'resident' role
                    return assignments.filter((assignment) => assignment.roles.indexOf('resident') !== -1);
                } else { // else return all assignments, so caregiver can see all assignments
                    return assignments;
                }
            });

        // filter assignemnts from rolesFilteredList based on which user is active
        var assignmentsList = rolesFilteredList.combineLatest(userobs, (assignments, user) => {
            // only return assignemts where assignemnts user array contain the current user
            return assignments.filter((assignment) => assignment.users.indexOf(user) !== -1);
        }).combineLatest(aDayLater, (items: Array<Assignment>, adaylater: Date) => {

            // final mapning needed for getting assignments: based on if the assignment is recurring or not

            // assignments that are not recurring is filtered in an array
            var oneoffs = items.filter((item) => !item.recurring).filter((item) => item.date.getTime() <= adaylater.getTime());

            // assignments that are recurring have an instance generated for each recurrence.
            var recurring = items.filter((item) => item.recurring)
                .map((item: Assignment) =>
                    item.recurringInfo // item has one recurrringInfo that is of type RRule
                        .all((date, i) => date.getTime() <= adaylater.getTime()) // get all recurrences untill a day later (24 hours later)
                        .map((date, i) => { // copy (shallow) the assignment and change date and seqno.
                            var assignment = Object.create(item); // copy
                            assignment.date = date; // set date to specific recurrence found in .all
                            assignment.seqno = i; // set seqno to sequence number in recurrence list
                            return assignment;
                        }));

            // merge oneoffs (Array<Assignment>) with recurring (Array<Array<Assignment>>)
            // http://stackoverflow.com/questions/10865025/merge-flatten-a-multidimensional-array-in-javascript/10865042 (2016-04-27)
            return [].concat.apply(oneoffs, recurring);
        });

        // sorting assigmnents based on date, to ensure earliest assignments is first in list
        assignmentsList.map((items: Array<Assignment>) => items.sort((left: Assignment, right: Assignment) => left.date.getTime() - right.date.getTime()))
            .subscribe(items => this.assignments = items);
    }

    /**
     * Gets assignmentMap to show in list. Called from html in ng-repeat
     * @returns {any} list of assignments
     */
    public getItems(): Array<Assignment> {
        return this.assignments;
    }

    /**
     * Compares string from carerInfo to 'true'
     * @param carerInfo string from observable on localStorageService
     * @returns {boolean} return true/false based on what string carerInfo is
     */
    public isCarerLoggedIn(carerInfo) {
        return (carerInfo === 'true');
    }

    //
    // http://stackoverflow.com/questions/35888671/sorting-string-enum-in-typescript-and-angularjs
    /**
     * Defines a custom order used to filter assignments based on Priority level
     * sorts list based on enum. asked for help on StackOverflow and got the following answer
     * @param item single item
     * @returns {number} returns number to sort after
     */
    public customOrder(item) {
        return item.priority === 'High' ? 1 // highest priority put on top
            : item.priority === 'Normal' ? 2 // then normal
            : 3;                             // and then low
    }

    // variables
    public assignments;

}

export {assignmentList};
