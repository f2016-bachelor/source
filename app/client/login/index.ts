import {routes}  from './routes';
import {LoginController} from './loginController';

var loginModule: ng.IModule = angular.module('login', ['ui.router', 'dal']);

loginModule
    .config(routes)
    .controller('LoginController', LoginController);
export {loginModule};
