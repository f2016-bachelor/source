/// <reference path="../../../typings/browser.d.ts" />
import {IStateProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider'];

/**
 * Set up routes for login page
 * @param $stateProvider for creating states
 */
function routes($stateProvider: IStateProvider) {
    'use strict';
    $stateProvider
        .state('main.dev', {
            url: '/login', // url
            templateUrl: 'client/login/deviceMain.html', // view
            controller: 'LoginController', // controller
            controllerAs: '$ctrl',
        })
        // sub-pages - links to htmls defined in subviews folder.
        // url is attached to the url in 'main' state: /:userId/create/ + fx 'name'
        .state('main.dev.login', {
            url: '/',
            templateUrl: 'client/login/login.html',

        })
        .state('main.dev.reg', {
            url: '/registerDevice',
            templateUrl: 'client/login/registerDevice.html',

        });
}

export {routes}
