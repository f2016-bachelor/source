import * as uuid from 'uuid';
import {IStateService} from 'angular-ui-router';
import {DeviceStore} from '../bll/stores/deviceStore';
import {Device} from '../../shared/dto/device';
import {LocalStorageService} from '../dal/localStorage';

/**
 * Controller for login-page
 */
export class LoginController {

    static $inject = ['$http', '$state', 'deviceStore', 'localStorageService'];

    /**
     * Constructor for LoginController
     * @param $http Service for creating HTTP calls
     * @param $state Service to change state
     * @param deviceStore Store for accessing Devices
     * @param localStorageService Wrapper for getting data from LocalStorage
     */
    constructor(protected $http: angular.IHttpService, protected $state: IStateService, protected deviceStore: DeviceStore, protected localStorageService: LocalStorageService) {

        // set ui-view default to name.html
        // http://www.podpea.co.uk/blog/default-child-states-in-angular-ui-router/
        var parent = 'main.dev';
        if ($state.current.name.substr(-parent.length) === parent) {
            $state.go('main.dev.login');
        }

        // Init configuration object for Angular Virtual Keyboard
        this.keyboardConfig = {
            kt: 'Dansk', // layout
            numberPad: false,
            relative: false, // Keyboard will be positioned in the bottom
            sizeAdj: false, // user cannot adjust size of keyboard
            deadkeysOn: false // Special keys that we don't need
        };
    }

    /**
     * Login function called when users has entered ID/password and pressed Login button
     * @param id Device ID entered by user
     * @param password Device Password entered by user
     */
    login(id, password) {
        this.device.id = id;
        this.device.password = password; // sets up device to send in http.post call to /api/login

        this.$http.post('/api/login', this.device).then((response) => { // calls api's login functionality

            // successful login

            var data = response.data;
            // Save the token in localstorage
            this.localStorageService.setItem('jwt', data.token);
            this.errmsg = ''; // no error msg
            // redirect to '/home'
            this.$state.go('main.home');
        }).catch((err) => {

            // not successful login

            this.errmsg = 'Ugyldigt login'; // error msg
            // redirect to '/login'
            this.$state.go('main.dev.login');
        });
    };

    /**
     * Creating a new device with user-chosen password
     * @param newPassword user specified password
     */
    registerDevice(newPassword) {
        var device = new Device(uuid.v1(), newPassword, [], []); // defines Device to be created
        this.deviceStore.create(device).then(() => { // creates device using device store and if successfull, logs in
            this.login(device.id, device.password);
        });
    };

    // variables
    public errmsg = '';
    protected device = {id: '', password: ''};
    public keyboardConfig: Object;
}
