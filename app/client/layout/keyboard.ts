keyboard.$inject = ['VKI_CONFIG'];
export function keyboard(VKI_CONFIG: any) {

    // Configure Angular Virtual Keyboard
    // Setup normal keyboard
    VKI_CONFIG.layout['Dansk'] = {
        'name': 'Danish', 'keys': [
            [['\u00bd', '\u00a7'], ['1', '!'], ['2', '"', '@'], ['3', '#', '\u00a3'], ['4', '\u00a4', '$'], ['5', '%', '\u20ac'], ['6', '&'], ['7', '/', '{'], ['8', '(', '['], ['9', ')', ']'], ['0', '=', '}'], ['+', '?'], ['\u00b4', '`', '|'], ['Bksp', 'Bksp']],
            [['Tab', 'Tab'], ['q', 'Q'], ['w', 'W'], ['e', 'E', '\u20ac'], ['r', 'R'], ['t', 'T'], ['y', 'Y'], ['u', 'U'], ['i', 'I'], ['o', 'O'], ['p', 'P'], ['\u00e5', '\u00c5'], ['\u00a8', '^', '~'], ['\'', ' * ']],
            [['Caps', 'Caps'], ['a', 'A'], ['s', 'S'], ['d', 'D'], ['f', 'F'], ['g', 'G'], ['h', 'H'], ['j', 'J'], ['k', 'K'], ['l', 'L'], ['\u00e6', '\u00c6'], ['\u00f8', '\u00d8'], ['Enter', 'Enter']],
            [['Shift', 'Shift'], ['<', '>', '\\'], ['z', 'Z'], ['x', 'X'], ['c', 'C'], ['v', 'V'], ['b', 'B'], ['n', 'N'], ['m', 'M', '\u03bc', '\u039c'], [',', ';'], ['.', ':'], ['-', '_'], ['Shift', 'Shift']],
            [[' ', ' ', ' ', ' '], ['AltGr', 'AltGr']]
        ],
        'lang': ['da']
    };

    // Setup numerical keyboard
    VKI_CONFIG.layout.Numerico = {
        'name': 'Numerico', 'keys': [
            [['1', '1'], ['2', '2'], ['3', '3'], ['Slet', 'Bksp']],
            [['4', '4'], ['5', '5'], ['6', '6'], ['Enter', 'Enter']],
            [['7', '7'], ['8', '8'], ['9', '9'], []],
            [['0', '0'], ['-'], ['+'], [',']]
        ], 'lang': ['da']
    };

    // Configure labels on the keyboard
    VKI_CONFIG.i18n = {
        '07': 'Ryd',

    };
}
