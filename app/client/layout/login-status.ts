/// <reference path="../../../typings/browser.d.ts" />

import * as Rx from 'rx';
import {LocalStorageService} from '../dal/localStorage';

/**
 * Defines loginstatus component by defining which Controller and what View component uses
 * @returns {{controller: LoginstatusComponent, templateUrl: string}}
 */
function loginstatus() {
    'use strict';
    return {
        controller: LoginstatusComponent,
        templateUrl: 'client/layout/login-status.html' // html view for component - includes ui-view
    };
}

/**
 * Controller for loginstatus component
 */
class LoginstatusComponent {
    static $inject = ['localStorageService'];

    /**
     * Constructor for Component Controller
     * @param localStorageService Wrapper for getting data from LocalStorage
     */
    constructor(protected localStorageService: LocalStorageService) {
        this.carerInfo = new Rx.BehaviorSubject<string>(localStorageService.getItem('caregiver'));
        // subscribes to localStorageService's item 'caregiver' for getting item when changed
        // filters localStorageServies for item with key 'caregiver', maps value .
        localStorageService.getStream().filter((vv) => vv.key === 'caregiver').map((vv) => vv.value).subscribe(this.carerInfo);
    }

    /**
     * Based on logged in status, sets style on Component <div>
     * @returns {any} style
     */
    public getHeader() {
        if ((this.carerInfo.value === 'true' )) { // if logged in
            this.val = true; // bool used in login-status.html, to determine which header to show
            return '#cdcdcd';
        } else { // otherwise
            this.val = false;
            return '';
        }
    }

    // variables
    public val;
    public carerInfo;
}

export {loginstatus};
