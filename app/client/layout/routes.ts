/// <reference path="../../../typings/browser.d.ts" />
import {IStateProvider, IUrlRouterProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider', '$urlRouterProvider', 'jwtInterceptorProvider', '$httpProvider'];

/**
 * Setup basis routes for app
 * @param $stateProvider for creating states
 * @param $urlRouterProvider watches $location. when $location changes, it routes to matching route
 * @param jwtInterceptorProvider
 * @param $httpProvider
 */
export function routes($stateProvider: IStateProvider, $urlRouterProvider: IUrlRouterProvider,
                jwtInterceptorProvider: angular.jwt.IJwtInterceptor, $httpProvider: angular.IHttpProvider) {
    'use strict';
    $stateProvider
        .state('main', {
            abstract: true, // defines main as 'parent' state .
            url: '', // no url - uses otherwise (below) to redirect
            templateUrl: 'client/layout/main.html', // url main.html defines loginstatus component (which links to ui-view)
            params: {userId: null}
        });

    // if no device is logged in, go to /login. Otherwise go to /home.
    // this checks both for startup page and for when invalid url is entered.
    $urlRouterProvider.otherwise((inject, location) => {
        if (localStorage.getItem('jwt') === null) {
            return '/login';
        } else {
            return '/home';
        }
    });

    // Setup JWT Interceptor Provider from JWT-Simple module.
    // Ensures that the stored token for the user is send with each request by adding it to the HTTP auth header
    // The 'config' parameter is injected by JWT-Simple
    jwtInterceptorProvider.tokenGetter.$inject = ['config'];
    jwtInterceptorProvider.tokenGetter = (config) => {

        // Skip authentication for any requests ending in .html - don't use the token here
        if (config.url.substr(config.url.length - 5) === '.html') {
            return null;
        }

        return localStorage.getItem('jwt');
    };

    // Register the interceptor with the http provider
    $httpProvider.interceptors.push('jwtInterceptor');
}
