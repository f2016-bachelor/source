import {routes}  from './routes';
import {keyboard} from './keyboard';
import {loginstatus} from './login-status';


var layoutModule: ng.IModule = angular.module('layout', ['ui.router', 'services']);

layoutModule
    .config(routes)
    .config(keyboard)
    .component('loginstatus', loginstatus());

export {layoutModule};
