/// <reference path="../../typings/browser.d.ts" />
import 'jquery';
import 'angular';
import 'angular-ui-router';
import 'angular-animate';
import 'angular-jwt';

import 'angular-virtual-keyboard/css/angular-virtual-keyboard.css!';
import 'angular-virtual-keyboard/release/angular-virtual-keyboard';

// css dependencies
import 'bootstrap/css/bootstrap.css!';
import 'font-awesome/css/font-awesome.css!';
import 'deepu105/angular-clock/dist/angular-clock.css!';

// Import the Angular Clock widget
import 'deepu105/angular-clock';

import 'angular-ui-bootstrap';

// data access & communication
import './dal/index';

// cross-cutting services
import './bll/index';

// website parts
import './layout/index';
import './login/index';
import './today/index';
import './home/index';
import './create/index';
import './registerUser/index';
import './log/index';
import './assignments/index';
import './calendar/index';


var app = angular.module('app', ['ui.router', 'angular-virtual-keyboard', 'ds.clock', 'ngAnimate', 'angular-jwt', 'login', 'registerUser', 'today', 'home', 'create', 'assignments', 'log', 'services', 'layout', 'calendar']);

angular.element(document).ready(() => {
    angular.bootstrap(document, ['app']);
});

export {app};
