/// <reference path="../../../typings/browser.d.ts" />
import {IStateProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider'];

/**
 * Set up routes for register user page
 * @param $stateProvider for creating states
 */
function routes($stateProvider: IStateProvider) {
    'use strict';
    $stateProvider
        .state('main.registerUser', {
            url: '/registerUser', // url
            templateUrl: 'client/registerUser/registerUser.html', // view
            controller: 'registerUserController', // controller
            controllerAs: 'registerUser',
        });
}

export {routes}
