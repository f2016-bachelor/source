import * as uuid from 'uuid';
import {IStateService} from 'angular-ui-router';
import {UserStore} from '../bll/stores/userStore';
import {User} from '../bll/models/user';

/**
 * Controller for registering new user
 */
export class RegisterUserController {

    static $inject = ['$state', 'userStore'];

    /**
     * Constructor for Controller
     * @param $state Service to change state
     * @param userStore Store for accessing CRUD actions on user
     */
    constructor(protected $state: IStateService, protected userStore: UserStore) {

        // Init configuration object for Angular Virtual Keyboard
        this.keyboardConfig = {
            kt: 'Dansk', // layout
            numberPad: false,
            relative: false, // Keyboard will be positioned in the bottom
            sizeAdj: false, // user cannot adjust size of keyboard
            deadkeysOn: false // Special keys that we don't need
        };
    }

    /**
     * Method for registering new user. called on button click in registerUser.html
     */
    registerUser() {
        var id = uuid.v4(); // new ID for new user
        var user = new User(id, this.firstName, this.lastName, this.middleName); // defines user based on input entered
        this.userStore.createUser(user); // creates user using userstore
        this.$state.go('main.home'); // redirect to home page
    }

    // variables
    public firstName: string;
    public middleName: string;
    public lastName: string;
    public keyboardConfig: Object;
}
