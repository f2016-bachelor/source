import {routes}  from './routes';
import {RegisterUserController} from './registerUserController';

var registerUserModule: ng.IModule = angular.module('registerUser', ['ui.router']);

registerUserModule
    .config(routes)
    .controller('registerUserController', RegisterUserController);
export {registerUserModule};
