/// <reference path="../../../../typings/browser.d.ts" />

// http://stackoverflow.com/a/28373164/3411879
import {Assignment} from '../../bll/assignment';

export class ConfirmDeleteController {
    static $inject = ['$uibModalInstance', 'assignment'];

    constructor(private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, public assignment: Assignment) {

    }
    static options(): ng.ui.bootstrap.IModalSettings {
        return {
            templateUrl: 'client/assignments/confirm-delete-modal/confirm-delete-view.html',
            controller: ConfirmDeleteController,
            controllerAs: '$ctrl',
            backdrop: 'static', // disable that clicks outside closes window
            keyboard: false,    // disable that keyboard ESC click closes window
        };
    }

    confirm() {
        this.$uibModalInstance.close(this.assignment);
    }

    cancel() {
        this.$uibModalInstance.dismiss('cancel'); // closes confirmview modal window
    }
}
