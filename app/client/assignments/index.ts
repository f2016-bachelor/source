import {routes}  from './routes';
import {AssignmentsController} from './assignmentsController';

var assignmentsModule: ng.IModule = angular.module('assignments', ['ui.router']);

assignmentsModule
    .config(routes)
    .controller('AssignmentsController', AssignmentsController);
export {assignmentsModule};
