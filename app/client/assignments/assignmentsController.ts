/// <reference path='../../../../typings/browser.d.ts' />

import {IAssignmentTransport} from '../bll/interfaces/IAssignmentTransport';
import {IUserStore} from '../bll/interfaces/userStore';
import {ConfirmDeleteController} from './confirm-delete-modal/confirm-delete-view';
import {AssignmentViewController} from '../today/assignment-view/assignment-view';


export class AssignmentsController {
    static $inject = ['assignmentStore', 'userStore', '$uibModal'];

    constructor(protected assignmentStore: IAssignmentTransport, protected userStore: IUserStore,
                protected $uibModal: ng.ui.bootstrap.IModalService) {
        this.users = userStore.getUsers();
        assignmentStore.getAssignmentsObservable().subscribe(items => this.assignments = items);
    }

    deleteAssignment(assignment) {
        var options = ConfirmDeleteController.options(); // gets setup for ConfirmView modal window
        options.resolve = {assignment: assignment};
        this.$uibModal
            .open(options) // open ConfirmView modal window
            .result
            .then(ass => {[
                this.assignmentStore.deleteAssignment(ass),
                this.assignments.splice(this.assignments.indexOf(ass), 1)];
            });
    }

    seeAssignment(assignment) {
        var options = AssignmentViewController.options(); // gets setup for AssignmentView modal window
        options.resolve = {assignment: assignment}; // send assignment to modal window

        this.$uibModal
            .open(options); // open modal window
    }

    public getItems(): Array<any> {
        return this.assignments;
    }

    public getRoles(assignment) {
        var ro = assignment.roles;
        var that = this;
        ro = ro.map((item) => {
            return that.rolesTranslation(item);
        });
        return ro.join(' og ');
    }

    public getStatus(assignment) {
        switch (assignment.finished) {
            case true:
                return 'Udført';
            case false:
                return 'Ikke Udført';
        }
    }

    public getUsername(user) {
        var name = this.users.filter((m) => m.id === user[0]).map((m) => {
            var n = m.firstName + ' ' + m.lastName;
            return n;
        });
        return name[0];
    }

    public rolesTranslation(role): string {
        switch (role) {
            case 'Plejer':
                return 'carer';
            case 'Borger':
                return 'resident';
            case 'carer':
                return 'Plejer';
            case 'resident':
                return 'Borger';
        }
    }

    public assignments;
    public users;
}
