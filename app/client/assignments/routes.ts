/// <reference path="../../../typings/browser.d.ts" />
import {IStateProvider} from 'angular-ui-router';

routes.$inject = ['$stateProvider'];

/**
 * Set up routes for log page
 * @param $stateProvider for creating states
 */
function routes($stateProvider: IStateProvider) {
    'use strict';
    $stateProvider
        .state('main.assignments', {
            url: '/assignments', // url
            templateUrl: 'client/assignments/assignments.html', // view
            controller: 'AssignmentsController', // controller
            controllerAs: '$ctrl',
        });
}

export {routes}
