/// <reference path="../../../typings/main.d.ts" />
/**
 * AssignmentDTO Class
 * Assignment version that can be send over the wire, ie no Date object
 */
export class AssignmentDTO {
    /**
     * Constructor for AssignmentDTO
     * @param id                UUID Assingment unique identifier
     * @param name              String Primary name of the assignment
     * @param priority          String The priority of assignment
     * @param date              Date/time for assignment
     * @param users             Array of user ID's referring to the users associated with this assignment
     * @param roles             Array of roles, specifieng who should do the assignment (carer or resident)
     * @param userInputNeeded   Bool, indicating if user input needed before assignement can be finished
     * @param recurring         Bool, indicating if the assignment is recurring
     * @param recurringInfo     String specifying how assignment should be recurring
     * @param description       Optional description
     * @param userInput         Field for entered userinput if needed when finished
     * @param finished          Bool, indicating if assignment is finished
     */
    constructor(public id: string, public name: string, public priority: String,
                public date: String, public users, public roles, public userInputNeeded = false,
                public recurring = false, public recurringInfo: string,
                public description = '', public userInput = '', public finished = false) {
        this.seqno = 0; // sequence number for assignment in list
        this.alarm = true; // notification is on
    }

    public seqno: number;
    public alarm;
}
