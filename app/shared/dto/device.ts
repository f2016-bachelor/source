class Device {
    /**
     * Constructor for Device
     * @param id device id
     * @param password device password
     * @param users array of user ids
     * @param assignments array of assignment ids
     */
    constructor(public id: string, public password: string, public users: Array<string>, public assignments: Array<string>) {
    }
}
export {Device};
