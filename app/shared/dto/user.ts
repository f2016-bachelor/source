/// <reference path="../../../typings/browser.d.ts" />

class UserDTO {
    /**
     * Constructor for UserDTO
     * @param id user id
     * @param firstName user firstname
     * @param lastName user lastname
     * @param middleName user middlename
     */
    constructor(public id: string, public firstName: string,
                public lastName: string, public middleName = '') {}
}

export { UserDTO };
