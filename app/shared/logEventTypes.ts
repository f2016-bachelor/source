/**
 * Class holding static string member variables describing the types of logs used in this system
 */
export class LogEventTypes {

    // Assignment specific log types - the reason for the form of these will be clear in the AssignmentLogger class
    static assignmentCreate: string = 'oprettet';
    static assignmentFinish: string = 'udført';
    static assignmentUpdate: string = 'opdateret';
    static assignmentDelete: string = 'slettet';

    // User specific log types
    static userCreate: string = 'Bruger oprettet';
    static userUpdate: string = 'Bruger opdateret';

    // Device specific log types
    static deviceCreate: string = 'Enhed oprettet';

    // Login specific log types
    static loggedOn: string = 'Enhed logget på';
}
