var api = '/api';
var apiAssignments = api + '/assignments';
var apiDevices = api + '/devices';
var apiLogs = api + '/logs';
var apiNotifications = api + '/notifications';
var apiUsers = api + '/users';
var apiLogin = api + '/login';

/**
 * Setup urls for api
 */
export default {
    api: {
        this: api,
        assignments: {
            this: apiAssignments,
            create: apiAssignments + '/create',
            delete: apiAssignments + '/delete',
            update: apiAssignments + '/update'
        },
        devices: {
            this: apiDevices
        },
        logs: {
            this: apiLogs
        },
        notifications: {
            this: apiNotifications,
            text: apiNotifications + '/text'
        },
        login: {
            this: apiLogin,
            rfid: apiLogin + '/rfid'
        },
        users: {
            this: apiUsers,
            create: apiAssignments + '/create',
            delete: apiAssignments + '/delete',
            update: apiAssignments + '/update'
        }
    }
};
