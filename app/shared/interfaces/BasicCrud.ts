/// <reference path="../../../typings/browser.d.ts" />

export interface BasicCRUD<T> {
    getAll(): Array<T>;
    get(id: String): T;
    create(T);
    update(T);
    delete(T);
}
