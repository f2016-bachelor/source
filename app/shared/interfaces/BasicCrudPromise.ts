/// <reference path="../../../typings/browser.d.ts" />
export interface BasicCRUDPromise<T> {
    getAll(populate: Array<String> = []): Promise<Array<T>>;
    get(id: String, populate: Array<String> = []): Promise<T>;
    find(query: any, populate: Array<String> = []): Promise<Array<T>>;
    create(item: T): Promise<any>;
    update(item: T): Promise<any>;
    delete(item: T): Promise<any>;
}
