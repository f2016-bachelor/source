import BasicCRUDPromise from '../../interfaces/BasicCrudPromise';

/**
 * The ConverterCRUD class is a wrapper for a class implementing BasicCRUDPromise<T>.
 * Thus it can convert items to and from T and TModel
 */
export class ConverterCRUD<T, TModel> implements BasicCRUDPromise<T> {
    /**
     *
     * @param wrapped      The underlying BasicCRUDPromise<TModel> that gets called after the items have been converted
     * @param fromTModel   Function<TModel, retval T> converter function taking a TModel and converts it to a T type
     * @param toTModel     Function<T, retval TModel> converter function taking a T and converts it to a TModel type
     */
    public constructor(protected wrapped: BasicCRUDPromise<TModel>, public fromTModel: Function, public toTModel: Function) {
    }

    getAll(populate: Array<String> = []): Promise<Array<T>> {
        return this.wrapped.getAll(populate).then(items => items.map(item => this.toTModel(item)));
    }

    get(id: String, populate: Array<String> = []): Promise<T> {
        return this.wrapped.get(id, populate).then((item) => this.toTModel(item));
    }

    find(query: any, populate: Array<String> = []): Promise<Array<T>> {
        return this.wrapped.find(query, populate).then(items => items.map(item => this.toTModel(item)));
    }

    create(item: T): Promise<any> {
        return this.wrapped.create(this.fromTModel(item)) as Promise<any>;
    }

    update(item: T): Promise<any> {
        return this.wrapped.update(this.fromTModel(item)) as Promise<any>;
    }

    delete(item: T): Promise<any> {
        return this.wrapped.delete(item);
    }
}
