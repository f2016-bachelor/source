/// <reference path="../../../../typings/browser.d.ts" />

export enum CRUD {
    Read,
    Create,
    Update,
    Delete
}
