/// <reference path="../../../../typings/main.d.ts" />
import Config from '../../../shared/presentation/config';

export function socketDebugger(io: SocketIO.Server) {
    var urls = [
        'connection',
        'connect',
        'connect_error',
        'connect_timeout',
        'reconnect',
        'error',
        Config.api.assignments.this,
        Config.api.assignments.create,
        Config.api.assignments.update,
        Config.api.assignments.delete,
        Config.api.users.this,
        Config.api.users.create,
        Config.api.users.update,
        Config.api.users.delete
    ];
    urls.forEach((u) =>
        io.on(u, (data) => {
            var dataString = '';
            try {
                dataString = JSON.stringify(data);
            } catch (e) {
            }
            console.log('socket event: ' + u + ', ' + dataString);
        })
    );

    console.log('Socket debug setup for: ' + urls.join(', '));
}
