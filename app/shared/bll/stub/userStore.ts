/// <reference path="../../../../typings/browser.d.ts" />

import * as Rx from 'rx';
import {User} from '../../../client/bll/models/user';
import {IUserStore} from '../../../client/bll/interfaces/userStore';

class UserStore implements IUserStore {
    users: Array<User> = [];
    userRx: Rx.Subject<Array<User>>;

    constructor() {
        this.users.push(new User('guid-1', 'Peter', 'Larsen'));
        this.users.push(new User('guid-2', 'John', 'Nielsen'));
        this.userRx = new Rx.Subject<Array<User>>();
    }

    public getUsers(): Array<User> {
        return this.users;
    }

    public getUser(id): User {
        var matchingUsers = this.users.filter(user => user.id === id);
        if (matchingUsers.length > 0) {
            return matchingUsers[0];
        } else {
            return undefined;
        }
    }

    getUsersObservable(): Rx.Observable<Array<User>> {
        return this.userRx;
    }

    createUser(item: User) {
    }

    updateUser(item: User) {
    }

    emitList() {
        this.userRx.onNext(this.users);
    }
}

export {UserStore};
