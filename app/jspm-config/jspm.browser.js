// https://github.com/andrenarchy/jspm-typescript-es6-boilerplate/
SystemJS.config({
    paths: {
        "github:*": "jspm/github/*",
        "npm:*": "jspm/npm/*"
    }
});