/*globals module,process,require */
module.exports = function (config) {
    'use strict';
    config.set({
        autoWatch: true,
        singleRun: false,
        basePath: '',

        frameworks: ['jspm', 'jasmine'],

        files: [
            'node_modules/babel-polyfill/dist/polyfill.js'
        ],

        jspm: {
            config: 'app/jspm-config/config.js',
            packages: "app/jspm/",
            loadFiles: [
                'target/development/client/**/*.spec.js'
            ],
            serveFiles: [
                'target/development/client/**/!(*spec).js'
            ]
        },

        // - Chrome, ChromeCanary, Firefox, Opera, Safari (only Mac), PhantomJS, IE (only Windows)
        browsers: ['Chrome'],

        reporters: ['dots', 'coverage'],

        preprocessors: {"target/development/**/!(*spec).js": "coverage"},
        coverageReporter: {
            dir: 'target/coverageReport',

            reporters: [
                {type: 'json'},
                {type: 'lcov'}
            ]
        }

    });

};
