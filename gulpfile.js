var requireDir = require('require-dir');

// Require all tasks in tools/gulp/tasks, including sub folders
if(process.env.NODE_ENV === 'production') {
    requireDir('./tools/gulp/tasks');
    requireDir('./tools/gulp/tasks/client', {recurse: true});
    requireDir('./tools/gulp/tasks/server', {recurse: true});
    requireDir('./tools/gulp/tasks/production', {recurse: true});
} else {
    requireDir('./tools/gulp/tasks', {recurse: true});
}
