var _ = require('lodash');
var environments = require('gulp-environments');
var development_mode = environments.development;
var production_mode = environments.production;

var app = './app';
var client_src = app + '/client';
var assets = client_src + '/assets';
var e2e = 'e2e';
var vendor = 'app/vendor';
var build = './target';
var coverage = build + '/coverage';
var development = build + '/development';
var development_server = development + '/server';
var development_client = development + '/client';
var production = build + '/production';
var production_server = production + '/server';
var production_server_public = production_server + '/public';
var production_client = production + '/client';

var watch_ignore = app + '/jspm';
var jspm_path = app + '/jspm';

var config = {
    app: app,
    entrypoint: {
        server: {
            development: {
                source: development_server + '/server/app.js'
            },
            production: {
                source: production_server + '/server/app.js'
            }
        }
    },
    browser_sync: {
        setup: {
            server: {
                baseDir: [app, development_client]
            },
            port: 3000
        },
        redirect: {
            '/api': 'http://localhost:8080/api',
            '/socket.io': 'http://localhost:8080/socket.io'
        }
    },
    copy: {
        src: client_src + '/**/*.html',
        development: {
            assets: assets + '/**/*.*',
            js: app + '/**/*.js',
            vendorFiles: [],
            dest: development_client
        },
        production: {
            vendorFiles: [jspm_path + '/**/*.@(otf|ttf|woff|woff2)', client_src + '/favicon.ico'],
            dest: production_server_public
        }
    },
    delete: {
        development: {
            client: {
                source: development_client + '/**/*'
            },
            server: {
                source: development_server + '/**/*'
            }
        },
        production: {
            client: {
                source: production_client + '/**/*'
            },
            server: {
                source: production_server + '/**/*'
            }
        },
        coverage: {
            source: coverage + '/**/*'
        }
    },
    html: {
        source: client_src + '/index.html',
        development: {
            dest: development_client,
            coverage: coverage + '/js'
        },
        production: {
            dest: production_server_public
        }
    },
    images: {
        development: {
            source: assets + '/images/*.*',
            dest: development_client + '/assets/images'
        },
        production: {
            source: assets + '/images/*.*',
            dest: production_server_public + '/assets/images'
        }
    },
    production: production,
    sass: {
        development: {
            main: client_src + '/app.scss',
            source: client_src + '/**/*.scss',
            dest: development_client + '/assets' + '/css'
        },
        production: {
            main: client_src + '/app.scss',
            source: client_src + '/**/*.scss',
            dest: production_server_public + '/assets/css'
        }
    },
    scripts: {
        source: production_client + '/client/app.js',
        dest: production_server_public
    },
    test: {
        development: {
            source: development_server + '/{shared,server}' + '/**/*.spec.js'

        },
        production: {
            source: production_server + '/{shared,server}' + '/**/*.spec.js'
        }
    },
    typescript: {
        all: {
            source: app + '/{client,shared,server}' + '/**/*.ts',
            production: {
                documentation: production_server_public + '/doc'
            }
        },
        client: {
            source: app + '/{client,shared}' + '/**/*.ts',
            development: {
                dest: development_client,
                coverage: coverage + '/src'
            },
            production: {
                dest: production_client
            }
        },
        server: {
            source: app + '/{server,shared}' + '/**/*.ts',
            development: {
                dest: development_server
            },
            production: {
                dest: production_server
            }
        },
        e2e: {
            dest: build + '/e2e'
        }
    },
    typings: {
        source: 'typings.json'
    },
    watch: {
        client: {
            html: [client_src + '/**/*.html', '!' + watch_ignore],
            scripts: app + '/{client,shared}' + '/**/*.ts',
            sass: client_src + '/**/*.scss',
            assets: [app + '/assets/**/*.*', '!' + watch_ignore]
        },
        server: {
            scripts: app + '/{server,shared}' + '/**/*.ts'
        }
    }
};

// http://stackoverflow.com/a/24647115/3411879
// example: console.log(mergeExtend(theme, ['ANDROID'], ['IOS']));
function mergeExtend(obj, merge, skip) {
    var o, o1, nObj = {};
    for (o in obj) {
        // ensure we are only checking this obj's keys
        if (obj.hasOwnProperty(o)) {
            // check if we have keys to merge
            if ((merge || []).indexOf(o) > -1) {
                // if something was defined in merge, we expect the value to be an object
                for (var o1 in obj[o]) {
                    if (obj[o].hasOwnProperty(o1)) {
                        // put child obj val under parent key
                        nObj[o1] = obj[o][o1];
                    }
                }
                // ensure we are not skipping this key
            } else if ((skip || []).indexOf(o) === -1) {
                if (Object.prototype.toString.call(obj[o]) === '[object Object]') { // typeof obj[o] === 'object'
                    // go recursive if we are not merging and val is an object
                    nObj[o] = mergeExtend(obj[o], merge, skip);
                } else {
                    // otherwise just copy the key val over
                    nObj[o] = obj[o];
                }
            }
        }
    }

    return nObj;
}

// Mode: development,production
// Type: server_mode,client_mode
function modes() {
    var config_copy = _.cloneDeep(config);

    var toMerge = [];
    var toSkip = [];

    //if (modes.indexOf('client') > -1) {
    //    toMerge.push('client_mode');
    //    toSkip.push('server_mode');
    //}
    //else if (modes.indexOf('server') > -1) {
    //    toMerge.push('server_mode');
    //    toSkip.push('client_mode');
    //}

    if (development_mode()) {
        toMerge.push('development');
        toSkip.push('production');
    }
    else if (production_mode()) {
        toMerge.push('production');
        toSkip.push('development');
    }

    var count = toMerge.length; // toMerge.length
    while (count-- > 0) {
        config_copy = mergeExtend(config_copy, toMerge, toSkip);
    }
    return config_copy;
}

var compiled_config = modes();
module.exports = compiled_config;
