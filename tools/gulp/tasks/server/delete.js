var gulp = require('gulp');
var del = require('del');

/**
 * Delete folders and files
 */
gulp.task('delete:server', function () {
    var conf = require('../../config').delete.server;

    return del(conf.source);
});
