'use strict';

var gulp = require('gulp');

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('tdd', ['development_mode', 'test:server'], function () {
    var config = require('../../config').watch.client;

    gulp.watch(config.scripts, ['test:server']);
});
