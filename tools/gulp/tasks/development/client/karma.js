'use strict';

var path = require('path');
var gulp = require('gulp');
var Server = require('karma').Server;

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('test:client', ['run-tests'], function () {
    var config = require('../../config').watch.client;
    gulp.watch(config.scripts, ['typescript:client']);
});

gulp.task('run-tests', ['typescript:client'], function (done) {
    runTests(done);
});

function runTests(done) {
    new Server({
        configFile: path.join(__dirname, '/../../../../../karma.conf.js')
    }, done).start();
}


