var gulp = require('gulp');

/**
 * Start browsersync task and then watch files for changes
 */
gulp.task('watch:client', ['build:client', 'browser_sync'], function () {
    var config = require('../../../config').watch.client;

    gulp.watch(config.sass, ['sass']);
    gulp.watch(config.scripts, ['typescript:client']);
    gulp.watch(config.html, ['html']);
});
