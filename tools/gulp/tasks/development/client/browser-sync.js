var gulp = require('gulp');
var browserSync = require('browser-sync');
var spa = require("browser-sync-spa");

var proxy = require('proxy-middleware');
var url = require('url');

/**
 * Run the build task and start a server with BrowserSync
 */
gulp.task('browser_sync', function () {
    var config = require('../../../config').browser_sync;

    // Setup redirects from config.
    config.setup.server.middleware=[];

    // key is: '/api' and the value is 'http://.../'
    // so traffic to browsersync/api goes to http://.../ instead
    for(proxyKey in config.redirect) {
        var proxyOptions = url.parse( config.redirect[proxyKey] );
        proxyOptions.route = proxyKey;

        config.setup.server.middleware.push(proxy(proxyOptions));
    }

    browserSync.use(spa({
        selector: "[ng-app]",
        history: {
            index: '/index.html'
        }
    }));
    browserSync(config.setup);
});
