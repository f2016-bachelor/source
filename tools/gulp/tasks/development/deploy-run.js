var gulp = require('gulp');
var server = require('gulp-develop-server');

// run server
gulp.task('deploy:run', ['deploy'], function () {
    var config = require('../../config').entrypoint.server;
    server.listen({path: config.source});
});
