'use strict';

var gulp = require('gulp');

var gulpTypings = require("gulp-typings");

gulp.task("typings", function () {
    var conf = require('../../config').typings;
    gulp.src(conf.source)
        .pipe(gulpTypings()); //will install all typingsfiles in pipeline.
});
