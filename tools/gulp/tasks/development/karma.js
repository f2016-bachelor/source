'use strict';

var path = require('path');
var gulp = require('gulp');
var Server = require('karma').Server;

function runTests(done) {
    new Server({
        configFile: path.join(__dirname, '/../../../../karma.conf.js'),
        singleRun: true,
        autoWatch: false,
        jspm: {
            config: 'app/jspm-config/config.js',
            packages: "app/jspm/",
            loadFiles: [
                'target/production/client/**/*.spec.js'
            ],
            serveFiles: [
                'target/production/client/**/!(*spec).js'
            ]
        },
        browsers: ['PhantomJS'],
        reporters: ['dots', 'coverage'],
        preprocessors: {"target/production/**/!(*spec).js": "coverage"}
    }, done).start();
}

gulp.task('test-deploy', ['production_mode', 'test-deploy:client', 'test:server']);

gulp.task('test-deploy:client', ['typescript:client'], function (done) {
    runTests(done);
});
