var gulp = require('gulp');
var templateCache = require('gulp-angular-templatecache');
var insert = require('gulp-insert');

var environments = require('gulp-environments');
var development = environments.development;


gulp.task('templates', function () {
    var conf = require('../../config').html;
    var browserSync = require('browser-sync');

    return gulp.src(conf.source)
        .pipe(templateCache({root: '/client/', moduleSystem: 'Browserify', standalone: true}))
        //need to add an angular require to keep karma happy!
        .pipe(insert.prepend('var angular = require(\'angular\');'))
        .pipe(gulp.dest(conf.dest))
        .pipe(development(browserSync.stream()));
});
