'use strict';

var gulp = require('gulp');
var jasmine = require('gulp-jasmine');
var reporters = require('jasmine-reporters');
/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('test:server', ['typescript:server'], function () {
    var config = require('../../../config');

    //find test code - note use of 'base'
    return gulp.src(config.test.source)
        /*execute tests*/
        .pipe(jasmine({
            varbose: true,
            includeStackTrace: true,
            reporter: new reporters.TerminalReporter()
        }));
});
