var gulp = require('gulp');
var server = require('gulp-develop-server');

// run server
gulp.task('server:start', ['build:server'], function () {
    var config = require('../../../config').entrypoint.server;
    server.listen({path: config.source, execArgv: ['--debug=5859']});
});

// restart server if entrypoint changed
gulp.task('server:restart', ['typescript:server'], function () {
    server.restart();
});

gulp.task('watch:server', ['server:start'], function () {
    var config = require('../../../config').watch.server;

    gulp.watch(config.scripts, ['server:restart']);
});
