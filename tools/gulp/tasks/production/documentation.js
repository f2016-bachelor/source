var gulp = require('gulp');
var typedoc = require("gulp-typedoc");

gulp.task('documentation', ['typescript:client', 'typescript:server'], function () {
    var config = require('../../config').typescript;
    return gulp
        .src(config.all.source)
        .pipe(typedoc({
            // TypeScript options (see typescript docs)
            module: "commonjs",
            target: "es5",
            includeDeclarations: false,
            exclude: '**/typings/**/*',

            // Output options (see typedoc docs)
            out: config.all.documentation,

            // TypeDoc options (see typedoc docs)
            name: "my-project",
            ignoreCompilerErrors: true,
            version: true
        }));
});