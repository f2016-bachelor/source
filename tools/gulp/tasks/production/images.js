/**
 * Move assets, eg. images into place
 */

var gulp = require('gulp');
// var imageop = require('gulp-image-optimization');

gulp.task('images', function () {
    var config = require('../../config').images;

    return gulp.src(config.source)
        .pipe(gulp.dest(config.dest));
});