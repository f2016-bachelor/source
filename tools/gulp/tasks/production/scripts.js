var gulp = require('gulp');
var jspm = require('jspm');

gulp.task('scripts-bundle', function () {
    var config = require('../../config').scripts;

    var environments = require('gulp-environments');
    var production = environments.production;

    var builder = new jspm.Builder();

    builder.config({
        paths: {
            "assets/material-icons.css": "target/production/assets/material-icons.css",
            "assets/app.css": "target/production/assets/app.css"
        },
        separateCSS: true
    });

    return builder.buildStatic(config.source, config.dest + '/app.min.js', {/* minify: false, mangle: false */});

});