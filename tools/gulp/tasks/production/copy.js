var gulp = require('gulp');

/**
 * Copy vendor files to production directory
 */
gulp.task('copy-deploy', function () {
    var conf = require('../../config').copy;

    gulp
        .src(conf.src)
        .pipe(gulp.dest(conf.dest + '/client'));

    gulp
        .src(conf.vendorFiles[0])
        .pipe(gulp.dest(conf.dest + '/app/jspm'));

    gulp
        .src(conf.vendorFiles[1])
        .pipe(gulp.dest(conf.dest));
});
