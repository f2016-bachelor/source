var gulp = require('gulp');
var runSequence = require('run-sequence');

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('build', ['production_mode'], function (callback) {
    runSequence(['delete:client', 'delete:server'],
        ['typescript:client', 'typescript:server', 'documentation', 'sass', 'images', 'copy-deploy'],
        'html',
        'scripts-bundle',
        'delete:client',
        callback);
});
