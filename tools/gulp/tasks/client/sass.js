'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var flatten = require('gulp-flatten');
var globbing = require('gulp-css-globbing');

gulp.task('sass', function () {
    var browserSync = require('../browser-sync');
    var environments = require('gulp-environments');
    var development = environments.development;
    var conf = require('../../config').sass;

    return gulp.src(conf.main)
    .pipe(globbing({
      extensions: ['.scss']
    }))
    .pipe(sass().on('error', sass.logError))
    .pipe(flatten())
    .pipe(gulp.dest(conf.dest))
    .pipe(development(browserSync.stream()));
});
