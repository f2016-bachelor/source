var gulp = require('gulp');
var del = require('del');

/**
 * Delete folders and files
 */
gulp.task('delete:client', function () {
    var conf = require('../../config').delete.client;

    return del(conf.source);
});
