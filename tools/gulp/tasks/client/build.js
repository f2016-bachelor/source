var gulp = require('gulp');
var runSequence = require('run-sequence');

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('build:client', function (callback) {
    runSequence('delete:client',
        'html',
        [
            'typescript:client',
            'templates',
            'sass'
        ],
        callback);
});
