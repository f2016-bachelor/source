var gulp = require('gulp');
var removeCode = require('gulp-remove-code');

var environments = require('gulp-environments');
var development = environments.development;
var production = environments.production;

gulp.task('html', function () {
    var browserSync = require('../browser-sync');
    var config = require('../../config').html;

    return gulp.src(config.source)
        .pipe(production(removeCode({production: true})))
        .pipe(development(removeCode({development: true})))
        .pipe(gulp.dest(config.dest))
        .pipe(development(browserSync.stream()))
});
