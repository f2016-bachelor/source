'use strict';

var gulp = require('gulp');
var environments = require('gulp-environments');
var development = environments.development;
var production = environments.production;

var typescript = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var tslint = require('gulp-tslint');
var cache = require('gulp-cached');
var tsProject = typescript.createProject('tsconfig.json');

gulp.task('typescript:client', [], function () {
    var browserSync = require('../browser-sync');
    var conf = require('../../config').typescript.client;

    return gulp.src(conf.source)
        .pipe(cache('typescript-client'))
        .pipe(development(sourcemaps.init()))
        .pipe(tslint())
        .pipe(tslint.report('prose', {emitError: production()}))
        .pipe(typescript(tsProject))
        .pipe(development(sourcemaps.write('./maps', {includeContent: false, sourceRoot: '/'})))
        .pipe(gulp.dest(conf.dest))
        .pipe(development(browserSync.stream()));
});
