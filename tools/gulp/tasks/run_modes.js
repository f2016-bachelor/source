var gulp = require('gulp');

/**
 * Start browsersync task and then watch files for changes
 */
gulp.task('production_mode', function () {
    var environments = require('gulp-environments');
    var production = environments.production;
    environments.current(production);
});

gulp.task('development_mode', function () {
    var environments = require('gulp-environments');
    var development = environments.development;
    environments.current(development);
});
