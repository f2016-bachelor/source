var environments = require('gulp-environments');
var development = environments.development;
var noop = require('gulp-util').noop;

var browserSync; // fake browsersync if not in development mode
if (development()) {
    browserSync = require('browser-sync');
} else {
    browserSync = {
        stream: noop
    };
}

module.exports = browserSync;