// Type definitions for rrule
// Project: https://github.com/jkbrzt/rrule
// Definitions by: Jesper Utoft <https://github.com/jutoft>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

interface RRuleOption {
    /**
     * One of the following constants:
     * RRule.YEARLY
     * RRule.MONTHLY
     * RRule.WEEKLY
     * RRule.DAILY
     * RRule.HOURLY
     * RRule.MINUTELY
     * RRule.SECONDLY
     */
    freq;
    /**
     * The recurrence start.
     * Besides being the base for the recurrence,
     * missing parameters in the final recurrence instances will also be extracted from this date.
     * If not given, new Date will be used instead.
     */
    dtstart?: Date;

    /**
     * The interval between each freq iteration.
     * For example, when using RRule.YEARLY, an interval of 2 means once every two years, but with RRule.HOURLY,
     * it means once every two hours.
     * The default interval is 1.
     */
    interval?: Number;

    /**
     * The week start day.
     * Must be one of the RRule.MO, RRule.TU, RRule.WE constants, or an integer, specifying the first day of the week.
     * This will affect recurrences based on weekly periods.
     * The default week start is RRule.MO.
     */
    wkst?;

    /**
     *    How many occurrences will be generated.
     */
    count?: Number;

    /**
     * If given, this must be a Date instance, that will specify the limit of the recurrence.
     * If a recurrence instance happens to be the same as the Date instance given in the until argument,
     * this will be the last occurrence.
     */
    until?: Date;

    /**
     * If given, it must be either an integer, or a sequence of integers, positive or negative.
     * Each given integer will specify an occurrence number, corresponding to the nth occurrence of the rule inside the frequency period.
     * For example, a bysetpos of -1 if combined with a RRule.MONTHLY frequency, and a byweekday of (RRule.MO, RRule.TU, RRule.WE, RRule.TH, FR), will result in the last work day of every month.
     */
    bysetpos?;

    /**
     * If given, it must be either an integer, or a sequence of integers, meaning the months to apply the recurrence to.
     */
    bymonth?;

    /**
     * If given, it must be either an integer, or a sequence of integers, meaning the month days to apply the recurrence to.
     */
    bymonthday?;

    /**
     * If given, it must be either an integer, or a sequence of integers, meaning the year days to apply the recurrence to.
     */
    byyearday?;

    /**
     * If given, it must be either an integer, or a sequence of integers, meaning the week numbers to apply the recurrence to.
     * Week numbers have the meaning described in ISO8601, that is, the first week of the year is that containing at least four days of the new year.
     */
    byweekno?;

    /**
     * If given, it must be either an integer (0 == RRule.MO), a sequence of integers, one of the weekday constants (RRule.MO, RRule.TU, etc), or a sequence of these constants. When given, these variables will define the weekdays where the recurrence will be applied. It's also possible to use an argument n for the weekday instances, which will mean the nth occurrence of this weekday in the period. For example, with RRule.MONTHLY, or with RRule.YEARLY and BYMONTH, using RRule.FR.nth(+1) or RRule.FR.nth(-1) in byweekday will specify the first or last friday of the month where the recurrence happens. Notice that the RFC documentation, this is specified as BYDAY, but was renamed to avoid the ambiguity of that argument.
     */
    byweekday?;

    /**
     * If given, it must be either an integer, or a sequence of integers, meaning the hours to apply the recurrence to.
     */
    byhour?;

    /**
     * If given, it must be either an integer, or a sequence of integers, meaning the minutes to apply the recurrence to.
     */
    byminute?;

    /**
     * If given, it must be either an integer, or a sequence of integers, meaning the seconds to apply the recurrence to.
     */
    bysecond?;
}

class RRule {
    constructor(options, noCache: boolean = false);

    /**
     * Processed options applied to the rule.
     * Includes default options (such us wkstart).
     * Currently, rule.options.byweekday isn't equal to rule.origOptions.byweekday (which is an inconsistency).
     */
    options: RRuleOption;

    /**
     * The original options argument passed to the constructor.
     */
    origOptions: RRuleOption;

    /**
     * Returns all dates matching the rule. It is a replacement for the iterator protocol this class implements in the Python version.

     As rules without until or count represent infinite date series, you can optionally pass iterator, which is a function that is called for each date matched by the rule. It gets two parameters date (the Date instance being added), and i (zero-indexed position of date in the result).
     Dates are being added to the result as long as the iterator returns true.
     If a false-y value is returned, date isn't added to the result and the iteration is interrupted (possibly prematurely).
     * @param iterator
     */
    all(iterator?: (date: Date, i: number) => boolean): Array<Date>;

    /**
     * Returns all the occurrences of the rrule between after and before.
     * The inc keyword defines what happens if after and/or before are themselves occurrences.
     * With inc == true, they will be included in the list, if they are found in the recurrence set.
     * @param after
     * @param before
     * @param inc
     * @param iterator
     */
    between(after: Date, before: Date, inc = false, iterator?: (date: Date, i: number) => boolean): Array<Date>;

    /**
     * Returns the last recurrence before the given Date instance.
     * The inc argument defines what happens if dt is an occurrence.
     * With inc == true, if dt itself is an occurrence, it will be returned.
     * @param dt
     * @param inc
     */
    before(dt: Date, inc = false): Date;

    /**
     * Returns the first recurrence after the given Date instance.
     * The inc argument defines what happens if dt is an occurrence.
     * With inc == true, if dt itself is an occurrence, it will be returned.
     * @param dt
     * @param inc
     */
    after(dt: Date, inc = false): Date;

    /**
     * Returns a string representation of the rule as per the iCalendar RFC.
     * Only properties explicitly specified in options are included:
     */
    toString(): string;

    /**
     * Converts options to iCalendar RFC RRULE string:
     * @param options
     */
    static optionsToString(options): string;

    /**
     * Constructs an RRule instance from a complete rfcString:
     * @param rfcString
     */
    static fromString(rfcString): RRule;

    /**
     * Only parse RFC string and return options.
     * @param rfcString
     */
    static parseString(rfcString): RRuleOption;

    /**
     * Returns a textual representation of rule.
     * The gettext callback, if provided, will be called for each text token and its return value used instead.
     * The optional language argument is a language definition to be used (defaults to rrule/nlp.js:ENGLISH).
     * @param gettext
     * @param language
     */
    toText(gettext?, language?);

    /**
     * Provides a hint on whether all the options the rule has are convertible to text.
     */
    isFullyConvertibleToText(): boolean;

    /**
     * Constructs an RRule instance from text.
     * @param text
     * @param language
     */
    static fromText(text, language?): RRule;

    /**
     * Parse text into options:
     * @param text
     * @param language
     */
    static parseText(text, language?): RRuleOption;

    /**
     * Returns the number of recurrences in this set. It will have go trough
     * the whole recurrence, if this hasn't been done before.
     */
    count(): number;

    /**
     * @return a RRule instance with the same freq and options
     *          as this one (cache is not cloned)
     */
    clone(): RRule;

    /*
     Static freq options
     */
    static YEARLY;
    static MONTHLY;
    static WEEKLY;
    static DAILY;
    static HOURLY;
    static MINUTELY;
    static SECONDLY;
}

class RRuleSet {
    constructor(noCache=false);
    /**
     * Include the given rrule instance in the recurrence set generation.
     * @param rrule
     */
    rrule(rrule: RRule);

    /**
     * Include the given datetime instance in the recurrence set generation.
     * @param dt
     */
    rdate(dt: Date);

    /**
     * Include the given rrule instance in the recurrence set exclusion list.
     * Dates which are part of the given recurrence rules will not be generated, even if some inclusive rrule or rdate matches them.
     * @param rrule
     */
    exrule(rrule: RRule);

    /**
     *     Include the given datetime instance in the recurrence set exclusion list.
     *     Dates included that way will not be generated, even if some inclusive rrule or rdate matches them.
     * @param dt
     */
    exdate(dt: Date);

    // below is same as RRule

    /**
     * Returns all dates matching the rule. It is a replacement for the iterator protocol this class implements in the Python version.

     As rules without until or count represent infinite date series, you can optionally pass iterator, which is a function that is called for each date matched by the rule. It gets two parameters date (the Date instance being added), and i (zero-indexed position of date in the result). Dates are being added to the result as long as the iterator returns true. If a false-y value is returned, date isn't added to the result and the iteration is interrupted (possibly prematurely).
     * @param iterator
     */
    all(iterator?: (date: Date, i: number) => boolean): Array<Date>;

    /**
     * Returns all the occurrences of the rrule between after and before.
     * The inc keyword defines what happens if after and/or before are themselves occurrences.
     * With inc == true, they will be included in the list, if they are found in the recurrence set.
     * @param after
     * @param before
     * @param inc
     * @param iterator
     */
    between(after: Date, before: Date, inc = false, iterator?: (date: Date, i: number) => boolean): Array<Date>;

    /**
     * Returns the last recurrence before the given Date instance.
     * The inc argument defines what happens if dt is an occurrence.
     * With inc == true, if dt itself is an occurrence, it will be returned.
     * @param dt
     * @param inc
     */
    before(dt: Date, inc = false): Date;

    /**
     * Returns the first recurrence after the given Date instance.
     * The inc argument defines what happens if dt is an occurrence.
     * With inc == true, if dt itself is an occurrence, it will be returned.
     * @param dt
     * @param inc
     */
    after(dt: Date, inc = false): Date;
}

/**
 *
 * @param rrule RRule or RRuleSet string to parse
 * @param opt {forceset: true} to force return RRuleSet
 */
declare function rrulestr(rrule: String, opt?): any;
